// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.filters', [
])
  .filter(
    'matchProject',
    function () {
      return function (project_list, search_term) {
        if (!search_term) {
          return project_list;
        }
        var result = [];
        // Do a comparison after converting everything into
        // lowercase
        var term = search_term.toLowerCase();
        for (var index in project_list) {
          var project_name = project_list[index].name.toLowerCase();
          var project_id = project_list[index].short_id.toLowerCase();
          if (project_name.indexOf(term) != -1 ||
              project_id.indexOf(term) != -1) {
            result.push(project_list[index]);
          }
        }
        return result;
      };
    })

  .filter(
    'matchUser',
    function () {
      return function (user_list, search_term) {
        if (!search_term) {
          return user_list;
        }
        var result = [];
        // Do a comparison after converting everything into
        // lowercase
        var term = search_term.toLowerCase();
        for (var index in user_list) {
          var name = user_list[index].name.toLowerCase();
          var login = user_list[index].login.toLowerCase();
          if (name.indexOf(term) != -1 ||
              login.indexOf(term) != -1) {
            result.push(user_list[index]);
          }
        }
        return result;
      };
    });


// Local Variables:
// mode: javascript
// End:
