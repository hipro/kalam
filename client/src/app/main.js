// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam', [
  'ui.router',
  'kalam.app.templates',
  'kalam.utils',
  'kalam.handlers',
  'kalam.manage',
  'kalam.users',
  'kalam.projects',
  'kalam.repositories'
])

  .config(
    ['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('page', {
          abstract: true,
          controller: 'basePageCtrl',
          resolve: {
            // // A string value resolves to a service
            userInfo: 'userInfo',
            // We don't use the 'gotData' in the controller as this is
            // used to ensure that the userInfo gets its data filled
            // from the $resource
            gotData : ["userInfo", function(userInfo) {
              return userInfo.initializeData();
            }]
          },
          templateUrl: 'page.tpl.html'
        })
        .state('two-part-page', {
          parent: 'page',
          templateUrl: 'twoPartPage.tpl.html'
        })
        .state('home', {
          parent: 'page',
          url: '/home',
          views: {
            '': {
              controller: 'userDashboardCtrl',
              templateUrl: 'users/userDashboard.tpl.html'
            },
            "breadcrumbs": {
              controller: 'mainBreadcrumbCtrl',
              templateUrl: 'breadcrumbs.tpl.html'
            }
          }
        });

      $urlRouterProvider.otherwise("/home");
    }])

  .run (
    ['$rootScope', '$state', '$stateParams',
     function ($rootScope,   $state,   $stateParams) {

       // It's very handy to add references to $state and $stateParams
       // to the $rootScope so that you can access them from any scope
       // within your applications.For example, <li
       // ui-sref-active="active }"> will set the <li> // to active
       // whenever 'contacts.list' or one of its decendents is active.
       $rootScope.$state = $state;
       $rootScope.$stateParams = $stateParams;
     }
    ])

  .controller(
    "basePageCtrl",
    ['$scope', '$rootScope', '$state', 'userInfo',
     function ($scope, $rootScope, $state, userInfo) {
       $scope.userInfo = userInfo;
       $scope.logout_url = $("#logout-url").text();
       $rootScope.$on(
         '$stateChangeStart',
         function (event, toState, toParams, fromState, fromParams) {
           if (userInfo.debug_ui_state) {
             console.log(
               '$stateChangeStart to ' + toState.to +
                 '- fired when the transition begins. ' +
                 'toState,toParams : \n',toState, toParams);
           }
         });

       $rootScope.$on(
         '$stateChangeError',
         function(event, toState, toParams, fromState, fromParams) {
           if (userInfo.debug_ui_state) {
             console.log('$stateChangeError - fired when an error ' +
                         'occurs during transition.');
             console.log(arguments);
           }
         });

       $rootScope.$on(
         '$stateChangeSuccess',
         function(event, toState, toParams, fromState, fromParams) {
           if (userInfo.debug_ui_state) {
             console.log('$stateChangeSuccess to ' + toState.name +
                         '- fired once the state transition is complete.');
           }
         });

       $rootScope.$on(
         '$viewContentLoaded',
         function(event){
           if (userInfo.debug_ui_state) {
             console.log('$viewContentLoaded - fired after dom rendered',event);
           }
         });

       $rootScope.$on(
         '$stateNotFound',
         function(event, unfoundState, fromState, fromParams) {
           if (userInfo.debug_ui_state) {
             console.log('$stateNotFound ' + unfoundState.to +
                         '  - fired when a state cannot be found by its name.');
             console.log(unfoundState, fromState, fromParams);
           }
         });
     }])

  .controller(
    'mainBreadcrumbCtrl',
    ['$scope', '$state', function ($scope, $state) {

      $scope.breadcrumbs = [
        { title: 'Home', activePage: true, url: $state.href('home') }
      ];

     }]);


// Local Variables:
// mode: javascript
// End:
