// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.urls', [
])

  .service (
    'kalamURLs',
    function () {

      var base_url = $("#base-url").text();

      this.baseURL = function () {
        return base_url;
      };
    });


// Local Variables:
// mode: javascript
// End:
