// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.manage', [
  'ui.router',
  'kalam.projects.manage',
  'kalam.users.manage'
])

  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('manage', {
        parent: 'two-part-page',
        url: '/manage',
        views: {
          "sidebar": {
            controller: 'managePageCtrl',
            templateUrl: 'twoPartMenuLinks.tpl.html'
          },
          "breadcrumbs@page": {
            controller: 'manageBreadcrumbCtrl',
            templateUrl: 'breadcrumbs.tpl.html'
          },
          "": {
            controller: 'managePageCtrl',
              // Switching state from template is an hack. Even though
              // the 'managePageCtrl' specified in 'sidebar' view
              // should switch the state automatically but I found
              // that this doesn't happen consistently so this
              // template gets rendered at times (which should not
              // happen). So, I forcefully call the switchState
              // function from the template code.
              template: "<div data-ui-view>Main manage {{ switchState() }}</div>"
            }
          }
        });

    }])

  .controller(
    'managePageCtrl',
    ['$scope', '$state', function($scope, $state) {

      $scope.title = "Manage";
      $scope.links = [];

      // Check permission and push links
      $scope.links.push({
        name: "Users",
        bref: 'manage.users', // base abstract state
        sref: "manage.users.list" // default state
      });

      $scope.links.push({
        name: "Projects",
        bref: "manage.projects",
        sref: "manage.projects.list"
      });

      // We don't specify the 'manage' state as abstract but rather
      // redirect the user when he lands on the '/manage' link to
      // '/manage/users' from here. This is done so that he/she may be
      // redirected to appropriate pages based on permissions
      $scope.switchState = function () {
        if ($state.is("manage")) {
          $state.go("manage.projects.list");
        }
      };
      $scope.switchState();

    }])

  .controller(
    'manageBreadcrumbCtrl',
    ['$scope', '$state', function ($scope, $state) {

      $scope.breadcrumbs = [
         { title: 'Home', activePage: false, url: $state.href('home') },
         { title: 'Manage',
           activePage: true,
           url: $state.href('manage')
         }
       ];

     }]);


// Local Variables:
// mode: javascript
// End:
