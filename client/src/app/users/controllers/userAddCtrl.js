// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.add', [
  'ui.validate',
  'kalam.utils',
  'kalam.users.resources',
  'kalam.users.directives'
]).controller(
  'userAddCtrl',
  ['$scope', 'UserCollection', '$state', 'apiErrorDialog',
   function ($scope, UserCollection, $state, errorDlg) {

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };


     $scope.save = function () {
       if (!$scope.userAddForm.$valid) {
         return;
       }
       if (! $scope.userAddForm.admin.$dirty) {
         $scope.user.admin = false;
       }

       UserCollection.save(
         $scope.user,
         function (data) {
           $state.go('manage.users.list');
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return ($scope.userAddForm.$dirty && $scope.userAddForm.$valid);
     };

   }]);

// Local Variables:
// mode: javascript
// End:
