// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.list', [
  'kalam.utils',
  'kalam.users.resources'
]).controller(
  'userListCtrl',
  ['$scope', 'UserCollection', 'User',
   'confirmationDialogYesNo', 'apiErrorDialog',
   function ($scope, UserCollection, User, confirmDlg, errorDlg) {
     var qParams = {
       page : 1,
       page_size: 20
     };
     $scope.pnMaxSize = 5;

     function update () {
       UserCollection.get(
         qParams,
         function (data) {
           $scope.pnItemsPerPage = data.page_size;
           $scope.pnTotalItems = data.total_size;
           $scope.pnCurrentPage = data.current_page;

           $scope.users = data.result;
       });
     }

     $scope.filterActive = true;

     $scope.can_show_pagination = function () {
       return $scope.pnItemsPerPage < $scope.pnTotalItems;
     };

     $scope.pageChanged = function () {
       qParams.page = $scope.pnCurrentPage;
       update();
     };

     $scope.deleteUser = function(userid) {
       var result = confirmDlg.show(
         "Delete user?",
         "All the information associated with the user '" + userid + "' " +
           "will also be deleted. Do you want to continue?");
       result.then(function () {
         User.remove(
           {id: userid},
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }]);

// Local Variables:
// mode: javascript
// End:
