// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.profile.edit', [
  'ui.router',
  'kalam.utils',
  'kalam.users.resources',
  'kalam.users.directives'
]).controller(
  'profileEditCtrl',
  ['$scope', '$state', 'UserProfile', 'apiErrorDialog',
   function ($scope, $state, UserProfile, errorDlg) {
     var userId = $scope.userInfo.getLoginID();

     UserProfile.get(
       {loginId: userId},
       function (data) {
         $scope.user = data.result;
       },
       function (err_res) {
         console.log("Unable to get current user : " + userId);
         $state.go('home');
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return ($scope.profileEditForm.$dirty && $scope.profileEditForm.$valid);
     };

     $scope.save = function () {
       if (!$scope.profileEditForm.$valid) {
         return;
       }
       UserProfile.save(
         { loginId: userId },
         $scope.user,
         function (data) {
           $state.go('user.profile.show');
         },
         function (err_res) {
           errorDlg(err_res);
         });
     };


   }]);


// Local Variables:
// mode: javascript
// End:
