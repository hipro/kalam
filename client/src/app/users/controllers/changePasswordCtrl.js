// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.change_password', [
  'ui.validate',
  'kalam.utils',
  'kalam.users.resources'
]).controller(
  'changePasswordCtrl',
  ['$scope', '$state', 'UserProfile', 'apiErrorDialog',
   function ($scope, $state, UserProfile, errorDlg) {
     $scope.userId = $scope.userInfo.getLoginID();

     $scope.alert = { show: false,
                      type: '',
                      msg: ''
                    };

     $scope.closeAlert = function () {
       $scope.alert.show = false;
       $scope.alert.type = '';
       $scope.alert.msg = '';
     };

     UserProfile.get(
       {loginId: $scope.userId},
       function (data) {
         $scope.user = data.result;
       },
       function (err_res) {
         console.log("Unable to get current user : " + $scope.userId);
         $state.go('home');
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.changePasswordForm.$dirty && $scope.changePasswordForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.changePasswordForm.$valid) {
         return;
       }
       UserProfile.save(
         { loginId: $scope.userId },
         $scope.user,
         function (data) {
           $scope.alert.show = true;
           $scope.alert.type = 'success';
           $scope.alert.msg = 'Password changed sucessfully.';
           $scope.changePasswordForm.$setPristine(true);
           $scope.user.old_password = $scope.confirm_password = "";
           $scope.user.new_password = "";
         },
         function (err_res) {
           $scope.alert.show = true;
           $scope.alert.type = 'danger';
           $scope.alert.msg = err_res.data.errors[0].description;
           $scope.changePasswordForm.$setPristine(true);
           // TODO: write a separate utility for this
           $("#app-error").hide();
         });

     };


   }]);


// Local Variables:
// mode: javascript
// End:
