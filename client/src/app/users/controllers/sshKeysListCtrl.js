// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.sshkeys.list', [
  'kalam.utils',
  'kalam.users.resources'
]).controller(
  'sshKeysListCtrl',
  ['$scope', '$state', 'UserSSHKeyCollection', 'UserSSHKey',
   'confirmationDialogYesNo', 'apiErrorDialog',
   function ($scope, $state, UserSSHKeyCollection, UserSSHKey,
             confirmDlg, errorDlg) {
     var userId = $scope.userInfo.getLoginID();

     function update() {
       UserSSHKeyCollection.get(
         {
           loginId: userId
         },
         function (data) {
           $scope.keys = data.result;
         },
         function (err_res) {
           console.log("Unable to get SSH Key listing for user : " + userId);
           $state.go('home');
         });
     }

     $scope.removeKey = function (key_id) {
       var result = confirmDlg.show(
         "Remove SSH Key?",
         "SSH access using the key '" + key_id + "' will no longer be " +
           "possible once it is removed. Do you want to continue?");
       result.then(function () {
         UserSSHKey.remove(
           {
             loginId: userId,
             keyId: key_id
           },
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
           });
       });
     };

     update();

   }]);

// Local Variables:
// mode: javascript
// End:
