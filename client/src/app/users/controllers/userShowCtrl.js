// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.show', [
  'ui.router',
  'kalam.users.resources'
]).controller(
    'userShowCtrl',
    ['$scope', 'User', '$stateParams', '$state',
     function ($scope, User, $stateParams, $state) {
       var userId = $stateParams.userId;
       User.get(
         {id: userId},
         function(data) {
           $scope.user = data.result;
         },
         function(err_res) {
           console.log("Invalid userid : " + userId);
           $state.go("manage.users.list");
         });
     }]);

// Local Variables:
// mode: javascript
// End:
