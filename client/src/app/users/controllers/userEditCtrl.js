// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.edit', [
  'ui.validate',
  'kalam.utils',
  'kalam.users.resources',
  'kalam.users.directives'
]).controller(
  'userEditCtrl',
  ['$scope', '$state', '$stateParams', 'User','apiErrorDialog',
   function ($scope, $state, $stateParams, User, errorDlg) {
     var userId = $stateParams.userId;
     User.get(
       {id: userId},
       function(data) {
         $scope.user = data.result;
       },
       function(err_res) {
         console.log("Invalid userid : " + userId);
         $state.go("manage.users.list");
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.save = function () {
       if (!$scope.userEditForm.$valid) {
         return;
       }

       User.save(
         {id: userId},
         $scope.user,
         function (data) {
           $state.go("manage.users.show", {'userId': userId});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return ($scope.userEditForm.$dirty && $scope.userEditForm.$valid);
     };

   }]);

// Local Variables:
// mode: javascript
// End:
