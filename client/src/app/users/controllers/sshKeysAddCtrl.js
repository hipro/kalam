// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.controllers.sshkeys.add', [
  'ui.bootstrap.alert',
  'kalam.users.directives',
  'kalam.users.resources'
]).controller(
  'sshKeysAddCtrl',
  ['$scope', '$state', 'UserSSHKeyCollection',
   function ($scope, $state, UserSSHKeyCollection) {
     $scope.userId = $scope.userInfo.getLoginID();

     $scope.alert = { show: false,
                      type: '',
                      msg: ''
                    };

     $scope.closeAlert = function () {
       $scope.alert.show = false;
       $scope.alert.type = '';
       $scope.alert.msg = '';
     };

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.sshKeyAddForm.$dirty && $scope.sshKeyAddForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.sshKeyAddForm.$valid) {
         return;
       }

       UserSSHKeyCollection.save (
         {
           loginId: $scope.userId
         },
         $scope.sshkey,
         function (data) {
           $state.go("user.ssh_keys.list");
         },
         function (err_res) {
           $scope.alert.show = true;
           $scope.alert.type = 'danger';
           $scope.alert.msg = err_res.data.errors[0].description;
           $scope.sshKeyAddForm.$setPristine(true);
           // TODO: write a separate utility for this
           $("#app-error").hide();
         });
     };

   }]);


// Local Variables:
// mode: javascript
// End:
