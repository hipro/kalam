// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users', [
  'ui.router',
  'kalam.users.resources',
  'kalam.users.providers',
  'kalam.users.controllers.profile.show',
  'kalam.users.controllers.profile.edit',
  'kalam.users.controllers.sshkeys.list',
  'kalam.users.controllers.sshkeys.add',
  'kalam.users.controllers.change_password'
])

  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('user', {
        url: '/user',
        parent: 'two-part-page',
        views: {
          "sidebar": {
            controller: 'profileHomeCtrl',
            templateUrl: 'twoPartMenuLinks.tpl.html'
          },
          "breadcrumbs@page": {
            controller: 'profileBreadcrumbCtrl',
            templateUrl: 'breadcrumbs.tpl.html'
          },
          "": {
            controller: 'profileHomeCtrl',
            template: "<div data-ui-view>Profile {{ switchState() }}</div>"
          }
        }
      })
    // ------------- User profile related states and sub-states -----------
      .state('user.profile', {
        url: '/profile',
        abstract: true,
        template: '<div data-ui-view></div>'
      })
      .state('user.profile.show', {
        url: '',
        controller: 'profileShowCtrl',
        templateUrl: 'users/profileShow.tpl.html'
      })
      .state('user.profile.edit', {
        url: '/edit',
        controller: 'profileEditCtrl',
        templateUrl: 'users/profileEdit.tpl.html'
      })

      .state('user.change_password', {
        url: '/change_password',
        controller: 'changePasswordCtrl',
        templateUrl: 'users/changePassword.tpl.html'
      })
    // ----------- SSH Keys related states and sub-states ------------
      .state('user.ssh_keys', {
        url: '/ssh_keys',
        abstract: true,
        template: '<div data-ui-view></div>'
      })
      .state('user.ssh_keys.list', {
        url: '',
        controller: 'sshKeysListCtrl',
        templateUrl: 'users/sshKeysList.tpl.html'
      })
      .state('user.ssh_keys.add', {
        url: '/add',
        controller: 'sshKeysAddCtrl',
        templateUrl: 'users/sshKeysAdd.tpl.html'
      });

  }])

  .controller(
    'userDashboardCtrl',
    ['$scope', '$state', 'UIUserDashboard',
     function($scope, $state, UIUserDashboard) {
       UIUserDashboard.get(function(data) {
         var plist = data.result.projects;
         $scope.projects = [];
         for (var psid in plist) {
           var pdef = plist[psid];
           pdef.href = $state.href(
             'project', {projectId: pdef.short_id});
           $scope.projects.push(pdef);
         }
       });
     }])
  .controller(
    'profileHomeCtrl',
    ['$scope', '$state', function ($scope, $state) {

      $scope.title = "Profile";
      $scope.links = [
        {
          name: 'Details',
          bref: 'user.profile',
          sref: 'user.profile.show'
        },
        {
          name: 'Change password',
          bref: 'user.change_password',
          sref: 'user.change_password'
        },
        {
          name: 'SSH Keys',
          bref: 'user.ssh_keys',
          sref: 'user.ssh_keys.list'
        }
      ];

      $scope.switchState = function () {
        if ($state.is('user') &&
            !$state.is('user.profile.show')) {
          $state.go('user.profile.show');
          console.log('Switching to user.profile.show');
        }
      };

      $scope.switchState();
    }])

  .controller(
    'profileBreadcrumbCtrl',
    ['$scope', '$state', function ($scope, $state) {

      $scope.breadcrumbs = [
        { title: 'Home', activePage: false, url: $state.href('home') },
        { title: 'Profile',
          activePage: $state.is('user.profile.show'),
          url: $state.href('user.profile.show') }
      ];

    }]);


// Local Variables:
// mode: javascript
// End:
