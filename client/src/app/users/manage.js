// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.manage', [
  'ui.router',
  'kalam.users.controllers.list',
  'kalam.users.controllers.add',
  'kalam.users.controllers.show',
  'kalam.users.controllers.edit'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('manage.users', {
          url: '/users',
          abstract: true,
          template: '<ui-view/>',
          controller: 'manageUserCtrl'
        })
        // ----------- States for managing User --------------
        // Make this state as default by making the url as '' and its
        // parent as abstract
        .state('manage.users.list', {
          url: '',
          controller: 'userListCtrl',
          templateUrl: 'users/userList.tpl.html'
        })
        .state('manage.users.add', {
          url: '/add',
          controller: 'userAddCtrl',
          templateUrl: 'users/userAdd.tpl.html'
        })
        .state('manage.users.show', {
          url: '/show/:userId',
          controller: 'userShowCtrl',
          templateUrl: 'users/userShow.tpl.html'
        })
        .state('manage.users.edit', {
          url: '/edit/:userId',
          controller: 'userEditCtrl',
          templateUrl: 'users/userEdit.tpl.html'
        });
    }
  ])

  .controller(
    'manageUserCtrl',
    ['$scope', '$state', function ($scope, $state) {

      // TODO: Check permission and redirect here
      console.log("manageUserCtrl");

    }]);


// Local Variables:
// mode: javascript
// End:
