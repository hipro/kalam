// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.resources', [
  'ngResource',
  'kalam.urls'
])

  .factory(
    "UserCollection",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/users";
      return $resource(url, {},
                      {
                        query : {
                          method : 'GET',
                          isArray : false
                        }
                      });
    }])
  .factory(
    "User",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/users/:id.json";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
  .factory(
    "UserSSHKeyCollection",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/users/:loginId/sshkeys";
      return $resource(url);
    }])
  .factory(
    "UserSSHKey",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/users/:loginId/sshkeys/:keyId";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
  .factory(
    "UserProfile",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/users/:loginId/account";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
// ------------------ UI related API ---------------------------
  .factory(
    "UIUserDashboard",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "ui/user_dashboard";
      return $resource(url);
    }])

  .factory(
    "UICurrentUserInfo",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "ui/user_info";
      return $resource(url);
    }]);

// Local Variables:
// mode: javascript
// End:
