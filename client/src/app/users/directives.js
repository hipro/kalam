// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.directives', [
  'kalam.users.resources'
])

  .directive(
    'uniqueUserid',
    ['User', function(User) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                User.get(
                  { id: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity(
                      'uniqueUserid', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity(
                      'uniqueUserid', true);
                  });
              }
            });
          });
        }
      };
    }])

  .directive(
    'uniqueEmail',
    ['UserCollection', function(UserCollection) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                UserCollection.query(
                  { email: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity(
                      'uniqueEmail', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity(
                      'uniqueEmail', true);
                  });
              }
            });
          });
        }
      };
    }])

  .directive(
    'uniqueEmailExclude',
    ['UserCollection', '$parse', function(UserCollection, $parse) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {

          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              var modelGetter = $parse(attrs.uniqueEmailExclude);
              var user_id = modelGetter(scope);
              if (viewValue && user_id) {
                UserCollection.query(
                  { email : viewValue },
                  function (data) {
                    if (data.result.login ===
                        user_id) {
                      ngModelCtrl.$setValidity(
                        'uniqueEmailExclude',
                        true);
                    } else {
                      ngModelCtrl.$setValidity(
                        'uniqueEmailExclude',
                        false);
                    }
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity(
                      'uniqueEmailExclude',
                      true);
                  });
              }
            });
          });
        }
      };
    }])

  .directive(
    'sshUniqueKeyId',
    ['UserSSHKey', function(User) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                User.get(
                  {
                    loginId: scope.userId,
                    keyId: viewValue
                  },
                  function (data) {
                    ngModelCtrl.$setValidity(
                      'sshUniqueKeyId', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity(
                      'sshUniqueKeyId', true);
                  });
              }
            });
          });
        }
      };
    }])


  .directive(
    'sshKeyPattern', [ function () {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                var li = viewValue.trim().split(' ');
                if (li.length != 3) {
                  ngModelCtrl.$setValidity('sshKeyPattern', false);
                } else {
                  ngModelCtrl.$setValidity('sshKeyPattern', true);
                }
              }
            });
          });
        }
      };
    }]);


// Local Variables:
// mode: javascript
// End:
