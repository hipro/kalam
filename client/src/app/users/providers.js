// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.users.providers', [
  'kalam.users.resources'
])

  .service(
    'userInfo',
    ['UICurrentUserInfo', '$q', '$state', '$stateParams',
     function (UICurrentUserInfo, $q, $state, $stateParams) {

       var deferred;
       var userid;
       var name;
       var admin;

       // Controlls the console log of ui state related changes
       this.debug_ui_state = false;

       function load_data () {
         deferred = $q.defer();
         UICurrentUserInfo.get (
           function (data) {
             userid = data.result.login;
             name = data.result.name;
             admin = data.result.admin;
             deferred.resolve(true);
           },
           function (err_res) {
             console.log("Error occurred while getting UICurrentUserInfo");
             console.log(err_res);
             clear_data();
             deferred.reject("Unable to get UICurrentUserInfo");
           }
         );
       }

       this.initializeData = function () {
         // The promise of the deferred returned in this method will be
         // resolved only when the UIUserSession resource gets
         // resolved. So, this method is basically to ensure that we
         // have a valid data in a UI State and it should be used in
         // 'resolve' parameter of UI Router's state config
         // load_data();
         load_data();
         return deferred.promise;
       };

       function clear_data() {
         userid = null;
         name = null;
         admin = null;
       }


       this.reload = function () {

         clear_data();

         load_data();

         // $state.reload() doesn't reinstantiate controllers
         // https://github.com/angular-ui/ui-router/issues/582
         // As a workaround we do this
         // http://stackoverflow.com/a/21936701
         $state.transitionTo($state.current, $stateParams, {
           reload: true,
           inherit: false,
           notify: true
         });

       };

       this.getLoginID = function () {
         return userid;
       };

       this.getName = function () {
         return name;
       };

       this.hasManagePermission = function () {
         return admin;
       };

     }
    ]
  );


// Local Variables:
// mode: javascript
// End:
