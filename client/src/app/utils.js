// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.utils', [
  'ui.bootstrap'
])
  .factory(
    'kalamUtils', function () {
      return {
        pad: function (number) {
          if (number < 10) {
            return '0' + number;
          }
          return number;
        },
        jq: function (myid) {
          return '#' + myid.replace(/(:|\.)/g,'\\$1');
        }
      };
    })
  .controller(
    "confirmationDialogYesNoController",
    ["$scope", "$uibModalInstance", "primaryMessage", "secondaryMessage",
     function($scope, $uibModalInstance, primaryMessage, secondaryMessage) {
       $scope.primaryMessage = primaryMessage;
       $scope.secondaryMessage = secondaryMessage;
       $scope.yes = function() {
         $uibModalInstance.close("yes");
       };
       $scope.no = function() {
         $uibModalInstance.dismiss("no");
       };
     }])
  .controller(
    "messageDialogController",
    ["$scope", "$uibModalInstance", "primaryMessage", "secondaryMessage",
     function($scope, $uibModalInstance, primaryMessage, secondaryMessage) {
       $scope.primaryMessage = primaryMessage;
       $scope.secondaryMessage = secondaryMessage;
     }])

  .factory(
    'confirmationDialogYesNo',
    ['$uibModal', function($uibModal) {
      return {
        show : function(primaryMessage, secondaryMessage) {
          var modalInstance = $uibModal.open({
            template : "" +
              '<div class="modal-header">' +
              '  <h3>{{primaryMessage}}</h3>' +
              '</div>' +
              '<div class="modal-body">' +
              '  <p>{{secondaryMessage}}</p>' +
              '</div>' +
              '<div class="modal-footer">' +
              '  <button class="btn btn-primary" data-ng-click="yes()">' +
              '    Yes' +
              '  </button>' +
              '  <button class="btn btn-default" data-ng-click="no()">' +
              '    No' +
              '  </button>' +
              '</div>',
            controller : "confirmationDialogYesNoController",
            resolve : {
              'primaryMessage' : function () {
                return primaryMessage;
              },
              'secondaryMessage' : function () {
                return secondaryMessage;
              }
            }
          });
          return modalInstance.result;
        }
      };
    }])
  .factory(
    'messageDialog',
    ['$uibModal',function($uibModal) {
      return {
        show : function(primaryMessage, secondaryMessage) {
          var modalInstance = $uibModal.open({
            template : "" +
              '<div class="modal-header">' +
              '  <h3>{{primaryMessage}}</h3>' +
              '</div>' +
              '<div class="modal-body">' +
              '  <p>{{secondaryMessage}}</p>' +
              '</div>' +
              '<div class="modal-footer">' +
              '  <button class="btn btn-primary" data-ng-click="$dismiss()">' +
              '    Ok' +
              '  </button>' +
              '</div>',
            controller : "messageDialogController",
            resolve : {
              'primaryMessage' : function () {
                return primaryMessage;
              },
              'secondaryMessage' : function () {
                return secondaryMessage;
              }
            }
          });
          return modalInstance.result;
        }
      };
    }])
  .directive(
    'triStateCheckbox',
    function () {
      // Thirdparty triStateCheckbox directive from SO
      // http://stackoverflow.com/a/14820851
      return {
        replace: true,
        restrict: 'E',
        scope: { checkboxes: '=' },
        template: '<input type="checkbox" ng-model="master" ng-change="masterChange()">',
        controller: ["$scope", "$element", function($scope, $element) {
          $scope.masterChange = function() {
            if ($scope.master) {
              angular.forEach($scope.checkboxes, function(cb, index) {
                cb.isSelected = true;
              });
            } else {
              angular.forEach($scope.checkboxes, function(cb, index) {
                cb.isSelected = false;
              });
            }
          };
          $scope.$watch('checkboxes', function() {
            var allSet = true, allClear = true;
            angular.forEach($scope.checkboxes, function(cb, index) {
              if (cb.isSelected) {
                allClear = false;
              } else {
                allSet = false;
              }
            });
            if (allSet) {
              $scope.master = true;
              $element.prop('indeterminate', false);
            }
            else if (allClear) {
              $scope.master = false;
              $element.prop('indeterminate', false);
            }
            else {
              $scope.master = false;
              $element.prop('indeterminate', true);
            }
          }, true);
        }]
      };
    })
  .controller(
    "apiErrorDialogController",
    ["$scope", "$uibModalInstance", "response",
     function($scope, $uibModalInstance, response) {
       $scope.response = response;
     }])
  .factory(
    'apiErrorDialog',
    ['$uibModal', function($uibModal) {
      return {
        show : function(response) {
          var modalInstance = $uibModal.open({
            template : "" +
              '<div class="modal-header">' +
              '  <h3>API error : {{response.status}}</h3>' +
              '</div>' +
              '<div class="modal-body">' +
              '  <ul>' +
              '    <li data-ng-repeat="error in response.data.errors">' +
              '      \'{{error.location}}\', \'{{error.name}}\', ' +
              '      \'{{error.description}}\' ' +
              '    </li>' +
              '  </ul>' +
              '</div>' +
              '<div class="modal-footer">' +
              '  <button class="btn btn-primary" data-ng-click="$dismiss()">' +
              '    Ok' +
              '  </button>' +
              '</div>',
            controller : "apiErrorDialogController",
            resolve : {
              'response' : function () {
                return response;
              }
            }
          });
        }
      };
    }]);

// Local Variables:
// mode: javascript
// End:
