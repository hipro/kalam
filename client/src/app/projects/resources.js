// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.resources', [
  'ngResource',
  'kalam.urls'
])

  .factory(
    "ProjectCollection",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/projects";
      return $resource(url, {},
                      {
                        query : {
                          method : 'GET',
                          isArray : false
                        }
                      });
    }])
  .factory(
    "Project",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/projects/:id.json";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
  .factory(
    "ProjectRoleCollection",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/projects/:projectId/roles";
      return $resource(url);
    }])
  .factory(
    "ProjectRole",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() +
            "api/v1/projects/:projectId/roles/:roleId.json";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
  .factory(
    "ProjectMemberCollection",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/projects/:projectId/members";
      return $resource(url);
    }])
  .factory(
    "ProjectMember",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() +
            "api/v1/projects/:projectId/members/:loginId.json";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
//------------ UI related --------------------------------------
  .factory(
    "UIProjectDashboard",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "ui/project_dashboard/:projectId";
      return $resource(url);
    }]);


// Local Variables:
// mode: javascript
// End:
