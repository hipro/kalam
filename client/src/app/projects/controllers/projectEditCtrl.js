// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.edit', [
  'kalam.utils',
  'kalam.projects.resources'
]).controller(
  'projectEditCtrl',
  ['$scope', 'Project', '$stateParams', '$state', 'apiErrorDialog',
   function ($scope, Project, $stateParams, $state, errorDlg) {
     var project_id = $stateParams.projectId;

     if (!$scope.projectDash.can_edit_project_settings) {
       console.log("Current user doesn't have permission to edit " +
                   "project settings");
       $state.go("project.overview", {projectId: project_id});
       return;
     }

     Project.get(
       {id: project_id},
       function(data) {
         $scope.project = data.result;
       },
       function(err_res) {
         console.log("Invalid project_id : " + project_id);
         $state.go("project.overview", {projectId: project_id});
       }
     );

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return $scope.projectEditForm.$dirty && $scope.projectEditForm.$valid;
     };

     $scope.save = function () {
       if (!$scope.projectEditForm.$valid) {
         return;
       }
       Project.save(
         {id: project_id}, $scope.project,
         function (data) {
           $state.go("project.settings.show", {projectId: project_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         }
       );
     };
   }
  ]);

// Local Variables:
// mode: javascript
// End:
