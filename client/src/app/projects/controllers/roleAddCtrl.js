// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.roles.add', [
  'kalam.utils',
  'kalam.projects.resources',
  'kalam.projects.directives'
]).controller(
  'projectRoleAddCtrl',
  ['$scope', '$stateParams', '$state', 'ProjectRoleCollection',
   'apiErrorDialog',
   function ($scope, $stateParams, $state, ProjectRoleCollection, errorDlg) {
     $scope.project_id = $stateParams.projectId;

     if (!$scope.projectDash.can_manage_roles) {
       console.log("Current user doesn't have permission to manage roles");
       $state.go("project.overview", {projectId: $scope.project.short_id});
       return;
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.permissions = $state.current.data.permissions();

     var p_changed = false;

     $scope.permission_changed = function (perm) {
       p_changed = true;
       if (perm.hasChildren && !perm.isSelected) {
         // Parent permission has been unchecked so uncheck the
         // child permissions too
         for (var i in perm.permissions) {
           var p = perm.permissions[i];
           p.isSelected = false;
         }
       }
     };

     $scope.canSave = function () {
       if ($scope.roleAddForm.$dirty && $scope.roleAddForm.$valid && p_changed) {
         for (var index in $scope.permissions) {
           var perm = $scope.permissions[index];
           if (perm.isSelected) {
             return true;
           }
         }
       }
       return false;
     };

     $scope.save = function () {
       if (!$scope.roleAddForm.$valid) {
         return;
       }
       $scope.role.permissions = [];
       for (var index in $scope.permissions) {
         var perm = $scope.permissions[index];
         if (perm.isSelected) {
           $scope.role.permissions.push(perm.name);
           if (perm.hasChildren) {
             for (var i in perm.permissions) {
               var sub = perm.permissions[i];
               if (sub.isSelected) {
                 $scope.role.permissions.push(sub.name);
               }
             }
           }
         }
       }
       ProjectRoleCollection.save(
         { projectId: $scope.project_id },
         $scope.role,
         function (data) {
           $state.go("project.roles.list",
                     {projectId: $scope.project_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }]);


// Local Variables:
// mode: javascript
// End:
