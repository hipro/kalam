// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.show', [
  'kalam.projects.resources'
]).controller(
  'projectShowCtrl',
  ['$scope', 'Project', '$stateParams', '$state',
   function ($scope, Project, $stateParams, $state) {
     var project_id = $stateParams.projectId;

     Project.get(
       {id: project_id},
       function (data) {
         $scope.project = data.result;
       },
       function(err_res) {
         console.log("Invalid project_id : " + project_id);
         $state.go("project.overview", {projectId: project_id});
       });
   }
  ]);

// Local Variables:
// mode: javascript
// End:
