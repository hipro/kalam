// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.members.add', [
  'kalam.utils',
  'kalam.filters',
  'kalam.projects.resources',
  'kalam.users.resources'
]).controller(
  'projectMemberAddCtrl',
  ['$scope', 'ProjectMemberCollection', 'UserCollection', '$state',
   '$stateParams', 'apiErrorDialog', 'ProjectRoleCollection',
   function ($scope, ProjectMemberCollection, UserCollection, $state,
             $stateParams, errorDlg, ProjectRoleCollection) {
     $scope.project_id = $stateParams.projectId;

     if (!$scope.projectDash.can_manage_members) {
       console.log("Current user doesn't have permission to manage roles");
       $state.go("project.overview", {projectId: $scope.project_id});
       return;
     }

     var all_users = null;
     UserCollection.get(
       function (data) {
         all_users = data.result;
         update();
       },
       function (err_res) {
         console.log("Unable to get user collection");
       });

     ProjectRoleCollection.get(
       { projectId: $scope.project_id },
       function (data) {
         $scope.roles = data.result;
       },
       function (err_res) {
         console.log("Error while getting ProjectRoleCollection : " + err_res);
         $state.go('home');
       });


     function get_user_diff(users, members) {
       var member_id={}, diff=[];
       for (var index in members) {
         var member = members[index];
         member_id[member.login] = true;
       }
       for (var i in users) {
         var user = users[i];
         if (!member_id[user.login] && user.active) {
           diff.push(user);
         }
       }
       return diff;
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };


     function update () {
       ProjectMemberCollection.get(
         {projectId: $scope.project_id},
         function (data) {
           $scope.users = get_user_diff(all_users, data.result);
         },
         function (err_res) {
           console.log("Unable to get members for project : " +
                       $scope.project_id);
           $state.go("manage.projects.show", { projectId: $scope.project_id });
         });
     }

     $scope.cancel = function () {
       $state.go("project.members.list",
                 {projectId: $scope.project_id});
     };

     $scope.canSave = function () {
       return ($scope.memberAddForm.$dirty && $scope.memberAddForm.$valid);
     };

     $scope.save = function () {
       if (!$scope.memberAddForm.$valid) {
         return;
       }
       var data = {};
       data.login = $scope.user.login;
       if ($scope.selected_role && $scope.selected_role.short_id) {
         data.role_id = $scope.selected_role.short_id;
       }

       ProjectMemberCollection.save(
         { projectId: $scope.project_id },
         data,
         function (data) {
           // TODO: if the added member is a current user, then
           // his/her permissions might have been alerted. So, we have
           // to check the new permission and redirect him to the
           // right page
           if (data.login == $scope.userInfo.getLoginID()) {
             console.log("TODO: check permission and redirect");
             // userInfo.reload();
           }
           $state.go("project.members.list",
                     { projectId : $scope.project_id });
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };


   }]);

// Local Variables:
// mode: javascript
// End:
