// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.members.edit', [
  'kalam.utils',
  'kalam.projects.resources'
]).controller(
  'projectMemberEditCtrl',
  ['$scope', 'ProjectMember', '$state',
   '$stateParams', 'apiErrorDialog', 'ProjectRoleCollection',
   function ($scope, ProjectMember, $state,
             $stateParams, errorDlg, ProjectRoleCollection) {
     $scope.project_id = $stateParams.projectId;
     var login_id = $stateParams.loginId;

     if (!$scope.projectDash.can_manage_members) {
       console.log("Current user doesn't have permission to manage roles");
       $state.go("project.overview", {projectId: $scope.project_id});
       return;
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     ProjectMember.get(
       {
         projectId: $scope.project_id,
         loginId: login_id
       },
       function (data) {
         $scope.member = data.result;
         update();
       },
       function (err_res) {
         console.log("Invalid member id : " + login_id);
         $state.go(
           "project.members.list", { projectId: $scope.project_id });
       });


     function update() {
       ProjectRoleCollection.get(
         { projectId: $scope.project_id },
         function (data) {
           $scope.roles = data.result;
           $scope.selected_role = "(No role assigned)";
           if ($scope.member.role_id) {
             for (var index in $scope.roles) {
               var role = $scope.roles[index];
               if (role.short_id == $scope.member.role_id) {
                 $scope.selected_role = role;
               }
             }
           }
         },
         function (err_res) {
           console.log("Error while getting ProjectRoleCollection : " + err_res);
           $state.go('home');
         });
     }


     $scope.cancel = function () {
       $state.go("project.members.show",
                 {projectId: $scope.project_id, loginId: login_id});
     };

     $scope.canSave = function () {
       return ($scope.selected_role && $scope.selected_role.short_id &&
               $scope.memberEditForm.$dirty && $scope.memberEditForm.$valid);
     };

     $scope.save = function () {
       if (!$scope.memberEditForm.$valid) {
         return;
       }
       var data = {};
       if ($scope.selected_role && $scope.selected_role.short_id) {
         data.role_id = $scope.selected_role.short_id;
       }

       ProjectMember.save(
         { projectId: $scope.project_id,
           loginId: login_id},
         data,
         function (data) {
           if (login_id == $scope.userInfo.getLoginID()) {
             // If the current user's permission has been altered
             // reload the controllers so that userInfo and
             // projectDashboard get reinitialized and appropriate
             // permissions kicks in display proper links
             console.log("Current user's permission has been altered");
             $state.transitionTo("project.members.list",
                                 {projectId: $scope.project_id},
                                 { reload: true,
                                   inherit: false,
                                   notify: true
                                 });
           }
           else {
             $state.go("project.members.list",
                       { projectId : $scope.project_id });
           }
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };


   }]);

// Local Variables:
// mode: javascript
// End:
