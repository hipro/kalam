// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.members.list', [
  'kalam.utils',
  'kalam.projects.resources'
]).controller(
  'projectMemberListCtrl',
  ['$scope', '$stateParams', '$state', 'ProjectMemberCollection',
   'ProjectMember', 'confirmationDialogYesNo', 'apiErrorDialog',
   function ($scope, $stateParams, $state, ProjectMemberCollection,
             ProjectMember, confirmDlg, errorDlg) {
     $scope.project_id = $stateParams.projectId;
     var qParams = {
       projectId: $scope.project_id,
       page : 1,
       page_size: 20
     };
     $scope.pnMaxSize = 5;

     if (!$scope.projectDash.can_manage_members) {
       console.log("Current user doesn't have permission to manage roles");
       $state.go("project.overview", {projectId: $scope.project_id});
       return;
     }

     $scope.can_show_pagination = function () {
       return $scope.pnItemsPerPage < $scope.pnTotalItems;
     };

     $scope.pageChanged = function () {
       qParams.page = $scope.pnCurrentPage;
       update();
     };

     function update() {
       ProjectMemberCollection.get(
         qParams,
         function (data) {
           $scope.pnItemsPerPage = data.page_size;
           $scope.pnTotalItems = data.total_size;
           $scope.pnCurrentPage = data.current_page;

           $scope.members = data.result;
         },
         function (err_res) {
           console.log(
             "Unable to get members for project (" + $scope.project_id +
               ")");
           $state.go("home");
         });
     }

     $scope.removeMember = function (userid) {
       var result = confirmDlg.show(
         "Remove member?",
         "The user '" + userid + "' will have no access to the project. " +
           "However, previous interactions made by the user will not be " +
           "removed. Do you want to continue?");
       result.then(function () {
         ProjectMember.remove(
           {
             projectId: $scope.project_id,
             loginId: userid
           },
           function (data) {
             if (userid == $scope.userInfo.getLoginID()) {
               // I am removed from the project goto home
               $state.go('home');
             } else {
               update();
             }
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };

     update();

   }]);


// Local Variables:
// mode: javascript
// End:
