// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.roles.edit', [
  'kalam.utils',
  'kalam.projects.resources'
]).controller(
  'projectRoleEditCtrl',
  ['$scope', '$stateParams', '$state', 'ProjectRole', 'apiErrorDialog',
   function ($scope, $stateParams, $state, ProjectRole, errorDlg) {
     $scope.project_id = $stateParams.projectId;

     if (!$scope.projectDash.can_manage_roles) {
       console.log("Current user doesn't have permission to manage roles");
       $state.go("project.overview", {projectId: $scope.projectDash.short_id});
       return;
     }

     var role_id = $stateParams.roleId;

     $scope.permissions = $state.current.data.permissions();

     ProjectRole.get(
       { projectId : $scope.project_id,
         roleId : role_id
       },
       function (data) {
         $scope.role = data.result;

         for (var index in data.result.permissions) {
           var perm = data.result.permissions[index];
           for (var i in $scope.permissions) {
             var lperm = $scope.permissions[i];
             if (lperm.name == perm) {
               lperm.isSelected = true;
             }
             if (lperm.hasChildren && lperm.isSelected) {
               for (var j in lperm.permissions) {
                 var sub = lperm.permissions[j];
                 if (sub.name == perm) {
                   sub.isSelected = true;
                 }
               }
             }
           }
         }

       },
       function (err_res) {
         console.log("Invalid role ID : " + role_id);
         $state.go('project.role.list',
                   {projectId: $scope.project_id});
       });

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       if ($scope.roleEditForm.$dirty && $scope.roleEditForm.$valid) {
         for (var index in $scope.permissions) {
           var perm = $scope.permissions[index];
           if (perm.isSelected) {
             return true;
           }
         }
       }
       return false;
     };

     $scope.save = function () {
       if (!$scope.roleEditForm.$valid) {
         return;
       }
       $scope.role.permissions = [];
       for (var index in $scope.permissions) {
         var perm = $scope.permissions[index];
         if (perm.isSelected) {
           $scope.role.permissions.push(perm.name);
           if (perm.hasChildren) {
             for (var i in perm.permissions) {
               var sub = perm.permissions[i];
               if (sub.isSelected) {
                 $scope.role.permissions.push(sub.name);
               }
             }
           }
         }
       }

       ProjectRole.save(
         { projectId : $scope.project_id,
           roleId : role_id
         },
         $scope.role,
         function (data) {
           $state.go("project.roles.show",
                     {projectId: $scope.project_id, roleId: role_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }]);


// Local Variables:
// mode: javascript
// End:
