// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.roles.show', [
  'kalam.utils',
  'kalam.projects.resources'
]).controller(
  'projectRoleShowCtrl',
  ['$scope', '$stateParams', '$state', 'ProjectRole',
   function ($scope, $stateParams, $state, ProjectRole) {
     $scope.project_id = $stateParams.projectId;

     if (!$scope.projectDash.can_manage_roles) {
       console.log("Current user doesn't have permission to manage roles");
       $state.go("project.overview", {projectId: $scope.project.short_id});
       return;
     }

     $scope.permissions = $state.current.data.permissions();

     var role_id = $stateParams.roleId;

     ProjectRole.get(
       {
         projectId: $scope.project_id,
         roleId: role_id
       },
       function (data) {
         $scope.role = data.result;

         // reconstruct UI specific permissions list
         var permissions = [];
         for (var index in data.result.permissions) {
           var perm = data.result.permissions[index];
           for (var i in $scope.permissions) {
             var lperm = $scope.permissions[i];
             if (lperm.hasChildren) {
               // Treat the node with children differently
               if (lperm.name == perm) {
                 permissions.push(lperm.display);
               }
               for (var j in lperm.permissions) {
                 var sub = lperm.permissions[j];
                 if (sub.name == perm) {
                   permissions.push(sub.title);
                 }
               }
             }
             else {
               if (lperm.name == perm) {
                 permissions.push(lperm.title);
               }
             }
           }
         }
         $scope.role.permissions = permissions;

       },
       function (err_res) {
         console.log("Invalid role id : " + role_id);
         $state.go("project.roles.list", { projectId: $scope.project_id });
       }
     );
   }
  ]);



// Local Variables:
// mode: javascript
// End:
