// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.add', [
  'kalam.utils',
  'kalam.projects.resources',
  'kalam.projects.directives'
]).controller(
  'projectAddCtrl',
  ['$scope', 'ProjectCollection', '$state', 'apiErrorDialog',
   function ($scope, ProjectCollection, $state, errorDlg) {

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.save = function () {
       if (!$scope.projectAddForm.$valid) {
         return;
       }

       ProjectCollection.save(
         $scope.project,
         function (data) {
           // if (userInfo.getID() == $scope.user.id) {
           //   // Current user is added as a project admin so reload the
           //   // userInfo to get the updated permissions
           //   userInfo.reload();
           // }
           $state.go("manage.projects.list");
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     $scope.canSave = function () {
       return $scope.projectAddForm.$dirty && $scope.projectAddForm.$valid;
     };

   }
  ]);

// Local Variables:
// mode: javascript
// End:
