// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.list', [
  'kalam.utils',
  'kalam.projects.resources'
]).controller(
  'projectListCtrl',
  ['$scope', 'ProjectCollection', 'Project',
   'confirmationDialogYesNo', 'apiErrorDialog',
   function ($scope, ProjectCollection, Project, confirmDlg, errorDlg) {
     var qParams = {
       page : 1,
       page_size: 20
     };
     $scope.pnMaxSize = 5;

     function update () {
       ProjectCollection.get(
         qParams,
         function (data) {
           $scope.pnItemsPerPage = data.page_size;
           $scope.pnTotalItems = data.total_size;
           $scope.pnCurrentPage = data.current_page;

           $scope.projects = data.result;
       });
     }

     $scope.can_show_pagination = function () {
       return $scope.pnItemsPerPage < $scope.pnTotalItems;
     };

     $scope.pageChanged = function () {
       qParams.page = $scope.pnCurrentPage;
       update();
     };

     $scope.deleteProject = function(short_id) {
       var result = confirmDlg.show(
         "Delete project?",
         "All the information associated with the project '" + short_id + "' " +
           "will also be deleted. Do you want to continue?");
       result.then(function () {
         Project.remove(
           {id: short_id},
           function (data) {
             // if (userInfo.isProjectMember(project_id)) { // If the
             //   current logged-in user is a member of the // deleted
             //   project then reload the userInfo userInfo.reload();
             //   } else { update(); }

             // TODO: calculate the page_size and check if the current
             // page would be valid after deletion if not change to
             // page-1
             qParams.page = 1;
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };
     update();
   }
  ]);

// Local Variables:
// mode: javascript
// End:
