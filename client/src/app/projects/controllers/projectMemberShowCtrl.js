// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.controllers.members.show', [
  'kalam.utils',
  'kalam.projects.resources'
]).controller(
  'projectMemberShowCtrl',
  ['$scope', 'ProjectMember', '$stateParams', '$state',
   function ($scope, ProjectMember, $stateParams, $state) {
     $scope.project_id = $stateParams.projectId;

     var login_id = $stateParams.loginId;

     if (!$scope.projectDash.can_manage_members) {
       console.log("Current user doesn't have permission to manage roles");
       $state.go("project.overview", {projectId: $scope.project_id});
       return;
     }

     ProjectMember.get(
       {
         projectId: $scope.project_id,
         loginId: login_id
       },
       function (data) {
         $scope.member = data.result;
       },
       function(err_res) {
         console.log("Invalid member id : " + login_id);
         $state.go("project.members.list",
                   {projectId: $scope.project_id});
       });
   }]);


// Local Variables:
// mode: javascript
// End:
