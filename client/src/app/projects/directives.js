// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.directives', [
  'kalam.projects.resources'
])

  .directive(
    'uniqueProjectId',
    ['Project', function(Project) {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                Project.get(
                  { id: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity('uniqueProjectId', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity('uniqueProjectId', true);
                  });
              }
            });
          });
        }
      };
    }])

  .directive(
    'uniqueRoleId',
    ['ProjectRole',
     function(ProjectRole) {
       return {
         require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
           element.on('blur', function (event) {
             scope.$apply(function () {
               var viewValue = element.val();
               if (viewValue) {
                 ProjectRole.get(
                   { projectId: scope.project_id,
                     roleId: viewValue },
                   function (data) {
                     ngModelCtrl.$setValidity('uniqueRoleId', false);
                   },
                   function (err_res) {
                     ngModelCtrl.$setValidity('uniqueRoleId', true);
                   });
               }
             });
           });
         }
       };
     }])

  .directive(
    'uniqueShortIdExclude',
    ['Project', '$parse', function(Project, $parse) {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              var modelGetter = $parse(attrs.uniqueShortIdExclude);
              var exclude_project_id = modelGetter(scope);
              if (viewValue) {
                Project.get(
                  { id: viewValue },
                  function (data) {
                    // There exists a project having the given short_id
                    if (data.result.short_id == exclude_project_id) {
                      // This project is excluded so the given value is valid
                      ngModelCtrl.$setValidity('uniqueShortIdExclude', true);
                    }
                    else {
                      ngModelCtrl.$setValidity('uniqueShortIdExclude', false);
                    }
                  },
                  function (err_res) {
                    // There is no such project having the given
                    // short_id so the value is valid
                    ngModelCtrl.$setValidity('uniqueShortIdExclude', true);
                  });
              }
            });
          });
        }
      };
    }]);




// Local Variables:
// mode: javascript
// End:
