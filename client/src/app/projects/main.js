// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects', [
  'ui.router',
  'kalam.projects.providers',
  'kalam.projects.controllers.roles.list',
  'kalam.projects.controllers.roles.add',
  'kalam.projects.controllers.roles.show',
  'kalam.projects.controllers.roles.edit',
  'kalam.projects.controllers.members.list',
  'kalam.projects.controllers.members.add',
  'kalam.projects.controllers.members.show',
  'kalam.projects.controllers.members.edit'
])

  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('project', {
        url: '/project/:projectId',
        parent: 'two-part-page',
        resolve: {
          // A string value resolves to a service
          projectDashboard: 'projectDashboard',
          // We don't use the 'gotData' in the controller as this is
          // used to ensure that the projectDashboard gets its data filled
          // from the $resource
          gotData: [
            "projectDashboard", '$stateParams',
            function (projectDashboard, $stateParams) {
              var project_id = $stateParams.projectId;
              return projectDashboard.initializeData(project_id);
            }]
        },

        views: {
          "sidebar": {
            controller: 'projectDashboardCtrl',
            templateUrl: 'twoPartMenuLinks.tpl.html'
          },
          "breadcrumbs@page": {
            controller: 'projectBreadcrumbCtrl',
            templateUrl: 'breadcrumbs.tpl.html'
          },
          "": {
            controller: 'projectDashboardCtrl',
            template: "<div data-ui-view>Project Dashboard {{ switchState() }}</div>"
          }
        }
      })
    // -------------- States for project dashboard ---------------
      .state('project.overview', {
        url: '',
        controller: 'projectOverviewCtrl',
        templateUrl: 'projects/overview.tpl.html'
      })
      .state('project.settings', {
        url: '/settings',
        abstract: true,
        template: '<ui-view/>'
      })
      .state('project.settings.show', {
        url: '',
        controller: 'projectShowCtrl',
        templateUrl: 'projects/projectShow.tpl.html'
      })
      .state('project.settings.edit', {
        url: '/edit',
        controller: 'projectEditCtrl',
        templateUrl: 'projects/projectEdit.tpl.html'
      })
    // --------------- States for project dashboard roles ---------------
      .state('project.roles', {
        url: '/roles',
        abstract: true,
        template: '<ui-view/>',
        data: {
          permissions: function () {
            return [
              { name : 'ps:projects.manage', title : 'Change project settings', isSelected : false, hasChildren: false},
              { name : 'ps:project_members.manage', title : 'Manage members', isSelected : false, hasChildren: false},
              { name : 'ps:project_roles.manage', title : 'Manage roles', isSelected : false, hasChildren: false},
              { name : 'ps:repositories.view-meta', title : 'Repositores', isSelected : false, hasChildren: true, display: 'View repositories',
                permissions: [
                  {name: 'ps:repositories.create-new', title: 'Create/Fork repositories', isSelected: false, tooltip: ''},
                  {name: 'ps:repositories.read-all', title: 'Read from all repositories', isSelected: false, tooltip: 'Enables read rights (git pull) on all repositories'},
                  {name: 'ps:repositories.write-all', title: 'Write to all repositories', isSelected: false, tooltip: 'Enables write rights (git push) on all repositories'},
                  {name: 'ps:repositories.manage-all', title: 'Manage all repositories', isSelected: false, tooltip: 'Enables manage rights on all repositories'}
                ]
              }
            ];
          }
        }
      })
      .state('project.roles.list', {
        url: '',
        controller: 'projectRoleListCtrl',
        templateUrl: 'projects/roleList.tpl.html'
      })
      .state('project.roles.add', {
        url: '/add',
        controller: 'projectRoleAddCtrl',
        templateUrl: 'projects/roleAdd.tpl.html'
      })
      .state('project.roles.show', {
        url: '/show/:roleId',
        controller: 'projectRoleShowCtrl',
        templateUrl: 'projects/roleShow.tpl.html'
      })
      .state('project.roles.edit', {
        url: '/edit/:roleId',
        controller: 'projectRoleEditCtrl',
        templateUrl: 'projects/roleEdit.tpl.html'
      })
    // --------------- States for project dashboard members ---------------
      .state('project.members', {
        url: '/members',
        abstract: true,
        template: '<ui-view/>'
      })
      .state('project.members.list', {
        url: '',
        controller: 'projectMemberListCtrl',
        templateUrl: 'projects/memberList.tpl.html'
      })
      .state('project.members.add', {
        url: '/add',
        controller: 'projectMemberAddCtrl',
        templateUrl: 'projects/memberAdd.tpl.html'
      })
      .state('project.members.show', {
        url: '/show/:loginId',
        controller: 'projectMemberShowCtrl',
        templateUrl: 'projects/memberShow.tpl.html'
      })
      .state('project.members.edit', {
        url: '/show/:loginId',
        controller: 'projectMemberEditCtrl',
        templateUrl: 'projects/memberEdit.tpl.html'
      });

    }])

  .controller(
    'projectDashboardCtrl',
    ['$scope', '$state', '$stateParams', 'projectDashboard',
     function ($scope, $state, $stateParams, projectDashboard) {

       var project_id = $stateParams.projectId;

       $scope.title = "Project";
       $scope.project = null;

       $scope.links = [
         {
           name: 'Overview',
           bref: 'project.overview',
           sref: 'project.overview'
         },
         {
           name: 'Settings',
           bref: 'project.settings',
           sref: 'project.settings.show'
         }
       ];

       $scope.projectDash = projectDashboard.getData();
       update();

       function update() {
         if ($scope.projectDash.can_view_repositories) {
           $scope.links.push({
             name: 'Repositories',
             bref: 'repositories',
             sref: 'repositories.list'
           });
         }

         if ($scope.projectDash.can_manage_roles) {
           $scope.links.push({
             name: 'Roles',
             bref: 'project.roles',
             sref: 'project.roles.list'
           });
         }

         if ($scope.projectDash.can_manage_members) {
           $scope.links.push({
             name: 'Members',
             bref: 'project.members',
             sref: 'project.members.list'
           });
         }

       }

       $scope.switchState = function () {
         // TODO: check the permission and do the switching
         if ($state.is('project') &&
             !$state.is('project.overview')) {
           $state.go('project.overview', {projectId: project_id});
           console.log("switching state to project.overview");
         }
       };

       $scope.switchState();
     }])

  .controller(
    'projectOverviewCtrl',
    ['$scope', '$state', '$stateParams',
     function ($scope, $state, $stateParams) {
       var project_id = $stateParams.projectId;

       // UIProjectOverview.get(
       //   {projectId: project_id},
       //   function (data) {
       //     $scope.project = data.result;
       //   },
       //   function(err_res) {
       //     console.log("Invalid project_id : " + project_id);
       //     $state.go("home");
       //   });

       $scope.title = "Overview of project " + project_id;

     }])

  .controller(
    'projectBreadcrumbCtrl',
    ['$scope', '$state', '$stateParams',
     function ($scope, $state, $stateParams) {
       var project_id = $stateParams.projectId;

       $scope.breadcrumbs = [
         { title: 'Home', activePage: false, url: $state.href('home') },
         { title: 'Project [' + project_id + ']',
           activePage: $state.is('project.overview'),
           url: $state.href('project.overview',
                            {projectId: project_id})
         }
       ];

     }]);


// Local Variables:
// mode: javascript
// End:
