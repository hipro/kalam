// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.projects.manage', [
  'ui.router',
  'kalam.projects.controllers.list',
  'kalam.projects.controllers.add',
  'kalam.projects.controllers.show',
  'kalam.projects.controllers.edit'
])

  .config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('manage.projects', {
          url: '/projects',
          abstract: true,
          template: '<ui-view/>',
          controller: 'manageProjectCtrl'
        })
        // ----------- States for managing Project --------------
        // Make this state as default by making the url as '' and its
        // parent as abstract
        .state('manage.projects.list', {
          url: '',
          controller: 'projectListCtrl',
          templateUrl: 'projects/projectList.tpl.html'
        })
        .state('manage.projects.add', {
          url: '/add',
          controller: 'projectAddCtrl',
          templateUrl: 'projects/projectAdd.tpl.html'
        });
    }
  ])

  .controller(
    'manageProjectCtrl',
    ['$scope', '$state', function ($scope, $state) {

      // TODO: Check permission and redirect here
      console.log("manageProjectCtrl");

    }]);


// Local Variables:
// mode: javascript
// End:
