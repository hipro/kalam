// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories', [
  'ui.router',
  'kalam.projects',
  'kalam.repositories.providers',
  'kalam.repositories.controllers.list',
  'kalam.repositories.controllers.add',
  'kalam.repositories.controllers.show',
  'kalam.repositories.controllers.edit',
  'kalam.repositories.controllers.acl',
  'kalam.repositories.controllers.fork'
])

  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('repositories', {
        url: '/repositories',
        parent: 'project',
        abstract: true,
        template: '<ui-view/>'
      })
      .state('repositories.list', {
        url: '',
        controller: 'repoListCtrl',
        templateUrl: 'repositories/repoList.tpl.html'
      })
      .state('repositories.add', {
        url: '/add',
        controller: 'repoAddCtrl',
        templateUrl: 'repositories/repoAdd.tpl.html'
      })

    // ---------- Repo Home related states and sub-states --------------
      .state('repository', {
        url: '/project/:projectId/repository/:repoId',
        parent: 'two-part-page',
        resolve: {
          // A string value resolves to a service
          repoDashboard: 'repoDashboard',
          // We don't use the 'gotData' in the controller as this is
          // used to ensure that the projectDashboard gets its data filled
          // from the $resource
          gotData: [
            "repoDashboard", '$stateParams',
            function (repoDashboard, $stateParams) {
              var project_id = $stateParams.projectId;
              var repo_id = $stateParams.repoId;
              return repoDashboard.initializeData(project_id, repo_id);
            }]
        },
        views: {
          "sidebar": {
            controller: 'repoHomeCtrl',
            templateUrl: 'twoPartMenuLinks.tpl.html'
          },
          "breadcrumbs@page": {
            controller: 'repoBreadcrumbCtrl',
            templateUrl: 'breadcrumbs.tpl.html'
          },
          "": {
            controller: 'repoHomeCtrl',
            template: "<div data-ui-view>Repo Home {{ switchState() }}</div>"
          }
        }
      })
      .state('repository.overview', {
        abstract: true,
        template: '<div data-ui-view></div>'
      })
      .state('repository.overview.show',  {
        url: '',
        controller: 'repoOverviewCtrl',
        templateUrl: 'repositories/overview.tpl.html'
      })
      .state('repository.overview.fork', {
        url: '/fork',
        controller: 'repoForkCtrl',
        templateUrl: 'repositories/repoFork.tpl.html'
      })
      //----------------- Repo Settings related states -------------------
      .state('repository.settings', {
        url: '/settings',
        abstract: true,
        template: '<ui-view/>'
      })
      .state('repository.settings.show', {
        url: '',
        controller: 'repoShowCtrl',
        templateUrl: 'repositories/repoShow.tpl.html'
      })
      .state('repository.settings.edit', {
        url: '/edit',
        controller: 'repoEditCtrl',
        templateUrl: 'repositories/repoEdit.tpl.html'
      })
      //------------ Repo Access management -----------------------------
      .state('repository.access', {
        url: '/access',
        controller: 'repoAccessCtrl',
        templateUrl: 'repositories/repoAccess.tpl.html'
      });
  }])

  .controller(
    'repoHomeCtrl',
    ['$scope', '$state', '$stateParams', 'repoDashboard',
     function ($scope, $state, $stateParams, repoDashboard) {

       var project_id = $stateParams.projectId;
       var repo_id = $stateParams.repoId;

       $scope.title = "Repository";
       $scope.links = [
         {
           name: 'Overview',
           bref: 'repository.overview',
           sref: 'repository.overview.show'
         },
         {
           name: 'Settings',
           bref: 'repository.settings',
           sref: 'repository.settings.show'
         }];
       $scope.repoDash = repoDashboard.getData();
       update();

       function update() {
         if ($scope.repoDash.can_manage_permissions) {
           $scope.links.push({
             name: 'Access management',
             bref: 'repository.access',
             sref: 'repository.access'
           });
         }

       }

       $scope.switchState = function () {
         // TODO: check the permission and do the switching
         if ($state.is('repository') &&
             !$state.is('repository.overview.show')) {
           $state.go('repository.overview.show', {projectId: project_id,
                                                  repoId: repo_id});
           console.log('Switching to repository.overview.show');
         }
       };

       $scope.switchState();
     }])

  .controller(
    'repoOverviewCtrl',
    ['$scope', '$state', '$stateParams',
     function ($scope, $state, $stateParams) {

       $scope.project_id = $stateParams.projectId;
       var repo_id = $stateParams.repoId;

       $scope.title = "Overview of repo " + repo_id + " of project " +
         $scope.project_id;

     }])

  .controller(
    'repoBreadcrumbCtrl',
    ['$scope', '$state', '$stateParams',
     function ($scope, $state, $stateParams) {
       var project_id = $stateParams.projectId;
       var repo_id = $stateParams.repoId;

       $scope.breadcrumbs = [
         { title: 'Home', activePage: false, url: $state.href('home') },
         { title: 'Project [' + project_id + ']', activePage: false,
           url: $state.href('project.overview', {projectId: project_id}) },
         { title: 'Repository [' + repo_id + ']',
           activePage: $state.is('repository.overview'),
           url: $state.href('repository.overview',
                            {projectId: project_id, repoId: repo_id}) }
       ];

     }]);


// Local Variables:
// mode: javascript
// End:
