// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.resources', [
  'ngResource',
  'kalam.urls',
])

  .factory(
    "RepositoryCollection",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() + "api/v1/projects/:projectId/repositories";
      return $resource(url);
    }])

  .factory(
    "Repository",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() +
            "api/v1/projects/:projectId/repositories/:repoId.json";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])

  .factory(
    "RepositoryACLCollection",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() +
            "api/v1/projects/:projectId/repositories/:repoId/acl";
      return $resource(url);
    }])

  .factory(
    "RepositoryACL",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() +
            "api/v1/projects/:projectId/repositories/:repoId/acl/:loginId";
      return $resource(url, {},
                       {
                         save : {
                           method : 'PUT'
                         }
                       });
    }])
//------------ UI related API -------------------------------------------
  .factory(
    "UIRepoDashboard",
    ['$resource', 'kalamURLs', function ($resource, kalamURLs) {
      var url = kalamURLs.baseURL() +
            "ui/repository_dashboard/:projectId/:repoId";
      return $resource(url);
    }]);

// Local Variables:
// mode: javascript
// End:
