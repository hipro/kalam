// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.providers', [
  'kalam.repositories.resources'
])

  .service(
    'repoDashboard',
    ['$q', '$state', '$stateParams', 'UIRepoDashboard',
     function ($q, $state, $stateParams, UIRepoDashboard) {

       var deferred;
       var repository;

       function clear_data() {
         repository = null;
       }

       function load_data(project_id, repo_id) {
         deferred = $q.defer();
         UIRepoDashboard.get(
           { projectId: project_id, repoId: repo_id },
           function (data) {
             repository = data.result;
             deferred.resolve(true);
           },
           function (err_res) {
             console.log("Error occurred while getting UIRepoDashboard");
             console.log(err_res);
             deferred.reject("Unable t get UIRepoDashboard");
             $state.go('home');
           }
         );
       }

       this.initializeData = function (project_id, repo_id) {
         // The promise of the deferred returned in this method will be
         // resolved only when the UIRepoDashboard resource gets
         // resolved. So, this method is basically to ensure that we
         // have a valid data in a UI State and it should be used in
         // 'resolve' parameter of UI Router's state config
         load_data(project_id, repo_id);
         return deferred.promise;
       };

       this.reload = function (project_id, repo_id) {
         clear_data();

         load_data(project_id, repo_id);

         // $state.reload() doesn't reinstantiate controllers
         // https://github.com/angular-ui/ui-router/issues/582
         // As a workaround we do this
         // http://stackoverflow.com/a/21936701
         $state.transitionTo($state.current, $stateParams, {
           reload: true,
           inherit: false,
           notify: true
         });

       };

       this.getData = function () {
         return repository;
       };
     }
    ]
  );




// Local Variables:
// mode: javascript
// End:
