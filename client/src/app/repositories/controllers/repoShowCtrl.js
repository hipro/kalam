// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.controllers.show', [
  'ngSanitize',
  'kalam.utils',
  'kalam.repositories.resources'
]).controller(
  'repoShowCtrl',
  ['$scope', '$stateParams', '$state', 'Repository',
   function ($scope, $stateParams, $state, Repository) {
     $scope.project_id = $stateParams.projectId;

     var repo_id = $stateParams.repoId;

     Repository.get(
       {
         projectId: $scope.project_id,
         repoId: repo_id
       },
       function (data) {
         $scope.repository = data.result;
         $scope.html_description = data.result.description.replace(/\n/g, '<br />');
       },
       function (err_res) {
         console.log("Invalid repo ID : " + repo_id);
         $state.go("repositories.list", { projectId: $scope.project_id});
       });

   }]);

// Local Variables:
// mode: javascript
// End:
