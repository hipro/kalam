// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.controllers.list', [
  'kalam.utils',
  'kalam.repositories.resources'
]).controller(
  'repoListCtrl',
  ['$scope', '$stateParams', '$state', 'RepositoryCollection', 'Repository',
   'confirmationDialogYesNo', 'apiErrorDialog',
   function ($scope, $stateParams, $state, RepositoryCollection, Repository,
             confirmDlg, errorDlg) {
     $scope.project_id = $stateParams.projectId;

     var qParams = {
       projectId: $scope.project_id,
       page : 1,
       page_size: 20
     };
     $scope.pnMaxSize = 5;

     if (!$scope.projectDash.can_view_repositories) {
       console.log("Current user doesn't have permission to view repositories");
       $state.go("project.overview", {projectId: $scope.project_id});
       return;
     }

     $scope.can_show_pagination = function () {
       return $scope.pnItemsPerPage < $scope.pnTotalItems;
     };

     $scope.pageChanged = function () {
       qParams.page = $scope.pnCurrentPage;
       update();
     };

     function update() {
       RepositoryCollection.get(
         qParams,
         function (data) {
           $scope.pnItemsPerPage = data.page_size;
           $scope.pnTotalItems = data.total_size;
           $scope.pnCurrentPage = data.current_page;

           $scope.repositories = data.result;
         },
         function (err_res) {
           console.log("Error while getting repo list for project " +
                       $scope.project_id);
           $state.go('home');
         });
     }

     $scope.deleteRepository = function (repo_id) {
       var result = confirmDlg.show(
         'Delete repository?',
         "This will delete the repository '" + repo_id + "', however " +
           "repositories forked from this will not be deleted. " +
           "Do you want to continue?");
       result.then(function () {
         Repository.remove(
           {
             projectId: $scope.project_id,
             repoId: repo_id
           },
           function (data) {
             // TODO: calculate the page_size and check if the current
             // page would be valid after deletion if not change to
             // page-1
             qParams.page = 1;

             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
             update();
           });
       });
     };

     update();

   }]);



// Local Variables:
// mode: javascript
// End:
