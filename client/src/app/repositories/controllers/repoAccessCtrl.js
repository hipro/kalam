// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.controllers.acl', [
  'kalam.utils',
  'kalam.filters',
  'kalam.repositories.resources',
  'kalam.projects.resources'
]).controller(
  'repoAccessCtrl',
  ['$scope', '$stateParams', '$state', 'RepositoryACLCollection',
   'RepositoryACL', 'ProjectMemberCollection', 'apiErrorDialog',
   'confirmationDialogYesNo',
   function ($scope, $stateParams, $state, RepositoryACLCollection,
             RepositoryACL, ProjectMemberCollection, errorDlg, confirmDlg) {
     $scope.project_id = $stateParams.projectId;

     $scope.action = "Add";

     var repo_id = $stateParams.repoId;
     var can_add = false;
     $scope.permissions = ['read', 'write', 'manage'];
     $scope.permission = $scope.permissions[0];

     if (!$scope.repoDash.can_manage_permissions) {
       console.log("Current user doesn't have permission to manage ACL");
       $state.go('repository.overview.show', {projectId: $scope.project_id,
                                              repoId: repo_id});
       return;
     }

     ProjectMemberCollection.get(
       { projectId: $scope.project_id },
       function (data) {
         $scope.users = data.result;
         update();
       },
       function (err_res) {
         console.log("Error while getting UserCollection");
         $state.go('home');
         return;
       });

     function update() {
       RepositoryACLCollection.get(
         { projectId: $scope.project_id,
           repoId: repo_id
         },
         function (data) {
           $scope.members = data.result;
         },
         function (err_res) {
           console.log("Error while fetching Repository ACL list");
           console.log(err_res);
           $state.go('repository.overview.show', {projectId: $scope.project_id,
                                                  repoId: repo_id});
         });
     }

     $scope.change_permission = function (login_id, permission) {
       RepositoryACL.save(
         { projectId: $scope.project_id,
           repoId: repo_id,
           loginId: login_id
         },
         { ui_permission: permission },
         function (data) {
           // Update the member listing
           $scope.members = data.result;
           if (($scope.userInfo.getLoginID() == login_id) &&
               permission != 'manage') {
             console.log("Current user's permission changed");
             // reload the state
             $state.transitionTo($state.current, $stateParams, {
               reload: true,
               inherit: true,
               notify: true
             });
           }
         },
         function (err_res) {
           console.log("Error while updating Repository ACL");
           errorDlg.show(err_res);
         });
     };

     $scope.canAdd = function () {
       // If we have a name property in filter_user that means
       // that we have choosen the right user from the
       // typeahead widget
       return (can_add && $scope.filter_user && $scope.filter_user.name);
     };

     $scope.onUserSelected = function () {
       can_add = false;
       for (var i in $scope.members) {
         var member = $scope.members[i];
         if (member.login == $scope.filter_user.login) {
           $scope.action = "Update";
           for (var j in $scope.permissions) {
             var perm = $scope.permissions[j];
             if (perm == member.ui_permission) {
               $scope.permission = perm;
               can_add = true;
               return;
             }
           }
           // ui_permission doesn't match our permission list so
           // return while can_add is false itself
           console.log(member.login + " has invalid permission " +
                       member.ui_permission);
           return;
         }
       }
       // The selected user is not yet an member so set the can_add as
       // true and reset the permission to first index ('read')
       $scope.action = "Add";
       can_add = true;
       $scope.permission = $scope.permissions[0];
     };

     $scope.removeAccess = function (login_id) {
       if ($scope.userInfo.getLoginID() == login_id) {
         console.log("Cannot remove access for current logged-in user");
         return;
       }
       var result = confirmDlg.show(
         'Remove access?',
         "This will remove all access for  '" + login_id + "', in this " +
           "repository. Do you want to continue?");
       result.then(function () {
         RepositoryACL.remove(
           {
             projectId: $scope.project_id,
             repoId: repo_id,
             loginId: login_id
           },
           function (data) {
             update();
           },
           function (err_res) {
             errorDlg.show(err_res);
           });
       });
     };

     $scope.addAccess = function () {
       if (!$scope.canAdd()) {
         return;
       }

       if ($scope.action == "Update") {
         $scope.change_permission($scope.filter_user.login, $scope.permission);
         $scope.permission = $scope.permissions[0];
         $scope.filter_user = null;
         $scope.action = "Add";
         return;
       }

       RepositoryACLCollection.save (
         { projectId: $scope.project_id,
           repoId: repo_id
         },
         { login: $scope.filter_user.login,
           ui_permission: $scope.permission
         },
         function (data) {
           $scope.members = data.result;
           can_add = false;
           $scope.filter_user = null;
           $scope.permission = $scope.permissions[0];
         },
         function (err_res) {
           errorDlg.show(err_res);
         });

     };
   }]);


// Local Variables:
// mode: javascript
// End:
