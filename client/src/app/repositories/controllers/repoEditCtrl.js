// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.controllers.edit', [
  'kalam.utils',
  'kalam.repositories.resources'
]).controller(
  'repoEditCtrl',
  ['$scope', '$stateParams', '$state', 'Repository', 'apiErrorDialog',
   function ($scope, $stateParams, $state, Repository, errorDlg) {
     $scope.project_id = $stateParams.projectId;

     var repo_id = $stateParams.repoId;

     if (!$scope.repoDash.can_edit_settings) {
       console.log("Current user doesn't have permission to edit settings");
       $state.go('repository.overview.show', {projectId: $scope.project_id,
                                              repoId: repo_id});
       return;
     }

     Repository.get(
       { projectId: $scope.project_id,
         repoId: repo_id
       },
       function (data) {
         $scope.repository = data.result;
       },
       function (err_res) {
         console.log("Unable to get repository '" + repo_id + "' for project " +
                     $scope.project_id);
         $state.go('repository.overview.show', {projectId: $scope.project_id,
                                                repoId: repo_id});
       });


     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return ($scope.repoEditForm.$dirty && $scope.repoEditForm.$valid);
     };

     $scope.save = function () {
       if (!$scope.repoEditForm.$valid) {
         return;
       }

       Repository.save(
         { projectId: $scope.project_id,
           repoId: repo_id
         },
         $scope.repository,
         function (data) {
           $state.go('repository.settings.show',
                     { projectId: $scope.project_id, repoId: repo_id });
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };

     }]);


// Local Variables:
// mode: javascript
// End:
