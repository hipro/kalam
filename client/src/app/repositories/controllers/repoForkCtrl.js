// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.controllers.fork', [
  'kalam.utils',
  'kalam.repositories.resources'
]).controller(
  'repoForkCtrl',
  ['$scope', '$stateParams', '$state', 'Repository', 'RepositoryCollection',
   'apiErrorDialog',
   function ($scope, $stateParams, $state, Repository, RepositoryCollection,
             errorDlg) {
     $scope.project_id = $stateParams.projectId;

     var repo_id = $stateParams.repoId;

     // TODO: check the permission from $scope.repoDash

     // Pre-fill the form fields via data-binding
     Repository.get(
       { projectId: $scope.project_id,
         repoId: repo_id
       },
       function (data) {
         $scope.repository = {
           fork_from_repo_id: repo_id,
           short_id: '',
           name: data.result.name,
           description: "Forked from '" + repo_id + "'\n" +
             data.result.description
         };
       },
       function (err_res) {
         console.log("Unable to get repository '" + repo_id + "' for project " +
                     $scope.project_id);
         $state.go('repository.overview.show', {projectId: $scope.project_id,
                                                repoId: repo_id});
       });


     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return ($scope.repoForkForm.$dirty && $scope.repoForkForm.$valid);
     };

     $scope.save = function () {
       if (!$scope.repoForkForm.$valid) {
         return;
       }
       RepositoryCollection.save(
         { projectId: $scope.project_id },
         $scope.repository,
         function (data) {
           var repo = data.result;
           $state.go('repository.overview.show', {projectId: $scope.project_id,
                                                  repoId: repo.short_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }]);


// Local Variables:
// mode: javascript
// End:
