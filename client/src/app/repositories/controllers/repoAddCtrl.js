// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.controllers.add', [
  'kalam.utils',
  'kalam.repositories.resources',
  'kalam.repositories.directives'
]).controller(
  'repoAddCtrl',
  ['$scope', '$stateParams', '$state', 'RepositoryCollection', 'apiErrorDialog',
   function ($scope, $stateParams, $state, RepositoryCollection, errorDlg) {
     $scope.project_id = $stateParams.projectId;

     if (!$scope.projectDash.can_add_repositories) {
       console.log("Current user doesn't have permission to add repository");
       if ($scope.projectDash.can_view_repositories) {
         $state.go("repositories.list", {projectId: $scope.project_id});
       }
       else {
         $state.go("project.overview", {projectId: $scope.project_id});
       }
       return;
     }

     $scope.getClass = function(ngModelCtrl) {
       return {
         'has-error': ngModelCtrl.$invalid && ngModelCtrl.$dirty
       };
     };

     $scope.showError = function(ngModelCtrl, error) {
       return ngModelCtrl.$error[error] && ngModelCtrl.$dirty;
     };

     $scope.canSave = function () {
       return ($scope.repoAddForm.$dirty && $scope.repoAddForm.$valid);
     };

     $scope.save = function () {
       if (!$scope.repoAddForm.$valid) {
         return;
       }
       RepositoryCollection.save(
         { projectId: $scope.project_id },
         $scope.repo,
         function (data) {
           $state.go('repositories.list', {projectId: $scope.project_id});
         },
         function (err_res) {
           errorDlg.show(err_res);
         });
     };
   }]);


// Local Variables:
// mode: javascript
// End:
