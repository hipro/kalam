// Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
// rights reserved.
//
// This program and the accompanying materials are made available
// under the terms described in the LICENSE file which accompanies
// this distribution. If the LICENSE file was not attached to this
// distribution or for further clarifications, please contact
// legal@hipro.co.in.

angular.module('kalam.repositories.directives', [
  'kalam.repositories.resources'
])

  .directive (
    'uniqueRepoId',
    ['Repository', function (Repository) {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          element.on('blur', function (event) {
            scope.$apply(function () {
              var viewValue = element.val();
              if (viewValue) {
                Repository.get(
                  { projectId: scope.project_id,
                    repoId: viewValue },
                  function (data) {
                    ngModelCtrl.$setValidity('uniqueRepoId', false);
                  },
                  function (err_res) {
                    ngModelCtrl.$setValidity('uniqueRepoId', true);
                  });
              }
             });
           });
         }
       };
     }]);


// Local Variables:
// mode: javascript
// End:
