# To run

First build the parts
        $ python bootstrap-buildout.py
        $ ./bin/buildout
        $ cd client/
        $ npm install
        $ grunt development

In one terminal:
        $ pwd
        /path/to/kalam/folder
        $ ./bin/runzeo -C parts/etc/zeo.conf

In another terminal:
        $ pwd
        /path/to/kalam/folder
        $ ./bin/pserve parts/etc/development.ini

In the browser, visit http://localhost:6543 . Tada.. :)

# Coding standards

We use the `pre-commit` tool to lint the code and check for common errors
and [PEP 8](https://www.python.org/dev/peps/pep-0008/) violations.

1. Install the `pre-commit` tool locally under your home directory

        $ pip install --user pre-commit

2. On GNU/Linux, this installs `pre-commit` under `~/.local` and the
`pre-commit` script is available in `~/.local/bin`. Add the directory
to `$PATH`. You can also install the `pre-commit` tool using other
ways as long as the `pre-commit` command is available in one of the
directories in `$PATH`.

3. Navigate to the root of this repo and run `pre-commit install` to
install the configured checks as a pre-commit hook. The checks are
configured in `.pre-commit-config.yaml`.

4. For installing the checks as a pre-push hook, run `pre-commit
install -t pre-push`.

5. To skip the execution of some checks, specify a comma-separated
list of check ids in a `SKIP` environment variable. For instance, the
below command skips the `flake8` check.

        $ SKIP=flake8 git commit

6. To skip all the checks, pass the `--no-verify` flag to `git commit`.