;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((js-mode
  (js-indent-level . 2))
 (python-mode
  (python-indent-offset . 4)
  (tab-width . 4)
  (indent-tabs-mode)))
