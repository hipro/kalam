# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import os

from zope.component import getGlobalSiteManager

from pyramid.config import Configurator
from pyramid.authentication import SessionAuthenticationPolicy

from pyramid_beaker import session_factory_from_settings

from kalam import (
    core,
)


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application. """

    registry = getGlobalSiteManager()
    config = Configurator(registry=registry)
    config.setup_registry(root_factory=core.root_factory, settings=settings)

    authn_policy = SessionAuthenticationPolicy(callback=core.get_principals)
    pset_manager = core.authorization.PermissionSetManager()
    authz_policy = core.authorization.ACLAuthorizationPolicy(pset_manager)
    session_factory = session_factory_from_settings(settings)

    config.set_session_factory(session_factory)
    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.registry.registerUtility(
        pset_manager, core.interfaces.IPermissionSetManager)

    config.include(".core")
    config.include(".repositories")
    config.include(".ui")

    # setup the routes for the client code
    pub_dir = settings['kalam.publish_folder']

    def pub_path(part):
        return os.path.join(pub_dir, part)

    config.add_static_view(name="app", path=pub_path("app"))
    config.add_static_view(name="scripts", path=pub_path("scripts"))
    config.add_static_view(name="styles", path=pub_path("styles"))
    config.add_static_view(name="fonts", path=pub_path("fonts"))
    config.add_static_view(name="vendor", path=pub_path("vendor"))

    config.scan()

    return config.make_wsgi_app()


# local Variables:
# mode: python
# End:
