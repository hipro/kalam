# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

# import logging

# from cornice.resource import resource as cornice_resource
# from pyramid.httpexceptions import HTTPNotFound

# from kalam.core import DBSession
# from kalam.projects.backend import ProjectContainer

# log = logging.getLogger(__name__)


# def resource_root_factory(request):
#     projects = ProjectContainer(None, DBSession, request)
#     project = projects.get_by_id(request.matchdict['project_id'])
#     if project is None:
#         raise HTTPNotFound("Project does not exist")
#     return project


# @cornice_resource(
#     name="ui.project_overview",
#     path="/ui/project_overview/{project_id}",
#     factory=resource_root_factory,
#     permission="project.view",
# )
# class Overview(object):
#     def __init__(self, context, request):
#         self.context = context
#         self.request = request

#     def get(self):
#         return {
#             'type': 'project_overview',
#             'result': {
#                 'id': self.context.id,
#                 'short_id': self.context.short_id,
#                 'name': self.context.name,
#                 'admin': self.context.admin,
#                 'activities': [],
#                 'issues': []
#             }
#         }


# Local Variables:
# mode: python
# End:
