# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from cornice.resource import resource as cornice_resource
from pyramid.httpexceptions import (
    HTTPNotFound,
)


log = logging.getLogger(__name__)


def resource_to_dict(user):
    return {
        'login': user.login,
        'email': user.email,
        'name': user.name,
        'active': user.active,
        'admin': user.admin,
    }


@cornice_resource(
    name="ui.user_info",
    path="/ui/user_info",

)
class CurrentUserInfo(object):
    def __init__(self, request):
        self.request = request

    def get(self):
        user = self.request.root['users'].get(
            self.request.authenticated_userid, None)
        if user is None:
            self.request.errors.add(
                'url', 'login', 'Authenticated user not found')
            self.request.errors.status = HTTPNotFound.code
            return

        return {
            'type': 'user',
            'result': resource_to_dict(user)
        }


# Local Variables:
# mode: python
# End:
