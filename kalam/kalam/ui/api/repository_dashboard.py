# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from cornice.resource import resource as cornice_resource

from kalam.core.utils import (
    login_has_permission,
    get_project_from_request,
)

log = logging.getLogger(__name__)


@cornice_resource(
    name="ui.repository_dashboard",
    path="/ui/repository_dashboard/{project_id}/{short_id}",
    traverse="/projects/{project_id}/repositories/{short_id}",
    permission="kalam.repository.read",
)
class Dashboard(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def get(self):
        data = {
            'short_id': self.context.short_id,
            'name': self.context.name,
        }

        has_permission = self.request.has_permission
        if has_permission('ps:repository.manage'):
            data['can_edit_settings'] = True
            data['can_manage_permissions'] = True
        else:
            data['can_edit_settings'] = False
            data['can_manage_permissions'] = False

        return {
            'type': 'repository_dashboard',
            'result': data
        }


@cornice_resource(
    name="ui.repository_acl_users",
    path="/ui/repository_acl_users/{project_id}/{short_id}",
    traverse="/projects/{project_id}/repositories/{short_id}",
    permission="kalam.repository.manage",
)
class UsersForACL(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def get(self):
        project_id = self.request.matchdict["project_id"]
        project = get_project_from_request(self.request, project_id)
        users = []
        for user in project.member_users:
            login = user.login
            if login_has_permission(self.request, login,
                                    'ps:repositories.view-meta'):
                d = {
                    'login': login,
                    'name': user.name,
                }
                users.append(d)

        return {
            'type': 'repository_acl_users',
            'result': users,
        }


# Local Variables:
# mode: python
# End:
