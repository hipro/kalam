# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from cornice.resource import resource as cornice_resource

from kalam.core.utils import get_projects_associated_with_user

log = logging.getLogger(__name__)


def to_dict(project_list):
    dlist = []
    for project in project_list:
        dlist.append({
            "short_id": project.short_id,
            "name": project.name
        })
    return dlist


@cornice_resource(
    name="ui.user_dashboard",
    path="/ui/user_dashboard",
    permission="kalam.view",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def get(self):
        associated_projects = get_projects_associated_with_user(
            self.context, self.request.authenticated_userid)
        return {
            'type': 'user_dashboard',
            'result': {
                'projects': to_dict(associated_projects),
            }
        }

# Local Variables:
# mode: python
# End:
