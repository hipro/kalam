# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from cornice.resource import resource as cornice_resource

log = logging.getLogger(__name__)


@cornice_resource(
    name="ui.project_dashboard",
    path="/ui/project_dashboard/{project_id}",
    traverse="/projects/{project_id}",
    permission="kalam.projects.view",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def get(self):
        data = {
            'short_id': self.context.short_id,
            'name': self.context.name,
        }
        has_permission = self.request.has_permission
        perm_map = (
            ('ps:projects.manage', 'can_edit_project_settings'),
            ('ps:project_members.manage', 'can_manage_members'),
            ('ps:project_roles.manage', 'can_manage_roles'),
            ('ps:repositories.view-meta', 'can_view_repositories'),
            ('ps:repositories.create-new', 'can_add_repositories'),
            ('ps:repositories.manage-all', 'can_manage_repositories'))

        for rperm, operm in perm_map:
            if has_permission(rperm):
                data[operm] = True
            else:
                data[operm] = False

        return {
            'type': 'project_dashboard',
            'result': data
        }


# Local Variables:
# mode: python
# End:
