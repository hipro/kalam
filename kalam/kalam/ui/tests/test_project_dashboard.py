# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import itertools

import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
)

from kalam.core import testing as kt

log = logging.getLogger(__name__)


def all_combinations(data):
    roles = []
    for index in range(1, len(data) + 1):
        c_list = list(itertools.combinations(data, index))
        roles.extend(c_list)
    return roles


ROLE_PERMS = (
    'ps:projects.manage',
    'ps:project_members.manage',
    'ps:project_roles.manage',
    'ps:repositories.view-meta',
    'ps:repositories.create-new',
    'ps:repositories.manage-all',
)


FIXTURE_DATA = all_combinations(ROLE_PERMS)


PERM_KEY_TRANSLATION = {
    'ps:projects.manage': ("can_edit_project_settings", ),
    'ps:project_members.manage': ("can_manage_members", ),
    'ps:project_roles.manage': ("can_manage_roles", ),
    'ps:repositories.view-meta': ("can_view_repositories", ),
    'ps:repositories.create-new': ("can_view_repositories",
                                   "can_add_repositories"),
    'ps:repositories.manage-all': ("can_view_repositories",
                                   "can_add_repositories",
                                   "can_manage_repositories"),
}


PERM_KEYS = (
    "can_edit_project_settings",
    "can_manage_members",
    "can_manage_roles",
    "can_view_repositories",
    "can_add_repositories",
    "can_manage_repositories",
)


class Fixture(object):
    def __init__(self, fixture_value):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config, ['kalam.ui', ])
        self.auth = self.app.k_authn
        kt.create_user(self.app, "john", "john@john.com")
        kt.create_project(self.app, "p1", "Project one")

        self.role_perms = fixture_value
        kt.create_role(self.app, "p1", "role1", "Role1", fixture_value)
        kt.create_membership(self.app, "p1", "john", "role1")

        result_keys = []
        for perm in self.role_perms:
            result_keys.extend(PERM_KEY_TRANSLATION[perm])
        self.result_keys = set(result_keys)

        self.url = "/ui/project_dashboard/p1"
        self.auth.set_current_user('john')


def generate_id(fixture_value):
    return ','.join(fixture_value)


@pytest.fixture(params=FIXTURE_DATA, ids=generate_id)
def fixture(request):
    fixture = Fixture(request.param)

    def fin():
        pyramid.testing.tearDown()
    request.addfinalizer(fin)
    return fixture


def test_dashboard_perm_keys(fixture):
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    data = resp.json["result"]
    for key in PERM_KEYS:
        if key in fixture.result_keys:
            assert data[key] is True
        else:
            assert data[key] is False


# Local Variables:
# mode: python
# End:
