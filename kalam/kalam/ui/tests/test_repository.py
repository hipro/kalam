# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPForbidden,
)

from kalam.core import testing as kt
from kalam.repositories import testing as krt

log = logging.getLogger(__name__)


class Fixture(object):
    def __init__(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config, ['kalam.repositories', 'kalam.ui', ])
        self.auth = self.app.k_authn
        kt.create_user(self.app, "u1", "u1@test.com", name="U1")
        kt.create_user(self.app, "u2", "u2@test.com", name="U2")
        kt.create_user(self.app, "u3", "u3@test.com", name="U3")
        kt.create_user(self.app, "u4", "u4@test.com", name="U4")

        kt.create_project(self.app, "p1", "Project one")
        kt.create_role(self.app, "p1", "role1", "Role1",
                       ["ps:repositories.create-new", ])
        kt.create_role(self.app, "p1", "role2", "Role2",
                       ["ps:repositories.view-meta", ])

        kt.create_membership(self.app, "p1", "u1", "role1")
        kt.create_membership(self.app, "p1", "u2", "role2")
        kt.create_membership(self.app, "p1", "u3")

        self.auth.set_current_user('u1')
        krt.create_repo(self.app, "p1", "repo1")
        self.url = "/ui/repository_dashboard/p1/repo1"


def fixture_finalizer():
    pyramid.testing.tearDown()


@pytest.fixture
def fixture(request):
    fixture = Fixture()
    request.addfinalizer(fixture_finalizer)
    return fixture


def test_dashboard_for_owner(fixture):
    fixture.auth.set_current_user('u1')
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    data = resp.json["result"]
    assert data['can_edit_settings'] is True
    assert data['can_manage_permissions'] is True


def test_dashboard_for_non_managing_member_without_read_rights(fixture):
    fixture.auth.set_current_user('u2')
    fixture.app.get(fixture.url, status=HTTPForbidden.code)


def test_dashboard_for_non_managing_member_with_read_rights(fixture):
    fixture.auth.set_current_user('u1')
    krt.set_acl(fixture.app, 'p1', 'repo1', 'u2', 'ps:repository.read')

    fixture.auth.set_current_user('u2')
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    data = resp.json["result"]
    assert data['can_edit_settings'] is False
    assert data['can_manage_permissions'] is False


def test_dashboard_for_non_managing_member_with_manage_rights(fixture):
    fixture.auth.set_current_user('u1')
    krt.set_acl(fixture.app, 'p1', 'repo1', 'u2', 'ps:repository.manage')

    fixture.auth.set_current_user('u2')
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    data = resp.json["result"]
    assert data['can_edit_settings'] is True
    assert data['can_manage_permissions'] is True


def test_dashboard_for_member_with_no_repo_rights(fixture):
    fixture.auth.set_current_user('u3')
    fixture.app.get(fixture.url, status=HTTPForbidden.code)


def test_dashboard_for_non_member(fixture):
    fixture.auth.set_current_user('u4')
    fixture.app.get(fixture.url, status=HTTPForbidden.code)

# Local Variables:
# mode: python
# End:
