# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from zope import interface

from pyramid.interfaces import IAuthorizationPolicy
from pyramid.location import lineage
from pyramid.compat import is_nonstr_iter

from pyramid.security import (
    Allow,
    Deny,
    ALL_PERMISSIONS,
    ACLAllowed,
    ACLDenied,
)

from .interfaces import IPermissionSetManager

log = logging.getLogger(__name__)


class PermissionSetManager(object):
    interface.implements(IPermissionSetManager)

    def __init__(self):
        self._psets = {}

    def add(self, name, permissions):
        if name in self._psets:
            raise RuntimeError("name=%s already exists" % (name, ))
        has_recur, stack = self._has_recursive_nesting(name, permissions)
        if has_recur:
            raise RuntimeError("recursive nesting detected: %s" % (stack, ))

        self._psets[name] = permissions

    def has(self, name):
        return name in self._psets

    def get_flat_permissions(self, name):
        perms = self._psets.get(name, ())
        flat_perms = []
        for p in perms:
            if p in self._psets:
                fperms = self.get_flat_permissions(p)
                flat_perms.extend(fperms)
            else:
                flat_perms.append(p)
        return flat_perms

    def _has_recursive_nesting(self, name, permissions, stack=None):
        # http://docs.python-guide.org/en/latest/writing/gotchas/
        if stack is None:
            stack = []
        stack.append(name)
        for p in permissions:
            if p in stack:
                stack.append(p)
                return True, stack
            if p in self._psets:
                perms = self._psets[p]
                dup_stack = [x for x in stack]
                has, rstack = self._has_recursive_nesting(p, perms, dup_stack)
                if has is True:
                    return has, rstack
        return False, None


class ACLAuthorizationPolicy(object):
    interface.implements(IAuthorizationPolicy)

    def __init__(self, pset_manager):
        self._pset_manager = pset_manager

    def permits(self, context, principals, permission):
        """ Return an instance of
        :class:`pyramid.security.ACLAllowed` instance if the policy
        permits access, return an instance of
        :class:`pyramid.security.ACLDenied` if not."""

        acl = '<No ACL found on any object in resource lineage>'
        req_perms = self._flatten_perms(permission)
        for location in lineage(context):
            try:
                acl = location.__acl__
            except AttributeError:
                continue

            if acl and callable(acl):
                acl = acl()

            for ace in acl:
                ace_action, ace_principal, ace_permissions = ace
                if ace_principal not in principals:
                    continue

                if ace_permissions == ALL_PERMISSIONS:
                    if ace_action == Allow:
                        return ACLAllowed(ace, acl, permission,
                                          principals, location)
                    else:
                        return ACLDenied(ace, acl, permission,
                                         principals, location)

                if not is_nonstr_iter(ace_permissions):
                    ace_permissions = [ace_permissions]

                flat_perms = self._flatten_perms(ace_permissions)
                common_perms = set.intersection(req_perms, flat_perms)

                if ace_action == Deny and (len(common_perms) != 0):
                    return ACLDenied(
                        ace, acl, permission, principals, location)

                req_perms = req_perms - common_perms
                if len(req_perms) == 0:
                    return ACLAllowed(
                        ace, acl, permission, principals, location)

        # default deny (if no ACL in lineage at all, or if none of the
        # principals were mentioned in any ACE we found)
        return ACLDenied(
            '<default deny>', acl, permission, principals, context)

    def _flatten_perms(self, perms):
        if not is_nonstr_iter(perms):
            perms = [perms, ]
        final = []
        pm = self._pset_manager
        for p in perms:
            if pm.has(p):
                final.extend(pm.get_flat_permissions(p))
            else:
                final.append(p)
        return set(final)

    def principals_allowed_by_permission(self, context, permission):
        raise NotImplementedError("Nobody uses this.")


# Local Variables:
# mode: python
# End:
