# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

from zope.interface import Interface


class IDataProvider(Interface):

    def data_for_ui(limit, offset):
        """Returns UI data for the given limit and offset value"""

    def size():
        """Return the total number of records"""


class IAuth(Interface):
    def check_password(request, login, password):
        pass


class IAppMaker(Interface):
    def __call__(request):
        pass


class IGetPrincipals(Interface):
    def __call__(login, request):
        pass


class IPermissionSetManager(Interface):
    def has(name):
        pass

    def add(name, permissions):
        pass

    def get_flat_permissions(name):
        pass


class IUser(Interface):
    pass


class IRole(Interface):
    pass


class IProject(Interface):
    pass


class IMember(Interface):
    pass


# Local Variables:
# mode: python
# End:
