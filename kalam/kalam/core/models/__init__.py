# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from .v1 import (  # noqa
    Root,
    User,
    UserContainer,
    SSHKey,
    Project,
    ProjectContainer,
    Role,
    Member,
    UI_PERMISSIONS,
    cleanup_removed_role,
    cleanup_removed_user,
)

log = logging.getLogger(__name__)


def init_db(root):
    _init_users(root)
    _init_projects(root)


def _init_users(root):
    container = root.get("users", None)
    if container is None:
        log.debug("Creating UserContainer at /users")
        container = UserContainer()
        root["users"] = container
    else:
        log.debug(
            "Skipping creation of UserContainer. Already exists at /users")

    admin_user = container.get("admin", None)
    if admin_user is None:
        log.debug("Creating admin-user at /users/admin")
        admin_user = User("admin", "admin@nowhere.com")
        admin_user.admin = True
        admin_user.set_password("admin")
        container.add(admin_user.login, admin_user)
    else:
        log.debug(
            "Skipping creation of admin-user. Already exists at /users/admin")


def _init_projects(root):
    container = root.get("projects", None)
    if container is None:
        log.debug("Creating ProjectContainer at /projects")
        container = ProjectContainer()
        root["projects"] = container
    else:
        log.debug("Skipping creation of ProjectContainer."
                  " Already exists at /projects")


# Local Variables:
# mode: python
# End:
