# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import random
import hashlib
import six

from zope.interface import implements
from zope.component import adapter

from repoze import folder

from persistent import Persistent
from persistent.mapping import PersistentMapping
from persistent.list import PersistentList

from pyramid.security import (
    Allow,
    Authenticated,
    ALL_PERMISSIONS,
)
from pyramid.traversal import (
    find_interface,
)

from ..interfaces import (
    IUser,
    IRole,
    IProject,
    IMember,
)
from ..common import (
    CaseInsensitiveFolder,
    Resource,
)

log = logging.getLogger(__name__)


DEFAULT_PERMISSIONS = (
    'kalam.projects.view',
)


UI_PERMISSIONS = {
    'ps:projects.manage': ('kalam.projects.manage', ),

    'ps:project_members.manage': ('kalam.project_members.manage', ),

    'ps:project_roles.manage': ('kalam.project_roles.manage', ),

    'ps:repositories.view-meta': ('kalam.repository.view-meta', ),

    'ps:repositories.create-new': ('kalam.repository.view-meta',
                                   'kalam.repository.add', ),

    'ps:repositories.read-all': ('kalam.repository.view-meta',
                                 'kalam.repository.read', ),

    'ps:repositories.write-all': ('kalam.repository.view-meta',
                                  'kalam.repository.read',
                                  'kalam.repository.write', ),

    'ps:repositories.manage-all': ('kalam.repository.view-meta',
                                   'kalam.repository.add',
                                   'kalam.repository.read',
                                   'kalam.repository.write',
                                   'kalam.repository.manage', ),
}


class Root(folder.Folder):
    __parent__ = None
    __name__ = ''
    __acl__ = (
        (Allow, Authenticated, ("kalam.view", "kalam.users.view")),
        (Allow, "g:admins", ALL_PERMISSIONS),
    )


class User(Persistent):
    implements(IUser)

    def __init__(self, login, email):
        super(User, self).__init__(self)
        self.login = login.strip().lower()
        self.email = email
        password = _generate_temp_password(8)
        self.password = _sha512(password)
        self.name = "Unknown user"
        self.admin = False
        self.active = True
        self._ssh_keys = PersistentMapping()
        self.__acl__ = (
            (Allow, self.login, 'kalam.modify-self'),
        )

    def set_password(self, password):
        self.password = _sha512(password)

    def check_password(self, password):
        return self.active and (self.password == _sha512(password))

    def parent(self):
        return self.__parent__

    def has_ssh_keyid(self, keyid):
        return keyid in self._ssh_keys

    def has_ssh_key(self, key):
        for keyid in self._ssh_keys:
            existing_key = self._ssh_keys[keyid]
            if (key.fingerprint == existing_key.fingerprint) and \
               (key.key_type == existing_key.key_type):
                return True
        return False

    def add_ssh_key(self, key):
        self._ssh_keys[key.short_id] = key

    def remove_ssh_key(self, keyid):
        del self._ssh_keys[keyid]

    def get_ssh_key(self, keyid):
        return self._ssh_keys.get(keyid, None)

    @property
    def ssh_keys(self):
        return self._ssh_keys.values()


class UserContainer(CaseInsensitiveFolder):
    def find_with_email(self, email):
        for user in self.values():
            if user.email == email:
                return user
        return None

    def is_email_unique(self, email):
        user = self.find_with_email(email)
        return user is None


class SSHKey(Persistent):
    def __init__(self, short_id, keystring):
        self._short_id = short_id.strip().lower()
        self.keystring = keystring
        self.key_type = None
        self.fingerprint = None
        self.bits = None

    @property
    def short_id(self):
        return self._short_id


class Role(Resource):
    implements(IRole)

    def __init__(self, short_id):
        super(Role, self).__init__(short_id)
        self.name = ''
        self.ui_permissions = PersistentList()


class RoleContainer(CaseInsensitiveFolder):
    pass


class Member(Resource):
    implements(IMember)

    def __init__(self, user):
        super(Member, self).__init__(user.login)
        self.user = user
        self.role = None


class MemberContainer(CaseInsensitiveFolder):
    pass


class Project(folder.Folder):
    implements(IProject)

    def __init__(self, short_id):
        super(Project, self).__init__()
        self._short_id = short_id.strip().lower()
        self.name = 'New project'
        self.add("roles", RoleContainer())
        self.add("members", MemberContainer())

    def __acl__(self):
        acl = []
        members = self["members"].values()
        for member in members:
            login = member.user.login
            acl.append((Allow, login, DEFAULT_PERMISSIONS))
            if member.role is not None:
                permissions = tuple(member.role.ui_permissions)
                acl.append((Allow, login, permissions))
        return acl

    @property
    def short_id(self):
        return self._short_id

    @short_id.setter
    def short_id(self, value):
        self._short_id = value.strip().lower()

    @property
    def member_users(self):
        users = (member.user for member in self["members"].values()
                 if member.user.active)
        return users

    def parent(self):
        return self.__parent__

    def has_member(self, login):
        return login in self["members"]

    def get_ui_permissions(self, login):
        """Get the permissions associated with the given login"""
        member = self["members"].get(login, None)
        if (member is None) or (member.role is None):
            return ()
        return tuple(member.role.ui_permissions)


class ProjectContainer(CaseInsensitiveFolder):
    pass


@adapter(IRole, folder.interfaces.IObjectWillBeRemovedEvent)
def cleanup_removed_role(removed_role, event):
    project = find_interface(removed_role, Project)
    if project is None:
        raise RuntimeError(
            "Given removed_role(%s) not contained in a Project. "
            "Cannot locate MemberContainer." % (removed_role.short_id, ))
    members = project["members"]
    for login in members:
        role = members[login].role
        if role and role.short_id == removed_role.short_id:
            members[login].role = None


@adapter(IUser, folder.interfaces.IObjectWillBeRemovedEvent)
def cleanup_removed_user(removed_user, event):
    root = find_interface(removed_user, Root)
    for project_id, project in root["projects"].items():
        members = project["members"]
        for login in members:
            if login == removed_user.login:
                members.remove(login)


def _generate_temp_password(length):
    chars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"
    r = random.SystemRandom()
    return ''.join(r.choice(chars) for _ in range(length))


def _sha512(text):
    sha = hashlib.sha512()
    sha.update(six.b(text))
    return sha.hexdigest()


# Local Variables:
# mode: python
# End:
