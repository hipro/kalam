# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from pyramid.interfaces import IAuthorizationPolicy
from pyramid.traversal import find_interface

from .interfaces import (
    IAppMaker,
    IGetPrincipals,
)
from . import models

log = logging.getLogger(__name__)


def root_factory(request):
    appmaker = request.registry.getUtility(IAppMaker)
    return appmaker(request)


def login_has_permission(request, login, permissions, context=None):
    if context is None:
        context = request.context
    registry = request.registry

    user = get_user(context, login)
    if user is None or user.active is False:
        return False

    get_principals = registry.getUtility(IGetPrincipals)
    principals = get_principals(login, request)
    principals.append(login)

    authz = registry.getUtility(IAuthorizationPolicy)
    action = authz.permits(context, principals, permissions)
    if action:
        return True
    else:
        return False


def find_app_root(context):
    return find_interface(context, models.Root)


def get_user(context, login):
    root = find_app_root(context)
    users = root["users"]
    return users.get(login, None)


def get_role(context, role_id):
    project = find_interface(context, models.Project)
    if project is None:
        raise RuntimeError("Given context not contained in a Project. "
                           "Cannot locate role.")
    return project["roles"].get(role_id, None)


def get_projects_associated_with_user(context, login):
    associated_projects = []
    root = find_app_root(context)
    projects = root["projects"]
    for project_id in projects:
        project = projects[project_id]
        if login in project["members"]:
            associated_projects.append(project)
    return associated_projects


def get_project(context, project_id):
    container = find_interface(context, models.ProjectContainer)
    return container.get(project_id, None)


def get_current_project(context):
    return find_interface(context, models.Project)


def get_project_from_request(request, project_id):
    return request.root["projects"].get(project_id, None)


# Local Variables:
# mode: python
# End:
