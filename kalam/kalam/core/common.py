# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from repoze import folder
from persistent import Persistent

log = logging.getLogger(__name__)


class PaginatedContainer(object):
    """Paginates a given container based on the given page_size. The
    page-numbering is UI centric i.e. the page-numbering always starts
    from 1. When the container is empty, the page-count is 1.

    :param container: A dict like container. repoze.folder.Folder
    works well.

    :param page_size: The number of data elements in each-page.

    """

    def __init__(self, container, page_size):
        self._container = container
        self._container_size = len(container)
        self._page_size = page_size
        self._page_count = self._container_size / page_size
        if self._page_count == 0 or (
                (page_size * self._page_count) < self._container_size):
            self._page_count += 1

    @property
    def page_count(self):
        return self._page_count

    @property
    def size(self):
        return self._container_size

    def is_valid(self, page_number):
        return page_number and page_number <= self._page_count

    def get(self, page_number):
        assert self.is_valid(page_number), "Invalid page-number"
        start_index = (page_number - 1) * self._page_size
        end_index = page_number * self._page_size
        index = 0
        key_set = self._container.keys()
        entries = []
        for key in key_set:
            if index < start_index:
                index += 1
                continue
            if index >= end_index:
                break
            entries.append(self._container.get(key))
            index += 1
        return entries


class CaseInsensitiveFolder(folder.Folder):
    def __contains__(self, key):
        key = key.strip().lower()
        return super(CaseInsensitiveFolder, self).__contains__(key)

    def __delitem__(self, key):
        key = key.strip().lower()
        return super(CaseInsensitiveFolder, self).__delitem__(key)

    def __getitem__(self, key):
        key = key.strip().lower()
        return super(CaseInsensitiveFolder, self).__getitem__(key)

    def __setitem__(self, key, value):
        key = key.strip().lower()
        return super(CaseInsensitiveFolder, self).__setitem__(key, value)

    def get(self, key, default=None):
        key = key.strip().lower()
        return super(CaseInsensitiveFolder, self).get(key, default)

    def add(self, key, value, send_events=True):
        key = key.strip().lower()
        super(CaseInsensitiveFolder, self).add(key, value, send_events)

    def remove(self, key):
        key = key.strip().lower()
        super(CaseInsensitiveFolder, self).remove(key)


class Resource(Persistent):
    def __init__(self, short_id):
        super(Resource, self).__init__()
        self.__parent__ = None
        self._short_id = short_id.strip().lower()

    @property
    def short_id(self):
        return self._short_id

    @short_id.setter
    def short_id(self, value):
        self._short_id = value.strip().lower()

    def parent(self):
        return self.__parent__


class ResourceFolder(CaseInsensitiveFolder):
    def __init__(self, short_id):
        super(Resource, self).__init__()
        self.__parent__ = None
        self._short_id = short_id.strip().lower()

    @property
    def short_id(self):
        return self._short_id

    @short_id.setter
    def short_id(self, value):
        self._short_id = value.strip().lower()

    def parent(self):
        return self.__parent__


# Local Variables:
# mode: python
# End:
