# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from zope import interface

from pyramid.httpexceptions import (
    HTTPOk,
)
from pyramid.security import (
    Authenticated,
    Everyone,
)

from webtest import TestApp

from cornice.tests.support import CatchErrors

from . import (
    interfaces,
    models,
    authorization,
    utils,
    get_principals,
)

log = logging.getLogger(__name__)


class TestAppMaker(object):
    interface.implements(interfaces.IAppMaker)

    def __init__(self):
        self.root = models.Root()
        models.init_db(self.root)

    def __call__(self, request):
        return self.root


class AuthenticationPolicy(object):
    def __init__(self, callback, login="admin"):
        self._callback = callback
        self.login = login

    def authenticated_userid(self, request):
        return self.login

    def unauthenticated_userid(self, request):
        return self.login

    def effective_principals(self, request):
        principals = [self.login, Authenticated, Everyone]
        groups = self._callback(self.login, request)
        principals.extend(groups)
        return principals

    def set_current_user(self, login):
        self.login = login


def create_app(config, includes=None):
    if includes is None:
        includes = []

    authn_policy = AuthenticationPolicy(get_principals)
    pset_manager = authorization.PermissionSetManager()
    authz_policy = authorization.ACLAuthorizationPolicy(pset_manager)

    registry = config.registry
    registry.registerUtility(
        pset_manager, interfaces.IPermissionSetManager)

    config.include("pyramid_tm")
    config.include("cornice")

    config.include("kalam.core")
    for x in includes:
        config.include(x)

    config.set_authorization_policy(authz_policy)
    config.set_authentication_policy(authn_policy)
    config.set_root_factory(utils.root_factory)

    config.scan("kalam.core")
    for x in includes:
        config.scan(x)

    registry.registerUtility(TestAppMaker(), interfaces.IAppMaker)
    app = TestApp(CatchErrors(config.make_wsgi_app()))
    app.k_registry = registry
    app.k_authn = authn_policy
    return app


def create_user(app, login, email, name=None, admin=False):
    d = {
        'login': login,
        'email': email,
        'admin': admin,
    }
    if name is not None:
        d["name"] = name
    app.post_json("/api/v1/users", d, status=HTTPOk.code)


def delete_user(app, login):
    url = "/api/v1/users/%s.json" % (login, )
    app.delete(url, status=HTTPOk.code)


def mark_user_as_inactive(app, login):
    url = "/api/v1/users/%s.json" % (login, )
    d = {
        'active': False,
    }
    app.put(url, d, status=HTTPOk.code)


def set_user_password(app, login, password):
    url = "/api/v1/users/%s.json" % (login, )
    d = {
        'password': password,
    }
    app.put(url, d, status=HTTPOk.code)


def create_project(app, project_id, name):
    d = {
        'short_id': project_id,
        'name': name,
    }
    app.post_json("/api/v1/projects", d, status=HTTPOk.code)


def create_role(app, project_id, role_id, name, permissions):
    d = {
        'short_id': role_id,
        'name': name,
        'permissions': permissions
    }
    url = "/api/v1/projects/%s/roles" % (project_id, )
    app.post_json(url, d, status=HTTPOk.code)


def create_membership(app, project_id, login, role_id=''):
    d = {
        "login": login,
        "role_id": role_id,
    }
    url = "/api/v1/projects/%s/members" % (project_id, )
    app.post_json(url, d, status=HTTPOk.code)


def delete_membership(app, project_id, login):
    url = "/api/v1/projects/%s/members/%s.json" % (project_id, login)
    app.delete(url, status=HTTPOk.code)


def add_sshkey(app, login, keyid, keystring):
    url = "/api/v1/users/%s/sshkeys" % (login, )
    d = {
        'short_id': keyid,
        'keystring': keystring,
    }
    app.post_json(url, d, status=HTTPOk.code)


def has_error(resp, location, attrib):
    errors = resp.json.get('errors', None)
    if errors is not None:
        for error in errors:
            if error['location'] == location and error['name'] == attrib:
                return True
    return False


# Local Variables:
# mode: python
# End:
