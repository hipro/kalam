# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.


from .common import (  # noqa
    PaginatedContainer,
    CaseInsensitiveFolder,
    Resource,
    ResourceFolder,
)

from . import interfaces  # noqa
from . import models  # noqa
from . import authorization  # noqa
from .utils import root_factory  # noqa


def get_principals(login, request):
    component = request.registry.getUtility(interfaces.IGetPrincipals)
    return component(login, request)


def includeme(config):
    from .interfaces import (
        IAppMaker, IAuth, IGetPrincipals,
        IPermissionSetManager)
    from .components import AppMaker, Auth, GetPrincipals

    config.set_default_permission("kalam.view")

    config.add_route("home", "/")
    config.add_route("login", "/login")
    config.add_route("logout", "/logout")

    config.registry.registerUtility(AppMaker(), IAppMaker)
    config.registry.registerUtility(Auth(), IAuth)
    config.registry.registerUtility(GetPrincipals(), IGetPrincipals)

    pset_manager = config.registry.getUtility(IPermissionSetManager)
    for name, pset in models.UI_PERMISSIONS.iteritems():
        pset_manager.add(name, pset)

    config.registry.registerHandler(models.cleanup_removed_role)
    config.registry.registerHandler(models.cleanup_removed_user)


# Local Variables:
# mode: python
# End:
