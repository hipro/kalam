# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import unittest

import pytest

from pyramid.security import (
    Allow,
    Deny,
    ALL_PERMISSIONS,
)

from ..authorization import (
    PermissionSetManager,
    ACLAuthorizationPolicy,
)

logging.basicConfig(level=logging.ERROR)


def test_pset_manager_throws_on_duplicate_pset():
    sut = PermissionSetManager()
    sut.add("r1", ("p1", "p2"))
    with pytest.raises(RuntimeError):
        sut.add("r1", ())


def test_pset_manager_flattens_nested_psets():
    sut = PermissionSetManager()
    sut.add("r1", ("p1", "p2"))
    sut.add("r2", ("r1", "p3"))
    flat_perms = sut.get_flat_permissions("r2")
    assert set(["p1", "p2", "p3"]) == set(flat_perms)


def test_pset_manager_does_not_allow_recursive_nested_psets_1():
    sut = PermissionSetManager()
    sut.add("r1", ("r2", "p2"))
    with pytest.raises(RuntimeError) as err:
        sut.add("r2", ("r1", "p3"))
    val = str(err.value)
    assert val.startswith("recursive nesting detected")


def test_pset_manager_does_not_allow_recursive_nested_psets_2():
    sut = PermissionSetManager()
    sut.add("r3", ("p1", "p2"))
    sut.add("r2", ("r3", "p3"))
    sut.add("r1", ("r2", "r3"))
    flat_perms = sut.get_flat_permissions("r1")
    assert set(["p1", "p2", "p3"]) == set(flat_perms)


class Resource(object):
    def __init__(self, parent=None):
        self.__parent__ = parent
        self.__acl__ = []

    def add_ace(self, action, principal, permissions):
        self.__acl__.append((action, principal, permissions))


class TestAuthz(unittest.TestCase):
    def setUp(self):
        self.pset_manager = PermissionSetManager()
        self.authz = ACLAuthorizationPolicy(self.pset_manager)
        self.pset_manager.add("r1", ("r1_p1", "r1_p2"))
        self.pset_manager.add("r2", ("r2_p1", "r2_p2"))
        self.pset_manager.add("r3", ("r1", "r2"))

    def tearDown(self):
        pass

    def test_scene1(self):
        r = Resource()
        r.add_ace(Allow, 'u1', 'p1')
        r.add_ace(Deny, 'u2', 'p1')

        sut = self.authz
        assert sut.permits(r, ('u1', ), 'p1')
        assert not sut.permits(r, ('u2', ), 'p1')
        assert not sut.permits(r, ('u1', ), 'p2')
        assert not sut.permits(r, ('u2', ), 'p2')

    def test_scene2(self):
        r = Resource()
        r.add_ace(Allow, 'u1', ('p1', 'p2'))
        r.add_ace(Deny, 'u2', 'p1')

        sut = self.authz
        assert sut.permits(r, ('u1', ), 'p1')
        assert not sut.permits(r, ('u2', ), 'p1')
        assert sut.permits(r, ('u1', ), 'p2')
        assert not sut.permits(r, ('u2', ), 'p2')

    def test_scene3(self):
        r1 = Resource()
        r1.add_ace(Allow, 'u1', ('p1', 'p2'))
        r1.add_ace(Deny, 'u2', 'p1')
        r2 = Resource(r1)
        r2.add_ace(Deny, 'u1', 'p2')

        sut = self.authz
        assert sut.permits(r2, ('u1', ), 'p1')
        assert not sut.permits(r2, ('u2', ), 'p1')
        assert not sut.permits(r2, ('u1', ), 'p2')
        assert not sut.permits(r2, ('u2', ), 'p2')

    def test_scene4(self):
        r1 = Resource()
        r1.add_ace(Deny, 'u1', ('p1', 'p2'))
        r1.add_ace(Deny, 'u2', 'p1')
        r2 = Resource(r1)
        r2.add_ace(Allow, 'u1', 'p2')

        sut = self.authz
        assert not sut.permits(r2, ('u1', ), 'p1')
        assert not sut.permits(r2, ('u2', ), 'p1')
        assert sut.permits(r2, ('u1', ), 'p2')
        assert not sut.permits(r2, ('u2', ), 'p2')

    def test_scene5(self):
        r1 = Resource()
        r1.add_ace(Allow, 'u1', 'r3')
        r2 = Resource(r1)
        r2.add_ace(Deny, 'u1', 'r1')

        sut = self.authz
        # checks on r2
        assert not sut.permits(r2, ('u1', ), 'r3')
        assert not sut.permits(r2, ('u1', ), 'r1')
        assert not sut.permits(r2, ('u1', ), 'r1_p1')
        assert not sut.permits(r2, ('u1', ), 'r1_p2')
        assert sut.permits(r2, ('u1', ), 'r2')
        assert sut.permits(r2, ('u1', ), 'r2_p1')
        assert sut.permits(r2, ('u1', ), 'r2_p2')
        assert not sut.permits(r2, ('u2', ), 'r1')
        assert not sut.permits(r2, ('u2', ), 'r2')
        assert not sut.permits(r2, ('u2', ), 'r3')

        # checks on r1
        assert sut.permits(r1, ('u1', ), 'r3')
        assert sut.permits(r1, ('u1', ), 'r2')
        assert sut.permits(r1, ('u1', ), 'r1')
        assert sut.permits(r1, ('u1', ), 'r1_p1')
        assert sut.permits(r1, ('u1', ), 'r1_p2')
        assert sut.permits(r1, ('u1', ), 'r2_p1')
        assert sut.permits(r1, ('u1', ), 'r2_p2')
        assert sut.permits(r1, ('u1', ), ('r1_p2', 'r2_p2'))
        assert not sut.permits(r1, ('u2', ), 'r1')
        assert not sut.permits(r1, ('u2', ), 'r2')
        assert not sut.permits(r1, ('u2', ), 'r3')

    def test_scene6(self):
        r1 = Resource()
        r1.add_ace(Allow, 'u1', ALL_PERMISSIONS)
        r2 = Resource(r1)
        r2.add_ace(Deny, 'u1', 'r1')

        sut = self.authz
        # checks on r2
        assert not sut.permits(r2, ('u1', ), 'r3')
        assert not sut.permits(r2, ('u1', ), 'r1')
        assert sut.permits(r2, ('u1', ), 'r2')
        assert not sut.permits(r2, ('u2', ), 'r1')
        assert not sut.permits(r2, ('u2', ), 'r2')
        assert not sut.permits(r2, ('u2', ), 'r3')

        # checks on r1
        assert sut.permits(r1, ('u1', ), 'r3')
        assert sut.permits(r1, ('u1', ), 'r2')
        assert sut.permits(r1, ('u1', ), 'r1')
        assert not sut.permits(r1, ('u2', ), 'r1')
        assert not sut.permits(r1, ('u2', ), 'r2')
        assert not sut.permits(r1, ('u2', ), 'r3')

    def test_scene7(self):
        r1 = Resource()
        r1.add_ace(Deny, 'u1', ALL_PERMISSIONS)
        r2 = Resource(r1)
        r2.add_ace(Allow, 'u1', 'r1')

        sut = self.authz
        # checks on r2
        assert sut.permits(r2, ('u1', ), 'r1')
        assert not sut.permits(r2, ('u1', ), 'r2')
        assert not sut.permits(r2, ('u1', ), 'r3')
        assert not sut.permits(r2, ('u2', ), 'r1')
        assert not sut.permits(r2, ('u2', ), 'r2')
        assert not sut.permits(r2, ('u2', ), 'r3')

        # checks on r1
        assert not sut.permits(r1, ('u1', ), 'r3')
        assert not sut.permits(r1, ('u1', ), 'r2')
        assert not sut.permits(r1, ('u1', ), 'r1')
        assert not sut.permits(r1, ('u2', ), 'r1')
        assert not sut.permits(r1, ('u2', ), 'r2')
        assert not sut.permits(r1, ('u2', ), 'r3')


# Local Variables:
# mode: python
# End:
