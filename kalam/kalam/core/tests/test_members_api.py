# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
)
import pyramid.testing

from .. import testing as kt

log = logging.getLogger(__name__)


class APITests(unittest.TestCase):
    def setUp(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config)
        self.auth = self.app.k_authn
        kt.create_user(self.app, 'john', 'j1@test.com')
        kt.create_user(self.app, 'jane', 'j2@test.com')
        kt.create_project(self.app, "p1", "Project one")
        kt.create_project(self.app, "p2", "Project two")
        kt.create_role(self.app, 'p1', 'role1', 'Role1',
                       ['ps:project_members.manage'])
        kt.create_role(self.app, 'p1', 'role2', 'Role2',
                       ['ps:project_members.manage'])
        kt.create_role(self.app, 'p2', 'role1', 'Role1',
                       ['ps:project_members.manage'])
        kt.create_role(self.app, 'p2', 'role2', 'Role2',
                       ['ps:project_members.manage'])
        self.p1_url = "/api/v1/projects/p1/members"
        self.p2_url = "/api/v1/projects/p2/members"

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_can_get_empty_list(self):
        resp = self.app.get(self.p1_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 0)

    def test_can_get_member_list(self):
        d = {
            'login': 'john',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        d = {
            'login': 'jane',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        resp = self.app.get(self.p1_url, status=HTTPOk.code)
        self.assertEqual(len(resp.json['result']), 2)

    def test_can_create_member_with_role(self):
        d = {
            'login': 'john',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        self.app.get(self.p1_url + '/john.json', status=HTTPOk.code)

    def test_can_create_member_without_role(self):
        d = {
            'login': 'john',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        self.app.get(self.p1_url + '/john.json', status=HTTPOk.code)

    def test_cannot_create_member_with_invalid_login(self):
        d = {
            'login': 'john1',
        }
        resp = self.app.post_json(self.p1_url, d, status=HTTPBadRequest.code)
        self.assertTrue(kt.has_error(resp, "body", "login"))

    def test_cannot_create_member_with_invalid_role(self):
        d = {
            'login': 'john',
            'role_id': 'no-such-role'
        }
        resp = self.app.post_json(self.p1_url, d, status=HTTPBadRequest.code)
        self.assertTrue(kt.has_error(resp, "body", "role_id"))

    def test_can_delete_member(self):
        d = {
            'login': 'john',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        john_url = self.p1_url + '/john.json'
        self.app.get(john_url, status=HTTPOk.code)
        self.app.delete(john_url, status=HTTPOk.code)
        self.app.get(john_url, status=HTTPNotFound.code)

    def test_cannot_delete_non_existing_member(self):
        self.app.delete(self.p1_url + '/john.json', status=HTTPNotFound.code)

    def test_can_update_previously_empty_role(self):
        d = {
            'login': 'john',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        john_url = self.p1_url + '/john.json'
        self.app.get(john_url, status=HTTPOk.code)
        d = {
            'role_id': 'role1',
        }
        self.app.put_json(john_url, d, status=HTTPOk.code)
        resp = self.app.get(john_url, status=HTTPOk.code)
        member = resp.json["result"]
        self.assertEqual(member["role_id"], "role1")

    def test_can_update_role(self):
        d = {
            'login': 'john',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        john_url = self.p1_url + '/john.json'

        resp = self.app.get(john_url, status=HTTPOk.code)
        member = resp.json["result"]
        self.assertEqual(member["role_id"], "role1")
        d = {
            'role_id': 'role2',
        }
        self.app.put_json(john_url, d, status=HTTPOk.code)
        resp = self.app.get(john_url, status=HTTPOk.code)
        member = resp.json["result"]
        self.assertEqual(member["role_id"], "role2")

    def test_cannot_update_with_invalid_role(self):
        d = {
            'login': 'john',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        d = {
            'role_id': 'no-such-role',
        }
        url = self.p1_url + '/john.json'
        resp = self.app.put_json(url, d, status=HTTPBadRequest.code)
        self.assertTrue(kt.has_error(resp, "body", "role_id"))

    def test_delete_role_updates_member(self):
        d = {
            'login': 'john',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        self.app.delete(
            '/api/v1/projects/p1/roles/role1.json', status=HTTPOk.code)
        resp = self.app.get(self.p1_url + '/john.json', status=HTTPOk.code)
        member = resp.json['result']
        self.assertEqual(member['role_id'], None)

    def test_delete_user_cleanups_members(self):
        # create memberships
        d = {
            'login': 'john',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        self.app.post_json(self.p2_url, d, status=HTTPOk.code)
        d = {
            'login': 'jane',
            'role_id': 'role1',
        }
        self.app.post_json(self.p1_url, d, status=HTTPOk.code)
        self.app.post_json(self.p2_url, d, status=HTTPOk.code)
        # initiate a delete-user
        self.app.delete("/api/v1/users/john.json", status=HTTPOk.code)
        # check cleanup status
        self.app.get(self.p1_url + '/john.json', status=HTTPNotFound.code)
        self.app.get(self.p2_url + '/john.json', status=HTTPNotFound.code)
        self.app.get(self.p1_url + '/jane.json', status=HTTPOk.code)
        self.app.get(self.p2_url + '/jane.json', status=HTTPOk.code)

# Local Variables:
# mode: python
# End:
