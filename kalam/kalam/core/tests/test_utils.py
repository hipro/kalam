# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import unittest

from ..common import (
    PaginatedContainer,
    CaseInsensitiveFolder,
)

log = logging.getLogger(__name__)


class PaginatedContainerTests(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_empty_container(self):
        sut = PaginatedContainer({}, 10)
        self.assertEqual(sut.page_count, 1)
        self.assertEqual(sut.size, 0)
        self.assertFalse(sut.is_valid(0))
        self.assertTrue(sut.is_valid(1))
        self.assertFalse(sut.is_valid(2))
        self.assertEqual(sut.get(1), [])

    def test_container_size_less_than_page_size(self):
        data = {"a": 1, "b": 2}
        sut = PaginatedContainer(data, 10)
        self.assertEqual(sut.page_count, 1)
        self.assertEqual(sut.size, 2)
        self.assertTrue(sut.is_valid(1))
        self.assertFalse(sut.is_valid(2))
        self.assertEqual(sut.get(1), [1, 2])

    def test_container_size_equal_to_page_size(self):
        data = {"a": 1, "b": 2}
        sut = PaginatedContainer(data, 2)
        self.assertEqual(sut.page_count, 1)
        self.assertEqual(sut.size, 2)
        self.assertTrue(sut.is_valid(1))
        self.assertFalse(sut.is_valid(2))
        self.assertEqual(sut.get(1), [1, 2])

    def test_container_size_greater_than_page_size(self):
        data = {"a": 1, "b": 2}
        sut = PaginatedContainer(data, 1)
        self.assertEqual(sut.page_count, 2)
        self.assertEqual(sut.size, 2)
        self.assertTrue(sut.is_valid(1))
        self.assertTrue(sut.is_valid(2))
        self.assertFalse(sut.is_valid(3))
        self.assertEqual(sut.get(1), [1, ])
        self.assertEqual(sut.get(2), [2, ])


class CaseInsensitiveFolderTests(unittest.TestCase):
    def test_ignores_case_of_key(self):
        class A(object):
            pass
        a = A()
        sut = CaseInsensitiveFolder()
        sut["aA"] = a
        self.assertEqual(sut["Aa"], a)
        self.assertEqual(sut["aa"], a)


# Local Variables:
# mode: python
# End:
