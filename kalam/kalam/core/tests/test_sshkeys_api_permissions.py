# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPForbidden,
    HTTPMethodNotAllowed
)

from .. import testing as kt

log = logging.getLogger(__name__)

FO = HTTPForbidden.code
OK = HTTPOk.code
NA = HTTPMethodNotAllowed.code

PARAMS = (
    # user-type, COLL_GET, COLL_POST, GET, PUT, DELETE
    (1, OK, OK, OK, NA, OK),  # admin
    (2, OK, OK, OK, NA, OK),  # self
    (3, FO, FO, FO, NA, FO),  # other non-admin
)

USER_TYPES = ('admin', 'self', 'other non-admin')


class Fixture(object):
    def __init__(self, fixture_value):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config)
        self.auth = self.app.k_authn
        kt.create_user(self.app, "john", "john@john.com")
        kt.create_user(self.app, "doe", "doe@doe.com")
        kt.add_sshkey(self.app, 'john', 'k1', KEY1)
        user_type, self.res_coll_get, self.res_coll_post, \
            self.res_get, self.res_put, self.res_delete = fixture_value
        if user_type == 1:
            # current user is already admin
            pass
        elif user_type == 2:
            self.auth.set_current_user('john')
        else:
            self.auth.set_current_user('doe')
        self.url = "/api/v1/users/john/sshkeys"


def generate_id(fixture_value):
    return USER_TYPES[fixture_value[0] - 1]


@pytest.fixture(params=PARAMS, ids=generate_id)
def fixture(request):
    fixture = Fixture(request.param)

    def fin():
        pyramid.testing.tearDown()
    request.addfinalizer(fin)
    return fixture


def test_collection_get(fixture):
    fixture.app.get(fixture.url, status=fixture.res_coll_get)


def test_collection_post(fixture):
    d = {
        'short_id': 'k2',
        'keystring': KEY2,
    }
    fixture.app.post_json(fixture.url, d, status=fixture.res_coll_post)


def test_get(fixture):
    url = fixture.url + '/k1'
    fixture.app.get(url, status=fixture.res_get)


def test_put(fixture):
    data = {
        'keystring': KEY3
    }
    url = fixture.url + '/k1'
    fixture.app.put_json(url, data, status=fixture.res_put)


def test_delete(fixture):
    url = fixture.url + '/k1'
    fixture.app.delete(url, status=fixture.res_delete)


KEY1 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYWAaU5LQJffGA/KxX6GvSlxTieXxXbBMnhdVICClbjKW9WriHfYkT0+3iGTqxPpLu4EKvxzJUSDnPY1FAmLWexlpSCmRWQz3jgyUhEdvj/lGCdkv7ZvScRtncCzUgxSQNshljS4NmOTkF1x+sReVgshigqCn713Pmvbkrr9xkXkKMY0OfvrWvSuxrNLuv5W6xQcROU+ghKzoLbx3Jp6OrU/ocRTrgJVlpANSDYObyfNspHx+Ud6r0FVtr3c+Eh5yndH8C3n0uFQX57XXweFQcxKSVfgWJ2m24o1oa7dech7P8ArANHHcE6I/kAbMSHBlntDf+FpeF+NY3cu5vCggZ joe@lama"  # noqa
KEY2 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDX1ccMhyIJe1KxPZtI7epCLSerjSR+/zrzUZ64mPywUYZjEsJReweJsKyQEbr4slTG7rk6zN97k2OiNHxfqUILPemEl53mznjCzNkzTfEp87RMW8oKOjaxgb2Z6L4pJEdIJJ0D6wfOD+2/waT6snuJTJbSz24QK2pTLrMDXac4QYKs0xjr1xeTJo/ceABzLBLuGNQFfVRJGDjtmXXWXXzvuijl4ne15bmXb4uthLhA92IMtjstZ/+MjO2EtP5KLxoz3eWcDXG98GF41yPHiHL0nHaHnVecDc6mk+ivffzFf74uQKNqXKUP5BGkibYAuZF32YV6v7ize+64CJv5RWTF joe@lama"  # noqa
KEY3 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4yPlv8ZDGPtI4Jmwxzf7ql58x/xoVqzMtF5V3dJr6Eu+SbwyrpMHlUyHLCdRWNroyI9V/KmthHusL3YkUHbeoFw7P6wSb6vPaB3xbFs+fp8pZ/XY2EUsKHKVEpG1+Kd2my7JYLzQsB2iN6cbTTwtrlsR5Zf9PDsO6+cpWytfUmcl1bpSuyl9f1uJZ7fAVrpE3u3BcFO92LmOvjB4OHKcUM8I/mayFT5VHUHjH45TpP5kekeMSODq/WdnHZwy4nqrAOvs8fpt9z2APfOdCGnzifiP5jNWDT0hEMK5w5V2UPw0e9v84nOZAM8pL5qUrqpOyBfXjyUEby+wIn0Q0/GvZ joe@lama"  # noqa

# Local Variables:
# mode: python
# End:
