# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPForbidden,
)

from .. import testing as kt

log = logging.getLogger(__name__)

FO = HTTPForbidden.code
OK = HTTPOk.code

PARAMS = (
    # Admin, COLL_GET, COLL_POST, GET, PUT, DELETE
    (False, OK, FO, OK, FO, FO),
    (True,  OK, OK, OK, OK, OK),
)


class Fixture(object):
    def __init__(self, fixture_value):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config)
        self.auth = self.app.k_authn
        kt.create_user(self.app, "john", "john@john.com")

        self.admin, self.res_coll_get, self.res_coll_post, \
            self.res_get, self.res_put, self.res_delete = fixture_value

        if self.admin is False:
            self.auth.set_current_user('john')
        self.url = "/api/v1/users"


def generate_id(fixture_value):
    if fixture_value[0] is True:
        return "Admin"
    else:
        return "Regular user"


@pytest.fixture(params=PARAMS, ids=generate_id)
def fixture(request):
    fixture = Fixture(request.param)

    def fin():
        pyramid.testing.tearDown()
    request.addfinalizer(fin)
    return fixture


def test_collection_get(fixture):
    fixture.app.get(fixture.url, status=fixture.res_coll_get)


def test_collection_post(fixture):
    user = {
        'login': 'mary',
        'email': 'mary@maryjane.com',
    }
    fixture.app.post_json(fixture.url, user, status=fixture.res_coll_post)


def test_get(fixture):
    url = fixture.url + '/john.json'
    fixture.app.get(url, status=fixture.res_get)


def test_put(fixture):
    data = {
        'name': 'New smith'
    }
    url = fixture.url + '/john.json'
    fixture.app.put_json(url, data, status=fixture.res_put)


def test_deny_delete(fixture):
    url = fixture.url + '/john.json'
    fixture.app.delete(url, status=fixture.res_delete)


# Local Variables:
# mode: python
# End:
