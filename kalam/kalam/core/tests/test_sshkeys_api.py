# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
)

from .. import testing as kt

log = logging.getLogger(__name__)


class APIFixture(object):
    def __init__(self):
        self.config = pyramid.testing.setUp()
        self.app = kt.create_app(self.config)
        self.auth = self.app.k_authn
        kt.create_user(self.app, 'u1', 'u1@test.com')
        self.url = "/api/v1/users/u1/sshkeys"


def fixture_finalizer():
    pyramid.testing.tearDown()


@pytest.fixture
def fixture(request):
    fixture = APIFixture()
    request.addfinalizer(fixture_finalizer)
    return fixture


def test_can_get_empty_list(fixture):
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    obj_list = resp.json['result']
    assert len(obj_list) == 0


def test_can_get_key_list(fixture):
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    obj_list = resp.json['result']
    assert len(obj_list) == 0

    d1 = {
        'short_id': 'k1',
        'keystring': GOOD_KEY1["keystring"]
    }
    d2 = {
        'short_id': 'k2',
        'keystring': GOOD_KEY2["keystring"]
    }
    fixture.app.post_json(fixture.url, d1, status=HTTPOk.code)
    fixture.app.post_json(fixture.url, d2, status=HTTPOk.code)

    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    obj_list = resp.json['result']
    assert len(obj_list) == 2


def test_can_add_key(fixture):
    d = {
        'short_id': 'k1',
        'keystring': GOOD_KEY1["keystring"]
    }
    resp = fixture.app.post_json(fixture.url, d, status=HTTPOk.code)
    obj = resp.json['result']
    assert obj['short_id'] == d['short_id']

    url = fixture.url + '/k1'
    resp = fixture.app.get(url, status=HTTPOk.code)
    obj = resp.json['result']
    assert obj['short_id'] == d['short_id']
    assert obj['fingerprint'] == GOOD_KEY1['fingerprint']
    assert obj['bits'] == GOOD_KEY1['bits']


def test_can_get_key(fixture):
    d = {
        'short_id': 'k1',
        'keystring': GOOD_KEY1["keystring"]
    }
    fixture.app.post_json(fixture.url, d, status=HTTPOk.code)

    url = fixture.url + '/k1'
    resp = fixture.app.get(url, status=HTTPOk.code)
    obj = resp.json['result']
    assert obj['short_id'] == d['short_id']


def test_can_delete_key(fixture):
    d = {
        'short_id': 'k1',
        'keystring': GOOD_KEY1["keystring"]
    }
    fixture.app.post_json(fixture.url, d, status=HTTPOk.code)

    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    obj_list = resp.json['result']
    assert len(obj_list) == 1

    url = fixture.url + '/k1'
    fixture.app.delete(url, status=HTTPOk.code)

    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    obj_list = resp.json['result']
    assert len(obj_list) == 0


def test_fails_on_deleting_non_existant_key(fixture):
    url = fixture.url + '/unknownkey'
    fixture.app.delete(url, status=HTTPNotFound.code)


def test_rejects_bad_key(fixture):
    d = {
        'short_id': 'k1',
        'keystring': BAD_KEY1,
    }
    resp = fixture.app.post_json(fixture.url, d, status=HTTPBadRequest.code)
    assert kt.has_error(resp, 'body', 'keystring')


GOOD_KEY1 = {
    "keystring": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYWAaU5LQJffGA/KxX6GvSlxTieXxXbBMnhdVICClbjKW9WriHfYkT0+3iGTqxPpLu4EKvxzJUSDnPY1FAmLWexlpSCmRWQz3jgyUhEdvj/lGCdkv7ZvScRtncCzUgxSQNshljS4NmOTkF1x+sReVgshigqCn713Pmvbkrr9xkXkKMY0OfvrWvSuxrNLuv5W6xQcROU+ghKzoLbx3Jp6OrU/ocRTrgJVlpANSDYObyfNspHx+Ud6r0FVtr3c+Eh5yndH8C3n0uFQX57XXweFQcxKSVfgWJ2m24o1oa7dech7P8ArANHHcE6I/kAbMSHBlntDf+FpeF+NY3cu5vCggZ joe@lama",  # noqa
    "bits": 2048,
    "fingerprint": "a5:e2:60:11:4c:69:29:a5:be:b3:a2:02:5f:64:ef:70",
}

GOOD_KEY2 = {
    "keystring": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDX1ccMhyIJe1KxPZtI7epCLSerjSR+/zrzUZ64mPywUYZjEsJReweJsKyQEbr4slTG7rk6zN97k2OiNHxfqUILPemEl53mznjCzNkzTfEp87RMW8oKOjaxgb2Z6L4pJEdIJJ0D6wfOD+2/waT6snuJTJbSz24QK2pTLrMDXac4QYKs0xjr1xeTJo/ceABzLBLuGNQFfVRJGDjtmXXWXXzvuijl4ne15bmXb4uthLhA92IMtjstZ/+MjO2EtP5KLxoz3eWcDXG98GF41yPHiHL0nHaHnVecDc6mk+ivffzFf74uQKNqXKUP5BGkibYAuZF32YV6v7ize+64CJv5RWTF joe@lama",  # noqa
    "bits": 2048,
    "fingerprint": "06:37:64:1c:54:82:98:3f:f2:8a:cb:6b:91:cd:70:f7",
}

BAD_KEY1 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYWAaU5LQJffGA/KxX6GvSlxTieXxXbBMnhdVICClbjKW9WriHfYkT0+3iGTqxPpLu4EKvxzJUSDnPY1FAmLWexlpSCmRWQz3jgyUhEdvj/lGCdkv7ZvScRtncCzUgxSQNshljS4NmOTkF1x+sReVgshigqCn713Pmvbkrr9xkXkKMY0OfvrWvSuxrNLuv5W6xQcROU+ghKzoLbx3Jp6OrU/ocRTrgJVlpANSDYObyfNspHx+Ud6r0FVtr3c+Eh5yndH8C3n0uFQX57XXweFQcxKSVfgWJ2m24o1oa7dech7P8ArANHHcE6I/kAbMSHBlntDf+FpeF+NY3cu5vCg joe@lama",  # noqa


# Local Variables:
# mode: python
# End:
