# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
)

from .. import testing as kt

log = logging.getLogger(__name__)


class Fixture(object):
    def __init__(self):
        self.config = pyramid.testing.setUp()
        self.app = kt.create_app(self.config)
        self.auth = self.app.k_authn
        self.url = "/api/v1/users"


def fixture_finalizer():
    pyramid.testing.tearDown()


@pytest.fixture
def fixture(request):
    fixture = Fixture()
    request.addfinalizer(fixture_finalizer)
    return fixture


def test_can_get_an_empty_list(fixture):
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    users = resp.json['result']
    assert len(users) == 1


def test_can_add(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'pass',
        }
    resp = fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    data = resp.json['result']
    assert data['login'] == 'mary'

    resp = fixture.app.get(fixture.url + '/mary.json', status=HTTPOk.code)
    data = resp.json['result']
    assert data['login'] == 'mary'


def test_add_fails_with_non_unique_login(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'pass',
        }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    resp = fixture.app.post_json(fixture.url, user, status=HTTPBadRequest.code)
    assert kt.has_error(resp, 'body', 'login')


def test_add_fails_with_non_unique_email(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'pass',
        }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    user['login'] = 'mary1'
    resp = fixture.app.post_json(fixture.url, user, status=HTTPBadRequest.code)
    assert kt.has_error(resp, 'body', 'email')


def test_can_query_by_login(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'pass',
        }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    resp = fixture.app.get(fixture.url + '?login=mary', status=HTTPOk.code)
    user = resp.json['result']
    assert user['login'] == 'mary'


def test_can_query_by_email(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'pass',
        }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    resp = fixture.app.get(fixture.url + '?email=mary@maryjane.com',
                           status=HTTPOk.code)
    user = resp.json['result']
    assert user['login'] == 'mary'


def test_querying_non_existant_email_returns_notfound(fixture):
    resp = fixture.app.get(fixture.url + '?email=bad@email.org',
                           status=HTTPNotFound.code)
    assert kt.has_error(resp, 'querystring', 'email')


def test_querying_non_existant_login_returns_notfound(fixture):
    resp = fixture.app.get(fixture.url + '?login=baduser',
                           status=HTTPNotFound.code)
    assert kt.has_error(resp, 'querystring', 'login')


def test_can_query_by_login_case_insensitively(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'pass',
        }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)

    resp = fixture.app.get(fixture.url + '?login=Mary', status=HTTPOk.code)
    user = resp.json['result']
    assert user['login'] == 'mary'


def test_cannot_add_login_unique_only_with_case_sensitivity(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'pass',
        }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    user['login'] = 'Mary'
    resp = fixture.app.post_json(fixture.url, user, status=HTTPBadRequest.code)
    assert kt.has_error(resp, 'body', 'login')


def test_cannot_add_when_missing_email_field(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'admin': True,
        'password': 'lala',
    }
    resp = fixture.app.post_json(fixture.url, user, status=HTTPBadRequest.code)
    assert kt.has_error(resp, "body", "email")


def test_can_add_with_only_login_and_email(fixture):
    user = {
        'login': 'mary',
        'email': 'm@m.com',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)


def test_by_default_new_user_is_non_admin(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'password': 'marypass',
    }
    resp = fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    result = resp.json['result']
    assert result['admin'] is False


def test_can_update_name(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'name': 'New mary'
    }
    resp = fixture.app.put_json(url, data, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['name'] == upd_user['name']

    resp = fixture.app.get(url, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['name'] == upd_user['name']


def test_can_update_admin_flag(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'admin': True
    }
    resp = fixture.app.put_json(url, data, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['admin'] == upd_user['admin']

    resp = fixture.app.get(url, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['admin'] == upd_user['admin']


def test_can_update_active_flag(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'active': False
    }
    resp = fixture.app.put_json(url, data, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['active'] == upd_user['active']

    resp = fixture.app.get(url, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['active'] == upd_user['active']


def test_can_update_email(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'email': 'mary@m.com'
    }
    resp = fixture.app.put_json(url, data, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['email'] == upd_user['email']

    resp = fixture.app.get(url, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['email'] == upd_user['email']


def test_can_update_password(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'password': 'newpass'
    }
    fixture.app.put_json(url, data, status=HTTPOk.code)


def test_update_all_fields(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'name': 'New Mary',
        'email': 'mary@m.com',
        'admin': True,
        'password': 'newpass',
        'active': False,
    }
    resp = fixture.app.put_json(url, data, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['name'] == upd_user['name']
    assert data['email'] == upd_user['email']
    assert data['active'] == upd_user['active']
    assert data['admin'] == upd_user['admin']

    resp = fixture.app.get(url, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['name'] == upd_user['name']
    assert data['email'] == upd_user['email']
    assert data['active'] == upd_user['active']
    assert data['admin'] == upd_user['admin']


def test_update_fail_on_duplicate_email(fixture):
    user = {
        'login': 'john',
        'name': 'John Doe',
        'email': 'john@users.com',
        'admin': False,
        'password': 'mypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'email': 'john@users.com'
    }
    fixture.app.put_json(url, data, status=HTTPBadRequest.code)


def test_update_fail_on_non_existing_login(fixture):
    url = fixture.url + '/mary.json'
    data = {
        'name': 'New Mary',
        'email': 'mary@m.com',
        'admin': True,
        'password': 'newpass',
        'active': False,
    }
    fixture.app.put_json(url, data, status=HTTPNotFound.code)


def test_update_login_field_ignored(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    data = {
        'login': 'newmary',
        'name': 'New Mary'
    }
    resp = fixture.app.put_json(url, data, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['name'] == upd_user['name']
    assert data['login'] != upd_user['login']
    assert user['login'] == upd_user['login']

    resp = fixture.app.get(url, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert data['name'] == upd_user['name']
    assert data['login'] != upd_user['login']
    assert user['login'] == upd_user['login']


def test_update_no_fields(fixture):
    user = {
        'login': 'mary',
        'name': 'Mary Jane',
        'email': 'mary@maryjane.com',
        'admin': False,
        'password': 'marypass',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    resp = fixture.app.put_json(url, {}, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert user['login'] == upd_user['login']
    assert user['name'] == upd_user['name']
    assert user['email'] == upd_user['email']
    assert user['admin'] == upd_user['admin']
    assert upd_user['active'] is True


def test_delete(fixture):
    user = {
        'login': 'mary',
        'email': 'm@m.com',
    }
    fixture.app.post_json(fixture.url, user, status=HTTPOk.code)
    url = fixture.url + '/mary.json'
    fixture.app.get(url, status=HTTPOk.code)
    fixture.app.delete(url, status=HTTPOk.code)
    fixture.app.get(url, status=HTTPNotFound.code)


def test_deny_delete_admin(fixture):
    fixture.app.delete(fixture.url + '/admin.json', status=HTTPBadRequest.code)


# Local Variables:
# mode: python
# End:
