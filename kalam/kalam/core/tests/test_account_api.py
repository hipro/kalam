# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPBadRequest,
    HTTPForbidden,
    HTTPNotFound,
)

from .. import testing as kt

log = logging.getLogger(__name__)


class Fixture(object):
    def __init__(self):
        self.config = pyramid.testing.setUp()
        self.app = kt.create_app(self.config)
        self.auth = self.app.k_authn
        kt.create_user(self.app, "john", "john@john.com")
        kt.set_user_password(self.app, "john", "johnpass")
        kt.create_user(self.app, "doe", "doe@doe.com")
        kt.set_user_password(self.app, "doe", "doepass")
        self.auth.set_current_user('john')
        self.url = "/api/v1/users/john/account"


def fixture_finalizer():
    pyramid.testing.tearDown()


@pytest.fixture
def fixture(request):
    fixture = Fixture()
    request.addfinalizer(fixture_finalizer)
    return fixture


def test_get_current_user_account(fixture):
    fixture.app.get(fixture.url, status=HTTPOk.code)


def test_cannot_get_other_user_account(fixture):
    fixture.auth.set_current_user("doe")
    fixture.app.get(fixture.url, status=HTTPForbidden.code)


def test_get_invalid_user_account(fixture):
    url = "/api/v1/users/invalid/account"
    fixture.app.get(url, status=HTTPNotFound.code)


def test_update_fail_on_missing_old_password(fixture):
    d = {
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPBadRequest.code)


def test_update_fail_on_missing_new_password(fixture):
    d = {
        'old_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPBadRequest.code)


def test_update_fail_on_invalid_old_password(fixture):
    d = {
        'old_password': 'oldpass',
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPBadRequest.code)


def test_update_password_successfully(fixture):
    d = {
        'old_password': 'johnpass',
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPOk.code)
    d['old_password'] = 'newpass'
    d['new_password'] = 'newnewpass'
    fixture.app.put_json(fixture.url, d, status=HTTPOk.code)


def test_update_password_fail_for_other_user(fixture):
    fixture.auth.set_current_user("doe")
    d = {
        'old_password': 'johnpass',
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPForbidden.code)


def test_update_fail_on_duplicate_email(fixture):
    d = {
        'email': 'doe@doe.com'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPBadRequest.code)


def test_update_email(fixture):
    d = {
        'email': 'new@john.com'
    }
    resp = fixture.app.put_json(fixture.url, d, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert d['email'] == upd_user['email']


def test_update_name(fixture):
    d = {
        'name': 'New John'
    }
    resp = fixture.app.put_json(fixture.url, d, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert d['name'] == upd_user['name']


def test_update_all_permissible_fields(fixture):
    d = {
        'name': 'New John',
        'email': 'new@john.com',
        'old_password': 'johnpass',
        'new_password': 'newpass'
    }
    resp = fixture.app.put_json(fixture.url, d, status=HTTPOk.code)
    upd_user = resp.json['result']
    assert d['name'] == upd_user['name']
    assert d['email'] == upd_user['email']
    d['old_password'] = 'newpass'
    d['new_password'] = 'newnewpass'
    fixture.app.put_json(fixture.url, d, status=HTTPOk.code)


def test_update_fail_for_other_user(fixture):
    fixture.auth.set_current_user("doe")
    d = {
        'name': 'New John',
        'email': 'new@john.com',
        'old_password': 'johnpass',
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPForbidden.code)


def test_admin_can_update_for_other_user(fixture):
    fixture.auth.set_current_user("admin")
    d = {
        'name': 'New John',
        'email': 'new@john.com',
        'old_password': 'johnpass',
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPOk.code)


def test_REGRESSION_23_dont_modify_when_validation_fails(fixture):
    # First PUT an object with invalid 'email'
    d = {
        'name': 'New John',
        'email': 'doe@doe.com',
        'old_password': 'johnpass',
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPBadRequest.code)

    # Since the above call failed, the password should not have
    # changed. So, lets change the password using old credentials to
    # check this.
    d = {
        'old_password': 'johnpass',
        'new_password': 'newpass'
    }
    fixture.app.put_json(fixture.url, d, status=HTTPOk.code)


# Local Variables:
# mode: python
# End:
