# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
)

from .. import testing as kt

log = logging.getLogger(__name__)


class APIFixture(object):
    def __init__(self):
        self.config = pyramid.testing.setUp()
        self.app = kt.create_app(self.config)
        self.auth = self.app.k_authn
        self.url = "/api/v1/projects"


def fixture_finalizer():
    pyramid.testing.tearDown()


@pytest.fixture
def api_fixture(request):
    fixture = APIFixture()
    request.addfinalizer(fixture_finalizer)
    return fixture


def test_can_get_all_projects(api_fixture):
    resp = api_fixture.app.get(api_fixture.url, status=HTTPOk.code)
    projects = resp.json['result']
    assert len(projects) == 0


def test_can_add_new_project(api_fixture):
    project = {
        'short_id': 'p1',
        'name': 'Project One'
    }
    resp = api_fixture.app.post_json(
        api_fixture.url, project, status=HTTPOk.code)
    prj = resp.json['result']
    assert prj['short_id'] == 'p1'

    resp = api_fixture.app.get(api_fixture.url + '/p1.json',
                               status=HTTPOk.code)
    prj = resp.json['result']
    assert prj['short_id'] == 'p1'


def test_cannot_add_project_with_non_unique_short_id(api_fixture):
    project = {
        'short_id': 'p1',
        'name': 'Project One'
    }
    api_fixture.app.post_json(api_fixture.url, project, status=HTTPOk.code)
    api_fixture.app.post_json(api_fixture.url, project,
                              status=HTTPBadRequest.code)


def test_cannot_get_project_with_non_existent_short_id(api_fixture):
    api_fixture.app.get(api_fixture.url + '/bad.json',
                        status=HTTPNotFound.code)


def test_get_project_by_short_id_case_insensitive(api_fixture):
    project = {
        'short_id': 'p1',
        'name': 'Project One'
    }
    api_fixture.app.post_json(api_fixture.url, project, status=HTTPOk.code)
    resp = api_fixture.app.get(api_fixture.url + '/P1.json',
                               status=HTTPOk.code)
    prj = resp.json['result']
    assert prj['short_id'] == 'p1'


def test_add_project_with_non_unique_short_id_case_insensitive(api_fixture):
    prj = {
        'short_id': 'p1',
        'name': 'Project One'
    }
    api_fixture.app.post_json(api_fixture.url, prj, status=HTTPOk.code)
    prj['short_id'] = "P1"
    resp = api_fixture.app.post_json(
        api_fixture.url, prj, status=HTTPBadRequest.code)
    assert resp.json['status']
    assert resp.json['errors'][0]['name'] == 'short_id'


def test_delete_existing_project(api_fixture):
    prj = {
        'short_id': 'p1',
        'name': 'Project One'
    }
    api_fixture.app.post_json(api_fixture.url, prj, status=HTTPOk.code)
    api_fixture.app.delete(api_fixture.url + "/p1.json", status=HTTPOk.code)
    api_fixture.app.get(api_fixture.url + "/p1.json", status=HTTPNotFound.code)


def test_update_name(api_fixture):
    kt.create_project(api_fixture.app, "p1", "Project one")
    update = {
        'short_id': 'p1',
        'name': 'Another'
    }
    url = api_fixture.url + "/p1.json"
    resp = api_fixture.app.put_json(url, update, status=HTTPOk.code)
    prj = resp.json['result']
    assert prj['name'] == update['name']


def test_update_short_id(api_fixture):
    kt.create_project(api_fixture.app, "p1", "Project one")
    update = {
        'short_id': 'p2'
    }
    url = api_fixture.url + "/p1.json"
    api_fixture.app.put_json(url, update, status=HTTPBadRequest.code)


# Local Variables:
# mode: python
# End:
