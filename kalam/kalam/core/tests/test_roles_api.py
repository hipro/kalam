# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import unittest

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPNotFound,
    HTTPBadRequest,
)

import pyramid.testing

from .. import testing as kt

log = logging.getLogger(__name__)


class APITests(unittest.TestCase):
    def setUp(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config)
        self.auth = self.app.k_authn
        kt.create_project(self.app, "p1", "Project one")
        self.url = "/api/v1/projects/p1/roles"

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_can_get_empty_list(self):
        resp = self.app.get(self.url, status=HTTPOk.code)
        result = resp.json['result']
        self.assertEqual(len(result), 0)

    def test_cannot_get_non_existing(self):
        self.app.get(self.url + "/r1.json", status=HTTPNotFound.code)

    def test_can_add(self):
        r = {
            'short_id': 'r1',
            'name': 'Role1',
            'permissions': ['ps:projects.manage', 'ps:project_members.manage'],
        }
        self.app.post_json(self.url, r, status=HTTPOk.code)
        resp = self.app.get(self.url + '/r1.json', status=HTTPOk.code)
        result = resp.json["result"]

        self.assertEqual(result["short_id"], r["short_id"])
        self.assertEqual(result["name"], r["name"])
        self.assertEqual(result["permissions"], r["permissions"])

    def test_fail_on_add_duplicate_role(self):
        r = {
            'short_id': 'r1',
            'name': 'Role1',
            'permissions': ['ps:projects.manage'],
        }
        self.app.post_json(self.url, r, status=HTTPOk.code)
        self.app.post_json(self.url, r, status=HTTPBadRequest.code)

    def test_fail_on_missing_short_id(self):
        r = {
            'name': 'Role1',
            'permissions': ['ps:projects.manage'],
        }
        self.app.post_json(self.url, r, status=HTTPBadRequest.code)

    def test_fail_on_missing_name(self):
        r = {
            'short_id': 'r1',
            'permissions': ['ps:projects.manage'],
        }
        self.app.post_json(self.url, r, status=HTTPBadRequest.code)

    def test_fail_on_missing_permissions(self):
        r = {
            'short_id': 'r1',
            'name': 'Role1',
        }
        self.app.post_json(self.url, r, status=HTTPBadRequest.code)

    def test_fail_to_add_with_invalid_permissions(self):
        r = {
            'short_id': 'r1',
            'name': 'Role1',
            'permissions': ['manage-monkey'],
        }
        self.app.post_json(self.url, r, status=HTTPBadRequest.code)

    def test_fail_to_add_with_duplicate_permissions(self):
        r = {
            'short_id': 'r1',
            'name': 'Role1',
            'permissions': ['ps:projects.manage', 'ps:projects.manage'],
        }
        self.app.post_json(self.url, r, status=HTTPBadRequest.code)


class PUTTests(unittest.TestCase):
    def setUp(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config)
        self.auth = self.app.k_authn
        kt.create_project(self.app, "p1", "Project one")
        kt.create_role(self.app, 'p1', 'r1', 'Role one',
                       ['ps:project_members.manage', ])
        self.url = "/api/v1/projects/p1/roles/r1.json"

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_fail_on_short_id_change(self):
        r1 = {
            'short_id': 'r2',
        }
        resp = self.app.put_json(self.url, r1, status=HTTPBadRequest.code)
        self.assertTrue(kt.has_error(resp, "body", "short_id"))

    def test_fail_on_invalid_permissions(self):
        r1 = {
            'permissions': ['invalid-permission', ],
        }
        resp = self.app.put_json(self.url, r1, status=HTTPBadRequest.code)
        self.assertTrue(kt.has_error(resp, "body", "permissions"))

    def test_changes_name(self):
        r = {
            'name': 'RR',
        }
        self.app.put_json(self.url, r, status=HTTPOk.code)
        resp = self.app.get(self.url)
        result = resp.json["result"]
        self.assertEqual(result["name"], r["name"])

    def test_adds_permission(self):
        r = {
            'permissions': ['ps:project_roles.manage',
                            'ps:project_members.manage'],
        }
        self.app.put_json(self.url, r, status=HTTPOk.code)
        resp = self.app.get(self.url)
        result = resp.json["result"]
        self.assertEqual(set(result["permissions"]), set(r["permissions"]))

    def test_removes_permission(self):
        r = {
            'permissions': [],
        }
        self.app.put_json(self.url, r, status=HTTPOk.code)
        resp = self.app.get(self.url)
        result = resp.json["result"]
        self.assertEqual(set(result["permissions"]), set(r["permissions"]))

    def test_no_change_permission(self):
        r = {
            'permissions': ['ps:project_members.manage'],
        }
        self.app.put_json(self.url, r, status=HTTPOk.code)
        resp = self.app.get(self.url)
        result = resp.json["result"]
        self.assertEqual(set(result["permissions"]), set(r["permissions"]))

    def test_fail_on_duplicate_permissions(self):
        r = {
            'permissions': ['ps:project_members.manage',
                            'ps:project_members.manage'],
        }
        self.app.put_json(self.url, r, status=HTTPBadRequest.code)

# Local Variables:
# mode: python
# End:
