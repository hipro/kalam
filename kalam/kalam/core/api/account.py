# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from pyramid.httpexceptions import (
    HTTPBadRequest,
)

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
)

import colander

log = logging.getLogger(__name__)


class EditProfileSchema(colander.MappingSchema):
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    email = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop,
        validator=colander.Email())
    old_password = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    new_password = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)


def resource_to_dict(user):
    return {
        'login': user.login,
        'email': user.email,
        'name': user.name,
    }


@cornice_resource(
    name="api.user_account",
    path="/api/v1/users/{login}/account",
    traverse="/users/{login}",
    permission="kalam.modify-self",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def get(self):
        return {
            'type': 'account',
            'result': resource_to_dict(self.context)
        }

    @cornice_view(schema=EditProfileSchema)
    def put(self):
        errors = self.request.errors
        updated_user = self.request.validated
        user = self.context
        container = self.context.parent()

        if 'old_password' in updated_user:
            if 'new_password' not in updated_user:
                errors.add('body', 'new_password', "New password not supplied")
                errors.status = HTTPBadRequest.code
                return
        if 'new_password' in updated_user:
            if 'old_password' not in updated_user:
                errors.add('body', 'old_password', "Old password not supplied")
                errors.status = HTTPBadRequest.code
                return
            if user.check_password(updated_user['old_password']) is False:
                errors.add('body', 'old_password', "Old password is incorrect")
                errors.status = HTTPBadRequest.code
                return

        if ('email' in updated_user) \
           and (updated_user['email'] != user.email) \
           and (container.find_with_email(updated_user['email'])):
            errors.add("body", "email", "%s is already in use" %
                       (user.email, ))
            errors.status = HTTPBadRequest.code
            return

        if "email" in updated_user:
            user.email = updated_user['email']
        if "name" in updated_user:
            user.name = updated_user["name"].strip()
        if "new_password" in updated_user:
            user.set_password(updated_user['new_password'])

        return {
            'type': 'account',
            'result': resource_to_dict(user)
        }


# Local Variables:
# mode: python
# End:
