# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import colander

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from pyramid.httpexceptions import (
    HTTPBadRequest,
)

from kalam import core
from .. import models

log = logging.getLogger(__name__)


class Permissions(colander.SequenceSchema):
    permission = colander.SchemaNode(
        colander.String(), location="body", type="str")


class AddSchema(colander.MappingSchema):
    short_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")
    permissions = Permissions(
        validator=colander.Length(max=len(models.UI_PERMISSIONS)))


class PutSchema(colander.MappingSchema):
    short_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    permissions = Permissions(
        validator=colander.Length(max=len(models.UI_PERMISSIONS)),
        missing=colander.drop)


class CollectionGetSchema(colander.MappingSchema):
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=1,
        required=False,
        validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=5,
        required=False,
        validator=colander.Range(min=1, max=100))


def resource_to_dict(obj):
    return {
        'short_id': obj.short_id,
        'name': obj.name,
        'permissions': [p for p in obj.ui_permissions],
    }


def resource_collection_to_dict(obj_list):
    return [resource_to_dict(obj) for obj in obj_list]


@cornice_resource(
    name="api.project_roles",
    collection_path="/api/v1/projects/{project_id}/roles",
    collection_traverse="/projects/{project_id}/roles",
    path="/api/v1/projects/{project_id}/roles/{role_id}.json",
    traverse="/projects/{project_id}/roles/{role_id}",
    permission="kalam.project_roles.manage",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def is_permission_valid(self):
        permissions = self.request.validated['permissions']
        if permissions is None:
            return False
        errors = self.request.errors
        for permission in permissions:
            if permission not in models.UI_PERMISSIONS:
                errors.add('body', 'permissions',
                           "Invalid permission %s" % permission)
                errors.status = HTTPBadRequest.code
                return False
        if len(permissions) != len(set(permissions)):
            errors.add('body', 'permissions', "Duplicate permissions")
            errors.status = HTTPBadRequest.code
            return False
        return True

    # Helper validator methods
    def body_short_id_unique(self, request):
        short_id = request.json.get('short_id', None)
        if short_id and (short_id in self.context):
            self.request.errors.add(
                'body', 'short_id', 'Duplicate role short_id')
            self.request.errors.status = HTTPBadRequest.code

    @cornice_view(schema=CollectionGetSchema)
    def collection_get(self):
        page = self.request.validated['page']
        page_size = self.request.validated['page_size']
        container = core.PaginatedContainer(self.context, page_size)
        if container.is_valid(page) is False:
            self.request.errors.add(
                'querystring', 'page', 'Page number (%d) out of range' % page)
            self.request.errors.status = HTTPBadRequest.code
            return
        entries = container.get(page)
        return {
            'type': 'project_role_list',
            'current_page': page,
            'page_size': page_size,
            'total_pages': container.page_count,
            'total_size': container.size,
            'result': resource_collection_to_dict(entries),
        }

    @cornice_view(
        schema=AddSchema,
        validators=('body_short_id_unique', ),
    )
    def collection_post(self):
        if self.is_permission_valid() is False:
            return
        data = self.request.validated
        role = models.Role(data['short_id'])
        role.name = data['name']
        for p in data['permissions']:
            role.ui_permissions.append(p)

        self.context.add(role.short_id, role)
        return {
            'type': 'project_role',
            'result': resource_to_dict(role),
        }

    def get(self):
        return {
            'type': 'project_role',
            'result': resource_to_dict(self.context)
        }

    @cornice_view(schema=PutSchema)
    def put(self):
        updated = self.request.validated
        if 'short_id' in updated and \
           updated['short_id'] != self.context.short_id:
            self.request.errors.add(
                'body', 'short_id', 'short_id is read-only')
            self.request.errors.status = HTTPBadRequest.code
            return

        if 'permissions' in updated and self.is_permission_valid() is False:
            return

        if 'name' in updated:
            self.context.name = updated['name']

        if 'permissions' in updated:
            new_permissions = updated['permissions']
            for permission in models.UI_PERMISSIONS:
                if (permission in new_permissions) and \
                   (permission not in self.context.ui_permissions):
                    self.context.ui_permissions.append(permission)
                    continue
                if (permission not in new_permissions) and \
                   (permission in self.context.ui_permissions):
                    self.context.ui_permissions.remove(permission)
        return {
            'type': 'project_role',
            'result': resource_to_dict(self.context),
        }

    def delete(self):
        container = self.context.parent()
        container.remove(self.context.short_id)


# Local Variables:
# mode: python
# End:
