# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from sshpubkeys import (
    SSHKey,
    InvalidKeyException,
)

from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPNotFound,
)

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
)

import colander

from .. import models

log = logging.getLogger(__name__)


class GetCollectionSchema(colander.MappingSchema):
    pass


class AddSchema(colander.MappingSchema):
    short_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    keystring = colander.SchemaNode(
        colander.String(), location="body", type="str")


def resource_to_dict(obj):
    return {
        'short_id': obj.short_id,
        'bits': obj.bits,
        'key_type': obj.key_type,
        'fingerprint': obj.fingerprint,
    }


def resource_collection_to_dict(obj_list):
    return [resource_to_dict(obj) for obj in obj_list]


@cornice_resource(
    name="api.user_sshkeys",
    collection_path="/api/v1/users/{login}/sshkeys",
    collection_traverse="/users/{login}",
    path="/api/v1/users/{login}/sshkeys/{short_id}",
    traverse="/users/{login}",
    permission="kalam.modify-self",
    )
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    @cornice_view(schema=GetCollectionSchema)
    def collection_get(self):
        entries = resource_collection_to_dict(self.context.ssh_keys)
        return {
            'type': 'user_ssh_key_list',
            'result': entries
        }

    @cornice_view(schema=AddSchema)
    def collection_post(self):
        data = self.request.validated
        errors = self.request.errors
        context = self.context

        keyid = data['short_id']
        if context.has_ssh_keyid(keyid):
            errors.add('body', 'short_id', "SSH keyid already exists")
            errors.status = HTTPBadRequest.code
            return

        keystring = data['keystring']
        model = self._create_ssh_key_model(keyid, keystring)
        if model is None:
            return

        if context.has_ssh_key(model):
            errors.add('body', 'keystring', "SSH key already exists")
            errors.status = HTTPBadRequest.code
            return

        context.add_ssh_key(model)
        return {
            'type': 'user_ssh_key',
            'result': resource_to_dict(model),
        }

    def get(self):
        errors = self.request.errors
        context = self.context
        request = self.request

        keyid = request.matchdict['short_id']
        key = context.get_ssh_key(keyid)
        if key is None:
            errors.add('body', 'short_id', "SSH keyid does not exist")
            errors.status = HTTPNotFound.code
            return

        return {
            'type': 'user_ssh_key',
            'result': resource_to_dict(key),
        }

    def delete(self):
        errors = self.request.errors
        keyid = self.request.matchdict['short_id']
        context = self.context

        if not context.has_ssh_keyid(keyid):
            errors.add('url', 'short_id', "SSH keyid does not exist")
            errors.status = HTTPNotFound.code
            return
        context.remove_ssh_key(keyid)

    def _create_ssh_key_model(self, short_id, keystring):
        errors = self.request.errors
        try:
            keyobj = SSHKey(keystring)
        except NotImplementedError:
            errors.add("body", "keystring", "Unknown key type")
            errors.status = HTTPBadRequest.code
            return None
        except InvalidKeyException:
            errors.add("body", "keystring", "Invalid key data")
            errors.status = HTTPBadRequest.code
            return None

        model = models.SSHKey(short_id, keystring)
        model.key_type = keyobj.key_type
        model.fingerprint = keyobj.hash_md5()
        model.bits = keyobj.bits
        return model


# Local Variables:
# mode: python
# End:
