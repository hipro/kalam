# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
)

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
)

import colander

from kalam import core
from .. import models

log = logging.getLogger(__name__)


class GetCollectionSchema(colander.MappingSchema):
    email = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    login = colander.SchemaNode(
        colander.String(), location="querystring", type="str",
        missing=colander.drop)
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=1,
        required=False,
        validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=5,
        required=False,
        validator=colander.Range(min=1, max=1000))


class AddSchema(colander.MappingSchema):
    login = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    email = colander.SchemaNode(
        colander.String(), location="body", type="str",
        validator=colander.Email())
    password = colander.SchemaNode(
        colander.String(), location="body", missing=colander.drop)
    admin = colander.SchemaNode(
        colander.Boolean(), location="body", default=False)


class PutSchema(colander.MappingSchema):
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    email = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop,
        validator=colander.Email())
    password = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    admin = colander.SchemaNode(
        colander.Boolean(), location="body", missing=colander.drop)
    active = colander.SchemaNode(
        colander.Boolean(), location="body", missing=colander.drop)


def resource_to_dict(user):
    return {
        'login': user.login,
        'email': user.email,
        'name': user.name,
        'active': user.active,
        'admin': user.admin,
    }


def resource_collection_to_dict(users):
    return [resource_to_dict(user) for user in users]


@cornice_resource(
    name="api.users",
    collection_path="/api/v1/users",
    collection_traverse="/users",
    path="/api/v1/users/{login}.json",
    traverse="/users/{login}",
    permission="kalam.users.manage",
    )
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    @cornice_view(schema=GetCollectionSchema,
                  permission="kalam.users.view")
    def collection_get(self):
        qs_has = self.request.params.has_key
        data = {
            "type": None,
            "result": None,
            }
        if qs_has('email'):
            email = self.request.params.get('email')
            user = self.context.find_with_email(email)
            if user:
                data['type'] = 'user'
                data['result'] = resource_to_dict(user)
                return data
            self.request.errors.add('querystring', 'email', 'No user found')
            self.request.errors.status = HTTPNotFound.code
            return
        elif qs_has('login'):
            login = self.request.params.get('login')
            user = self.context.get(login, None)
            if user:
                data['type'] = 'user'
                data['result'] = resource_to_dict(user)
                return data
            self.request.errors.add('querystring', 'login', 'No user found')
            self.request.errors.status = HTTPNotFound.code
            return

        page = self.request.validated['page']
        page_size = self.request.validated['page_size']
        container = core.PaginatedContainer(self.context, page_size)
        if container.is_valid(page) is False:
            self.request.errors.add(
                'querystring', 'page', 'Invalid page number = %d' % page)
            self.request.errors.status = HTTPBadRequest.code
            return
        entries = resource_collection_to_dict(container.get(page))

        return {
            'type': 'user_list',
            'current_page': page,
            'page_size': page_size,
            'total_pages': container.page_count,
            'total_size': container.size,
            'result': entries
        }

    @cornice_view(schema=AddSchema)
    def collection_post(self):
        data = self.request.validated
        errors = self.request.errors

        if self.context.get(data['login'], None):
            errors.add('body', 'login', "User already exists")
            errors.status = HTTPBadRequest.code
            return

        if self.context.find_with_email(data['email']):
            errors.add('body', 'email', "Email is already in use")
            errors.status = HTTPBadRequest.code
            return

        user = models.User(data['login'], data['email'])
        if "name" in data:
            user.name = data['name']
        if "password" in data:
            user.set_password(data['password'])
        if "admin" in data:
            user.admin = data['admin']
        self.context.add(user.login, user)
        return {
            'type': 'user',
            'result': resource_to_dict(user),
        }

    @cornice_view(schema=PutSchema)
    def put(self):
        errors = self.request.errors
        user = self.context
        updated_user = self.request.validated
        container = self.context.parent()

        if ('email' in updated_user) \
           and (updated_user['email'] != user.email) \
           and (container.find_with_email(updated_user['email'])):
            errors.add("body", "email", "%s is already in use" %
                       (user.email, ))
            errors.status = HTTPBadRequest.code
            return

        if "email" in updated_user:
            user.email = updated_user['email']
        if "name" in updated_user:
            user.name = updated_user["name"].strip()
        if "password" in updated_user:
            user.set_password(updated_user["password"])
        if "admin" in updated_user:
            user.admin = updated_user["admin"]
        if "active" in updated_user:
            user.active = updated_user["active"]

        return {
            'type': 'user',
            'result': resource_to_dict(user),
        }

    @cornice_view(permission="kalam.users.view")
    def get(self):
        return {
            'type': 'user',
            'result': resource_to_dict(self.context)
        }

    def delete(self):
        if self.context.login == "admin":
            self.request.errors.add(
                "url", "login", "'admin' user cannot be deleted")
            self.request.errors.status = HTTPBadRequest.code
            return
        if self.request.authenticated_userid == self.context.login:
            self.request.errors.add(
                "url", "login", "Cannot delete self")
            self.request.errors.status = HTTPBadRequest.code
            return

        container = self.context.parent()
        container.remove(self.context.login)

# Local Variables:
# mode: python
# End:
