# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import colander

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
)

from pyramid.httpexceptions import (
    HTTPBadRequest,
)

from .. import (
    models,
    utils,
    common,
)

log = logging.getLogger(__name__)


class AddSchema(colander.MappingSchema):
    login = colander.SchemaNode(
        colander.String(), location="body", type="str")
    role_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)


class PutSchema(colander.MappingSchema):
    role_id = colander.SchemaNode(
        colander.String(), location="body", type="str")


class CollectionGetSchema(colander.MappingSchema):
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=1,
        required=False,
        validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=5,
        required=False,
        validator=colander.Range(min=1, max=1000))


def resource_to_dict(obj):
    d = {
        'login': obj.user.login,
        'name': obj.user.name,
        'role_id': None,
        'role_name': None,
    }
    if obj.role is not None:
        d['role_id'] = obj.role.short_id
        d['role_name'] = obj.role.name
    return d


def resource_collection_to_dict(obj_list):
    return [resource_to_dict(obj) for obj in obj_list]


@cornice_resource(
    name="api.project.members",
    collection_path="/api/v1/projects/{project_id}/members",
    collection_traverse="/projects/{project_id}/members",
    path="/api/v1/projects/{project_id}/members/{login}.json",
    traverse="/projects/{project_id}/members/{login}",
    permission="kalam.project_members.manage",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    @cornice_view(schema=CollectionGetSchema)
    def collection_get(self):
        data = self.request.validated
        page = data['page']
        page_size = data['page_size']
        container = common.PaginatedContainer(self.context, page_size)
        if container.is_valid(page) is False:
            self.request.errors.add(
                'querystring', 'page', 'Page number (%d) out of range' % page)
            self.request.errors.status = HTTPBadRequest.code
            return
        entries = container.get(page)
        return {
            'type': 'project_member_list',
            'current_page': page,
            'page_size': page_size,
            'total_pages': container.page_count,
            'total_size': container.size,
            'result': resource_collection_to_dict(entries),
        }

    @cornice_view(schema=AddSchema)
    def collection_post(self):
        data = self.request.validated
        errors = self.request.errors

        login = data['login']
        if login in self.context:
            errors.add('body', 'login', "User is already a member")
            errors.status = HTTPBadRequest.code
            return

        user = utils.get_user(self.context, login)
        if user is None:
            errors.add('body', 'login', "User does not exist")
            errors.status = HTTPBadRequest.code
            return

        member = models.Member(user)

        role_id = data.get('role_id', None)
        if role_id is not None:
            role = utils.get_role(self.context, role_id)
            if role is None:
                errors.add('body', 'role_id', "Bad role_id")
                errors.status = HTTPBadRequest.code
                return
            member.role = role

        self.context.add(login, member)
        return {
            'type': 'project_member',
            'result': resource_to_dict(member)
        }

    def get(self):
        return {
            'type': 'project_member',
            'result': resource_to_dict(self.context),
        }

    @cornice_view(schema=PutSchema)
    def put(self):
        data = self.request.validated
        errors = self.request.errors

        role = utils.get_role(self.context, data['role_id'])
        if role is None:
            errors.add('body', 'role_id', "Bad role_id")
            errors.status = HTTPBadRequest.code
            return

        self.context.role = role
        return {
            'type': 'project_member',
            'result': resource_to_dict(self.context),
        }

    def delete(self):
        container = self.context.parent()
        container.remove(self.context.short_id)


# Local Variables:
# mode: python
# End:
