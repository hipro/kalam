# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import colander

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from pyramid.httpexceptions import (
    HTTPBadRequest,
)

from kalam import core
from .. import models

log = logging.getLogger(__name__)


class AddSchema(colander.MappingSchema):
    short_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str")


class PutSchema(colander.MappingSchema):
    short_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)


class CollectionGetSchema(colander.MappingSchema):
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=1,
        required=False,
        validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=5,
        required=False,
        validator=colander.Range(min=1, max=100))


def resource_to_dict(obj):
    return {
        'short_id': obj.short_id,
        'name': obj.name,
    }


def resource_collection_to_dict(obj_list):
    return [resource_to_dict(obj) for obj in obj_list]


@cornice_resource(
    name="api.projects",
    collection_path="/api/v1/projects",
    collection_traverse="/projects",
    path="/api/v1/projects/{short_id}.json",
    traverse="/projects/{short_id}",
    permission="kalam.projects.manage",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    # Helper validator methods
    def body_short_id_unique(self, request):
        short_id = request.json.get('short_id', None)
        if short_id and (short_id in self.context):
            self.request.errors.add(
                'body', 'short_id', 'Duplicate Project short_id')
            self.request.errors.status = HTTPBadRequest.code

    @cornice_view(
        schema=CollectionGetSchema,
        permission="kalam.projects.view",
    )
    def collection_get(self):
        page = self.request.validated['page']
        page_size = self.request.validated['page_size']
        container = core.PaginatedContainer(self.context, page_size)
        if container.is_valid(page) is False:
            self.request.errors.add(
                'querystring', 'page', 'Page number (%d) out of range' % page)
            self.request.errors.status = HTTPBadRequest.code
            return
        entries = container.get(page)
        return {
            'type': 'project_list',
            'current_page': page,
            'page_size': page_size,
            'total_pages': container.page_count,
            'total_size': container.size,
            'result': resource_collection_to_dict(entries),
        }

    @cornice_view(
        schema=AddSchema,
        validators=('body_short_id_unique',),
    )
    def collection_post(self):
        proj = models.Project(self.request.json['short_id'])
        proj.name = self.request.json['name']
        self.context.add(proj.short_id, proj)
        return {
            'type': 'project',
            'result': resource_to_dict(proj),
        }

    @cornice_view(permission="kalam.projects.view")
    def get(self):
        return {
            'type': 'project',
            'result': resource_to_dict(self.context)
        }

    @cornice_view(
        schema=PutSchema,
    )
    def put(self):
        updated_project = self.request.validated
        if 'short_id' in updated_project and \
           updated_project['short_id'] != self.request.matchdict['short_id']:
            self.request.errors.add(
                'body', 'short_id', 'short_id is read-only')
            self.request.errors.status = HTTPBadRequest.code
            return

        if 'name' in updated_project:
            self.context.name = updated_project['name']

        return {
            'type': 'project',
            'result': resource_to_dict(self.context),
        }

    def delete(self):
        container = self.context.parent()
        container.remove(self.context.short_id)


# Local Variables:
# mode: python
# End:
