# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from zope import interface

from pyramid_zodbconn import get_connection

from .interfaces import (
    IAppMaker,
    IAuth,
    IGetPrincipals,
)
from .models import Root
from .utils import find_app_root

log = logging.getLogger(__name__)


class AppMaker(object):
    interface.implements(IAppMaker)

    def __call__(self, request):
        root = get_connection(request).root()
        app = root.get("kalam", None)
        if app is None:
            app = Root()
            root["kalam"] = app
        return app


class Auth(object):
    interface.implements(IAuth)

    def check_password(self, request, login, password):
        user = request.root["users"].get(login)
        if user and user.check_password(password):
            return True
        return False


class GetPrincipals(object):
    interface.implements(IGetPrincipals)

    def __call__(self, login, request):
        root = find_app_root(request.context)
        user = root['users'].get(login, None)
        groups = []
        if user and (user.admin or user.login == "admin"):
            groups.append("g:admins")
        return groups


# Local Variables:
# mode: python
# End:
