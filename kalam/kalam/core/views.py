# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import os

from pyramid.view import (
    view_config,
    forbidden_view_config,
)
from pyramid.security import (
    NO_PERMISSION_REQUIRED,
    remember,
    forget,
)
from pyramid.httpexceptions import (
    HTTPForbidden,
    HTTPFound,
)
from pyramid.renderers import (
    render_to_response,
)

from . import interfaces

log = logging.getLogger(__name__)


@forbidden_view_config()
def forbidden_view(request):
    # do not allow a user to login if they are already logged in
    if request.authenticated_userid:
        log.error("User=%s tried accessing %s",
                  request.authenticated_userid, request.path_qs)
        return HTTPForbidden()
    loc = request.route_url('login', _query={'next': request.path})
    return HTTPFound(location=loc)


@view_config(
    route_name='login',
    permission=NO_PERMISSION_REQUIRED,
)
def login_view(request):
    next = request.params.get('next') or request.route_url('home')
    login = ''
    did_fail = False
    if 'submit' in request.POST:
        login = request.POST.get('login', '')
        password = request.POST.get('password', '')
        auth = request.registry.queryUtility(interfaces.IAuth)
        if auth is None:
            log.error("Failed to get an IAuth utility.")
        elif auth.check_password(request, login, password):
            headers = remember(request, login)
            return HTTPFound(location=next, headers=headers)
        did_fail = True

    data = {
        'login': login,
        'next': next,
        'failed_attempt': did_fail,
    }
    pub_dir = request.registry.settings['kalam.publish_folder']
    tmpl = os.path.join(pub_dir, "login.pt")
    return render_to_response(tmpl, data, request=request)


@view_config(
    route_name='logout',
)
def logout_view(request):
    headers = forget(request)
    loc = request.route_url('home')
    return HTTPFound(location=loc, headers=headers)


@view_config(
    route_name="home",
)
def home_view(request):
    pub_dir = request.registry.settings['kalam.publish_folder']
    tmpl = os.path.join(pub_dir, "index.pt")
    data = {
        'logout_url': request.route_url('logout'),
        'base_url': request.route_url('home'),
    }
    return render_to_response(tmpl, data, request=request)


# Local Variables:
# mode: python
# End:
