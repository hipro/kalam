# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import argparse
import transaction
from os import path

from ZODB.DB import DB

import zodburi

from pyramid.paster import (
    get_appsettings,
    setup_logging,
)

from kalam import core

log = logging.getLogger(__name__)

__all__ = ['main', 'populate_users', 'populate_projects']


def main():
    parser = argparse.ArgumentParser(
        description="Populate projects table")
    parser.add_argument(
        'config_uri', help="Configuration URI filename", type=str)
    args = parser.parse_args()
    fname = args.config_uri
    if path.isfile(fname) is False:
        parser.error("Invalid filename (%s) for configuration URI" % fname)

    setup_logging(fname)
    settings = get_appsettings(fname)
    storage_factory, dbkw = zodburi.resolve_uri(settings['zodbconn.uri'])
    storage = storage_factory()
    db = DB(storage, **dbkw)
    conn = db.open()
    root = conn.root()
    app_root = root.get("kalam", None)
    if app_root is None:
        parser.error("DB not initialised. Run scripts/initialisedb first.")

    with transaction.manager:
        populate_users(app_root)
        populate_projects(app_root)


def populate_users(root):
    user_container = root["users"]
    _populate_normal_users(user_container)
    _populate_admin_users(user_container)


def _populate_normal_users(user_container):
    for i in range(1, 26):
        login = "au%02d" % (i,)
        email = "user_%02d@auth.com" % (i,)
        user = core.models.User(login, email)
        user.name = "Auth User %02d" % (i,)
        user_container.add(login, user)


def _populate_admin_users(user_container):
    for i in range(1, 26):
        login = "adu%02d" % (i,)
        email = "user_%02d@admin.com" % (i,)
        user = core.models.User(login, email)
        user.name = "Admin User %02d" % (i,)
        user.admin = True
        user_container.add(login, user)


def populate_projects(root):
    project_container = root["projects"]
    for i in range(1, 51):
        short_id = "p%02d" % (i,)
        project = core.models.Project(short_id)
        project.name = "Project %02d" % (i,)
        project_container.add(short_id, project)


# Local Variables:
# mode: python
# End:
