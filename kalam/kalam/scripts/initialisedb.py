# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import argparse
import transaction
from os import path

from ZODB.DB import DB

import zodburi

from pyramid.paster import (
    get_appsettings,
    setup_logging,
)

from kalam import (
    core,
)

log = logging.getLogger(__name__)

__all__ = ['main', ]


def main():
    parser = argparse.ArgumentParser(description="Initialize the database")
    parser.add_argument('config_uri',
                        help="Configuration URI filename", type=str)
    parser.add_argument('--force', help="Forcefully overwrite existing data",
                        action="store_true", default=False)
    args = parser.parse_args()
    fname = args.config_uri
    if path.isfile(fname) is False:
        parser.error("Invalid filename (%s) for configuration URI" % fname)

    setup_logging(fname)
    settings = get_appsettings(fname)
    storage_factory, dbkw = zodburi.resolve_uri(settings['zodbconn.uri'])
    storage = storage_factory()
    db = DB(storage, **dbkw)
    conn = db.open()
    with transaction.manager:
        root = conn.root()
        app_root = None
        if args.force is False:
            app_root = root.get("kalam", None)
        if app_root is None:
            app_root = core.models.Root()
        core.models.init_db(app_root)
        root["kalam"] = app_root


# Local Variables:
# mode: python
# End:
