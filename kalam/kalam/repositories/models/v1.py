# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

from persistent.mapping import PersistentMapping

from zope.component import adapter

from repoze import folder

from pyramid.security import (
    Allow,
)

from kalam import (
    core,
)
from kalam.core.utils import find_app_root

log = logging.getLogger(__name__)

UI_PERMISSIONS = {
    'ps:repository.read': ('kalam.repository.read', ),

    'ps:repository.write': ('kalam.repository.read',
                            'kalam.repository.write', ),

    'ps:repository.manage': ('kalam.repository.read',
                             'kalam.repository.write',
                             'kalam.repository.manage', ),
}


class Repository(core.Resource):
    def __init__(self, short_id):
        super(Repository, self).__init__(short_id)
        self.name = ""
        self.description = ""
        self.owner = None
        self.forked_from = None
        self.ui_acl = PersistentMapping()

    def __acl__(self):
        acl = []
        if self.owner is not None:
            acl.append((Allow, self.owner.login, 'ps:repository.manage'))
        for login, ui_permission in self.ui_acl.iteritems():
            acl.append((Allow, login, ui_permission))
        return acl


class RepositoryContainer(core.CaseInsensitiveFolder):
    def __init__(self):
        super(RepositoryContainer, self).__init__()


@adapter(core.interfaces.IProject,
         folder.interfaces.IObjectAddedEvent)
def initialise_on_project_creation(new_project, event):
    new_project["repositories"] = RepositoryContainer()


@adapter(core.interfaces.IProject,
         folder.interfaces.IObjectWillBeRemovedEvent)
def cleanup_on_project_deletion(project, event):
    # todo: For each repo in the container, do cleanup action
    pass


@adapter(core.interfaces.IUser,
         folder.interfaces.IObjectWillBeRemovedEvent)
def cleanup_removed_user(user, event):
    log.debug("Cleaning up repository owner user-removed = %s", user.login)
    root = find_app_root(user)
    for project_id, project in root["projects"].items():
        repositories = project["repositories"]
        for repository_id, repository in repositories.items():
            if repository.owner and (repository.owner.login == user.login):
                repository.owner = None


# Local Variables:
# mode: python
# End:
