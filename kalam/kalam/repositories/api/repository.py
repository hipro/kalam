# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import colander

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPForbidden,
)

from kalam.core.common import PaginatedContainer
from kalam.core.utils import get_user
from .. import models

log = logging.getLogger(__name__)


class AddSchema(colander.MappingSchema):
    short_id = colander.SchemaNode(
        colander.String(), location="body", type="str")
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    description = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    fork_from_repo_id = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)


class PutSchema(colander.MappingSchema):
    name = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)
    description = colander.SchemaNode(
        colander.String(), location="body", type="str", missing=colander.drop)


class CollectionGetSchema(colander.MappingSchema):
    page = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=1,
        required=False,
        validator=colander.Range(min=1))
    page_size = colander.SchemaNode(
        colander.Integer(), location="querystring", type="int", default=5,
        required=False,
        validator=colander.Range(min=1, max=100))


@cornice_resource(
    name="api.repositories",
    collection_path="/api/v1/projects/{project_id}/repositories",
    collection_traverse="/projects/{project_id}/repositories",
    path="/api/v1/projects/{project_id}/repositories/{short_id}.json",
    traverse="/projects/{project_id}/repositories/{short_id}",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def resource_to_dict(self, obj):
        d = {
            'short_id': obj.short_id,
            'name': obj.name,
            'description': obj.description,
            'owner_login': None,
            'forked_from': None,
        }
        if obj.owner is not None:
            d['owner_login'] = obj.owner.login
        if obj.forked_from is not None:
            d['forked_from'] = obj.forked_from.short_id
        return d

    def resource_collection_to_dict(self, obj_list):
        return [self.resource_to_dict(obj) for obj in obj_list]

    # Helper validator methods
    def body_short_id_unique(self, request):
        short_id = request.json.get('short_id', None)
        if short_id and (short_id in self.context):
            self.request.errors.add(
                'body', 'short_id', 'Duplicate short_id')
            self.request.errors.status = HTTPBadRequest.code

    @cornice_view(
        schema=CollectionGetSchema,
        permission="kalam.repository.view-meta",
    )
    def collection_get(self):
        page = self.request.validated['page']
        page_size = self.request.validated['page_size']
        container = PaginatedContainer(self.context, page_size)
        if container.is_valid(page) is False:
            self.request.errors.add(
                'querystring', 'page', 'Page number (%d) out of range' % page)
            self.request.errors.status = HTTPBadRequest.code
            return
        entries = container.get(page)
        return {
            'type': 'repository_list',
            'current_page': page,
            'page_size': page_size,
            'total_pages': container.page_count,
            'total_size': container.size,
            'result': self.resource_collection_to_dict(entries),
        }

    @cornice_view(
        schema=AddSchema,
        validators=('body_short_id_unique',),
        permission="kalam.repository.add",
    )
    def collection_post(self):
        data = self.request.validated
        errors = self.request.errors
        repo = models.Repository(data['short_id'])
        repo.name = data.get('name', '')
        repo.description = data.get('description', '')

        fork_from_repo_id = data.get('fork_from_repo_id', None)
        fork_from = None
        if fork_from_repo_id is not None:
            fork_from = self.context.get(fork_from_repo_id, None)
            if fork_from is None:
                errors.add('body', 'fork_from_repo_id', 'No such repository')
                errors.status = HTTPBadRequest.code
                return
            if not self.request.has_permission(
                    'kalam.repository.read', context=fork_from):
                errors.add('body', 'fork_from_repo_id',
                           'No permission to read from repository')
                errors.status = HTTPForbidden.code
                return
            repo.forked_from = fork_from

        repo.owner = get_user(self.context, self.request.authenticated_userid)

        self.context.add(repo.short_id, repo)
        return {
            'type': 'repository',
            'result': self.resource_to_dict(repo),
        }

    @cornice_view(permission="kalam.repository.read")
    def get(self):
        return {
            'type': 'repository',
            'result': self.resource_to_dict(self.context)
        }

    @cornice_view(
        schema=PutSchema,
        permission="kalam.repository.manage"
    )
    def put(self):
        data = self.request.validated
        if 'name' in data:
            self.context.name = data['name']
        if 'description' in data:
            self.context.description = data['description']

        return {
            'type': 'repository',
            'result': self.resource_to_dict(self.context),
        }

    @cornice_view(permission="kalam.repository.manage")
    def delete(self):
        container = self.context.parent()
        container.remove(self.context.short_id)


# Local Variables:
# mode: python
# End:
