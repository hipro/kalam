# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import colander

from cornice.resource import (
    resource as cornice_resource,
    view as cornice_view,
    )

from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPNotFound,
)

from kalam.core.utils import login_has_permission, get_user

log = logging.getLogger(__name__)


class AddSchema(colander.MappingSchema):
    login = colander.SchemaNode(
        colander.String(), location="body", type="str")
    ui_permission = colander.SchemaNode(
        colander.String(), location="body", type="str")


class PutSchema(colander.MappingSchema):
    ui_permission = colander.SchemaNode(
        colander.String(), location="body", type="str")


@cornice_resource(
    name="api.repository_acl",
    collection_path="/api/v1/projects/{project_id}/repositories/{repo_id}/acl",
    collection_traverse="/projects/{project_id}/repositories/{repo_id}",
    path="/api/v1/projects/{project_id}/repositories/{repo_id}/acl/{login}",
    traverse="/projects/{project_id}/repositories/{repo_id}",
    permission="kalam.repository.manage",
)
class API(object):
    def __init__(self, request):
        self.request = request
        self.context = request.context

    def resource_to_dict(self, obj):
        vals = []
        for login, permission in obj.iteritems():
            user = get_user(self.context, login)
            vals.append({
                'login': login,
                'username': user.name,
                'ui_permission': permission,
            })
        return vals

    @cornice_view(permission="kalam.repository.view-meta")
    def collection_get(self):
        return {
            'type': 'repository_acl_list',
            'result': self.resource_to_dict(self.context.ui_acl),
        }

    @cornice_view(schema=AddSchema)
    def collection_post(self):
        request = self.request
        data = request.validated
        errors = request.errors
        login = data['login']
        ui_acl = self.context.ui_acl

        if login in ui_acl:
            errors.add('body', 'login', 'Already exists')
            errors.status = HTTPBadRequest.code
            return

        user = get_user(self.context, login)
        if user is None:
            errors.add('body', 'login', 'User does not exist')
            errors.status = HTTPBadRequest.code
            return

        if not login_has_permission(
                self.request, login, 'ps:repositories.view-meta',
                self.context):
            errors.add('body', 'login', 'User does not have sufficient rights')
            errors.status = HTTPBadRequest.code
            return

        ui_acl[login] = data['ui_permission']
        return {
            'type': 'repository_acl_list',
            'result': self.resource_to_dict(ui_acl),
        }

    @cornice_view(schema=PutSchema)
    def put(self):
        errors = self.request.errors
        data = self.request.validated
        login = self.request.matchdict['login']
        ui_acl = self.context.ui_acl

        if login not in ui_acl:
            errors.add('querystring', 'login', 'No entry for user')
            errors.status = HTTPNotFound.code
            return

        ui_acl[login] = data['ui_permission']
        return {
            'type': 'repository_acl_list',
            'result': self.resource_to_dict(ui_acl),
        }

    def delete(self):
        errors = self.request.errors
        login = self.request.matchdict['login']
        ui_acl = self.context.ui_acl

        if login not in ui_acl:
            errors.add('querystring', 'login', 'No entry for user')
            errors.status = HTTPNotFound.code
            return
        del(ui_acl[login])


# Local Variables:
# mode: python
# End:
