# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

from pyramid.httpexceptions import (
    HTTPOk,
)


def create_repo(app, project_id, repo_id):
    d = {
        'short_id': repo_id,
        'name': "Name " + repo_id,
    }
    url = "/api/v1/projects/%s/repositories" % (project_id, )
    app.post_json(url, d, status=HTTPOk.code)


def set_acl(app, project_id, repo_id, login, permission):
    d = {
        'login': login,
        'ui_permission': permission,
    }
    url = "/api/v1/projects/%s/repositories/%s/acl" % (project_id, repo_id)
    app.post_json(url, d, status=HTTPOk.code)


# Local Variables:
# mode: python
# End:
