# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPForbidden,
)

from kalam.core import testing as kt
from kalam.repositories import testing as util

log = logging.getLogger(__name__)


FIXTURE_DATA = [
    {
        "member": False,
        "result": {
            "view_list": HTTPForbidden.code,
            "create": HTTPForbidden.code,
            "read": HTTPForbidden.code,
            "modify": HTTPForbidden.code,
            "delete": HTTPForbidden.code,
        }
    },

    {
        "member": True,
        "role_permission": None,
        "result": {
            "view_list": HTTPForbidden.code,
            "create": HTTPForbidden.code,
            "read": HTTPForbidden.code,
            "modify": HTTPForbidden.code,
            "delete": HTTPForbidden.code,
        }
    },

    {
        "member": True,
        "role_permission": ("ps:repositories.view-meta", ),
        "result": {
            "view_list": HTTPOk.code,
            "create": HTTPForbidden.code,
            "read": HTTPForbidden.code,
            "modify": HTTPForbidden.code,
            "delete": HTTPForbidden.code,
        }
    },

    {
        "member": True,
        "role_permission": ("ps:repositories.create-new", ),
        "result": {
            "view_list": HTTPOk.code,
            "create": HTTPOk.code,
            "read": HTTPForbidden.code,
            "modify": HTTPForbidden.code,
            "delete": HTTPForbidden.code,
        }
    },

    {
        "member": True,
        "role_permission": ("ps:repositories.read-all", ),
        "result": {
            "view_list": HTTPOk.code,
            "create": HTTPForbidden.code,
            "read": HTTPOk.code,
            "modify": HTTPForbidden.code,
            "delete": HTTPForbidden.code,
        }
    },

    {
        "member": True,
        "role_permission": ("ps:repositories.write-all", ),
        "result": {
            "view_list": HTTPOk.code,
            "create": HTTPForbidden.code,
            "read": HTTPOk.code,
            "modify": HTTPForbidden.code,  # write implies git push not PUT
            "delete": HTTPForbidden.code,
        }
    },

    {
        "member": True,
        "role_permission": ("ps:repositories.manage-all", ),
        "result": {
            "view_list": HTTPOk.code,
            "create": HTTPOk.code,
            "read": HTTPOk.code,
            "modify": HTTPOk.code,
            "delete": HTTPOk.code,
        }
    },

    {
        "member": True,
        "role_permission": ("ps:repositories.view-meta",
                            "ps:repositories.create-new"),
        "result": {
            "view_list": HTTPOk.code,
            "create": HTTPOk.code,
            "read": HTTPForbidden.code,
            "modify": HTTPForbidden.code,
            "delete": HTTPForbidden.code,
        }
    },
]


class Fixture(object):
    def __init__(self, fixture_value):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config,  ['kalam.repositories', ])
        self.auth = self.app.k_authn
        kt.create_user(self.app, "john", "john@john.com")
        kt.create_project(self.app, "p1", "Project one")

        if fixture_value["member"] is True:
            role_permission = fixture_value["role_permission"]
            if role_permission is None:
                kt.create_membership(self.app, "p1", "john")
            else:
                kt.create_role(self.app, "p1", "role1", "Role1",
                               role_permission)
                kt.create_membership(self.app, "p1", "john", "role1")

        self.result = fixture_value["result"]

        util.create_repo(self.app, "p1", "r1")
        self.url = "/api/v1/projects/p1/repositories"
        self.r1_url = "/api/v1/projects/p1/repositories/r1.json"
        self.auth.set_current_user('john')


def generate_id(fixture_value):
    out = []
    if fixture_value["member"] is True:
        role_permission = fixture_value.get("role_permission", None)
        perm_string = None
        if role_permission:
            perm_string = ','.join(role_permission)
        out.append("Member(%s)" % (perm_string, ))
    else:
        out.append("Non member")
    return ",".join(out)


@pytest.fixture(params=FIXTURE_DATA, ids=generate_id)
def fixture(request):
    fixture = Fixture(request.param)

    def fin():
        pyramid.testing.tearDown()
    request.addfinalizer(fin)
    return fixture


def test_create_repo(fixture):
    d = {
        'short_id': 'r2',
        'name': 'Repo 2',
    }
    fixture.app.post_json(fixture.url, d, status=fixture.result["create"])


def test_view_list(fixture):
    fixture.app.get(fixture.url, status=fixture.result["view_list"])


def test_read_repo(fixture):
    fixture.app.get(fixture.r1_url, status=fixture.result["read"])


def test_modify_repo(fixture):
    d = {
        'name': 'New name',
    }
    fixture.app.put(fixture.r1_url, d, status=fixture.result["modify"])


def test_delete_repo(fixture):
    fixture.app.delete(fixture.r1_url, status=fixture.result["delete"])


# Local Variables:
# mode: python
# End:
