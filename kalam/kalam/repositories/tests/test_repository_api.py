# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import unittest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPBadRequest,
)

from kalam.core import testing as kt
from kalam.repositories import testing as util

log = logging.getLogger(__name__)


class TestPOST(unittest.TestCase):
    def setUp(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config, ['kalam.repositories', ])
        self.auth = self.app.k_authn

        kt.create_user(self.app, "john", "john@john.com")
        kt.create_project(self.app, "p1", "Project one")
        kt.create_role(self.app, "p1", "r1", "Role1",
                       ["ps:repositories.create-new", ])
        kt.create_membership(self.app, "p1", "john", "r1")
        self.url = "/api/v1/projects/p1/repositories"
        self.auth.set_current_user('john')

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_create_repo(self):
        d = {
            'short_id': 'r1',
            'name': 'Repo 1',
        }
        resp = self.app.post_json(self.url, d, status=HTTPOk.code)
        data = resp.json['result']
        self.assertEqual(data['owner_login'], 'john')
        self.assertEqual(data['forked_from'], None)

    def test_fails_on_duplicate_repo(self):
        d = {
            'short_id': 'r1',
            'name': 'Repo 1',
        }
        self.app.post_json(self.url, d, status=HTTPOk.code)
        self.app.post_json(self.url, d, status=HTTPBadRequest.code)

    def test_fork_repo(self):
        d = {
            'short_id': 'r1',
            'name': 'Repo 1',
        }
        self.app.post_json(self.url, d, status=HTTPOk.code)
        d = {
            'short_id': 'r2',
            'name': 'Repo 2',
            'fork_from_repo_id': 'r1',
        }
        resp = self.app.post_json(self.url, d, status=HTTPOk.code)
        data = resp.json['result']
        self.assertEqual(data['owner_login'], 'john')
        self.assertEqual(data['forked_from'], 'r1')

    def test_fork_fails_on_non_existing_repo(self):
        d = {
            'short_id': 'r1',
            'name': 'Repo 1',
            'fork_from_repo_id': 'r0',
        }
        self.app.post_json(self.url, d, status=HTTPBadRequest.code)


class TestGET(unittest.TestCase):
    def setUp(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config, ['kalam.repositories', ])
        self.auth = self.app.k_authn

        kt.create_user(self.app, "john", "john@john.com")
        kt.create_project(self.app, "p1", "Project one")
        kt.create_role(self.app, "p1", "r1", "Role1",
                       ["ps:repositories.read-all",
                        "ps:repositories.create-new"])
        kt.create_membership(self.app, "p1", "john", "r1")

        self.collection_url = "/api/v1/projects/p1/repositories"
        self.url = self.collection_url + '/r1.json'
        self.auth.set_current_user('john')
        util.create_repo(self.app, 'p1', 'r1')
        self.auth.set_current_user('admin')

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_can_get_existing_with_all_keys(self):
        resp = self.app.get(self.url, status=HTTPOk.code)
        data = resp.json['result']
        keys = ('short_id', 'name', 'description', 'owner_login',
                'forked_from')
        for k in keys:
            self.assertTrue(k in data)
        self.assertEqual(len(keys), len(data))
        self.assertEqual(data['short_id'], 'r1')


class TestPUT(unittest.TestCase):
    def setUp(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config, ['kalam.repositories', ])
        self.auth = self.app.k_authn

        kt.create_user(self.app, "john", "john@john.com")
        kt.create_project(self.app, "p1", "Project one")
        kt.create_role(self.app, "p1", "r1", "Role1",
                       ["ps:repositories.create-new", ])
        kt.create_membership(self.app, "p1", "john", "r1")
        self.collection_url = "/api/v1/projects/p1/repositories"
        self.url = self.collection_url + '/r1.json'
        self.auth.set_current_user('john')
        util.create_repo(self.app, 'p1', 'r1')

    def tearDown(self):
        pyramid.testing.tearDown()

    def test_can_modify_name(self):
        d = {
            'name': 'New',
        }
        self.app.put_json(self.url, d, status=HTTPOk.code)
        resp = self.app.get(self.url, status=HTTPOk.code)
        data = resp.json['result']
        self.assertEqual(data['name'], d['name'])

    def test_can_modify_description(self):
        d = {
            'description': 'New',
        }
        self.app.put_json(self.url, d, status=HTTPOk.code)
        resp = self.app.get(self.url, status=HTTPOk.code)
        data = resp.json['result']
        self.assertEqual(data['description'], d['description'])


# Local Variables:
# mode: python
# End:
