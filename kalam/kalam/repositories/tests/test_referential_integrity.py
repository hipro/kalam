# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging
import unittest

import pyramid.testing

from pyramid import traversal

from kalam.core import testing as kt
from kalam import core
from kalam.repositories import testing as util

log = logging.getLogger(__name__)


class TestReferentialIntegrity(unittest.TestCase):
    def setUp(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config, ['kalam.repositories', ])
        self.auth = self.app.k_authn

        kt.create_user(self.app, "john", "john@john.com")
        kt.create_project(self.app, "p1", "Project one")
        kt.create_role(self.app, "p1", "r1", "Role1",
                       ["ps:repositories.create-new", ])
        kt.create_membership(self.app, "p1", "john", "r1")
        self.collection_url = "/api/v1/projects/p1/repositories"
        self.url = self.collection_url + '/r1.json'
        self.auth.set_current_user('john')
        util.create_repo(self.app, 'p1', 'r1')
        self.auth.set_current_user('admin')

    def tearDown(self):
        pyramid.testing.tearDown()

    def _getRoot(self):
        appmaker = self.app.k_registry.getUtility(
            core.interfaces.IAppMaker)
        return appmaker.root

    def test_remove_repository_owner_on_user_deletion(self):
        root = self._getRoot()
        repo = traversal.find_resource(
            root, '/projects/p1/repositories/r1')
        self.assertEqual(repo.owner.login, 'john')
        kt.delete_user(self.app, 'john')
        self.assertTrue(repo.owner is None)


# Local Variables:
# mode: python
# End:
