# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

import logging

import pytest

import pyramid.testing

from pyramid.httpexceptions import (
    HTTPOk,
    HTTPBadRequest,
    HTTPNotFound,
)

from kalam.core import testing as kt
from kalam.repositories import testing as util

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Fixture(object):
    def __init__(self):
        config = pyramid.testing.setUp()
        self.app = kt.create_app(config, ['kalam.repositories'])
        self.auth = self.app.k_authn
        kt.create_user(self.app, "u1", "u1@test.com")
        kt.create_user(self.app, "u2", "u2@test.com", name="u2")
        kt.create_user(self.app, "u3", "u3@test.com")
        kt.create_user(self.app, "u4", "u4@test.com")
        kt.create_user(self.app, 'u5', 'u5@test.com', admin=True)
        kt.create_user(self.app, "u6", "u6@test.com")
        kt.mark_user_as_inactive(self.app, 'u6')

        kt.create_project(self.app, "p1", "Project one")
        kt.create_role(self.app, "p1", "role1", "Role1",
                       ("ps:repositories.manage-all", ))
        kt.create_role(self.app, "p1", "role2", "Role2",
                       ("ps:repositories.view-meta", ))
        kt.create_membership(self.app, "p1", "u1", "role1")
        kt.create_membership(self.app, "p1", "u2", "role2")
        kt.create_membership(self.app, "p1", "u3")
        kt.create_membership(self.app, "p1", "u6", "role2")

        self.auth.set_current_user('u1')
        util.create_repo(self.app, "p1", "r1")
        self.url = "/api/v1/projects/p1/repositories/r1/acl"


def fixture_finalizer():
    pyramid.testing.tearDown()


@pytest.fixture
def fixture(request):
    fixture = Fixture()
    request.addfinalizer(fixture_finalizer)
    return fixture


def test_get_empty_list(fixture):
    resp = fixture.app.get(fixture.url, status=HTTPOk.code)
    assert resp.json['type'] == 'repository_acl_list'
    result = resp.json['result']
    assert len(result) == 0


def test_create_acl_entry_for_read(fixture):
    d = {
        'login': 'u2',
        'ui_permission': "ps:repository.read",
    }
    resp = fixture.app.post_json(fixture.url, d, status=HTTPOk.code)
    assert resp.json['type'] == 'repository_acl_list'
    result = resp.json['result']
    assert len(result) == 1
    assert result[0]['login'] == 'u2'
    assert result[0]['username'] == 'u2'
    assert result[0]['ui_permission'] == 'ps:repository.read'


def test_create_acl_entry_for_write(fixture):
    d = {
        'login': 'u2',
        'ui_permission': "ps:repository.write",
    }
    resp = fixture.app.post_json(fixture.url, d, status=HTTPOk.code)
    assert resp.json['type'] == 'repository_acl_list'
    result = resp.json['result']
    assert len(result) == 1
    assert result[0]['login'] == 'u2'
    assert result[0]['ui_permission'] == 'ps:repository.write'


def test_create_acl_entry_for_manage(fixture):
    d = {
        'login': 'u2',
        'ui_permission': "ps:repository.manage",
    }
    resp = fixture.app.post_json(fixture.url, d, status=HTTPOk.code)
    assert resp.json['type'] == 'repository_acl_list'
    result = resp.json['result']
    assert len(result) == 1
    assert result[0]['login'] == 'u2'
    assert result[0]['ui_permission'] == 'ps:repository.manage'


def test_cannot_create_duplicate_acl_entry(fixture):
    d = {
        'login': 'u2',
        'ui_permission': "ps:repository.manage",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPOk.code)
    fixture.app.post_json(fixture.url, d, status=HTTPBadRequest.code)


def test_cannot_create_acl_entry_for_non_existant_user(fixture):
    d = {
        'login': 'unknown-user',
        'ui_permission': "ps:repository.manage",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPBadRequest.code)


def test_cannot_create_acl_entry_for_non_member(fixture):
    d = {
        'login': 'u4',
        'ui_permission': "ps:repository.manage",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPBadRequest.code)


def test_cannot_create_acl_entry_for_inactive_user(fixture):
    d = {
        'login': 'u6',
        'ui_permission': "ps:repository.manage",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPBadRequest.code)


def test_can_create_acl_entry_for_non_member_admin_user(fixture):
    d = {
        'login': 'u5',
        'ui_permission': "ps:repository.manage",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPOk.code)


def test_cannot_create_acl_for_user_without_view_list_permission(fixture):
    d = {
        'login': 'u3',
        'ui_permission': "ps:repository.read",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPBadRequest.code)


def test_update_acl_for_existing_entry(fixture):
    d = {
        'login': 'u2',
        'ui_permission': "ps:repository.read",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPOk.code)
    d = {
        'ui_permission': "ps:repository.write",
    }
    resp = fixture.app.put_json(fixture.url + "/u2", d, status=HTTPOk.code)
    result = resp.json['result']
    assert result[0]["ui_permission"] == d["ui_permission"]


def test_update_acl_for_non_existant_entry(fixture):
    d = {
        'ui_permission': "ps:repository.write",
    }
    fixture.app.put_json(fixture.url + "/u2", d, status=HTTPNotFound.code)


def test_remove_existing_entry(fixture):
    d = {
        'login': 'u2',
        'ui_permission': "ps:repository.read",
    }
    fixture.app.post_json(fixture.url, d, status=HTTPOk.code)
    fixture.app.delete(fixture.url + "/u2", status=HTTPOk.code)


def test_remove_non_existant_entry(fixture):
    fixture.app.delete(fixture.url + "/u2", status=HTTPNotFound.code)

# Local Variables:
# mode: python
# End:
