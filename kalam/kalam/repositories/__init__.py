# Copyright (C) 2016 HiPro IT Solutions Private Limited, Chennai. All
# rights reserved.
#
# This program and the accompanying materials are made available
# under the terms described in the LICENSE file which accompanies
# this distribution. If the LICENSE file was not attached to this
# distribution or for further clarifications, please contact
# legal@hipro.co.in.

from . import models


def includeme(config):
    from kalam.core.interfaces import IPermissionSetManager

    pset_manager = config.registry.getUtility(IPermissionSetManager)
    for name, pset in models.UI_PERMISSIONS.iteritems():
        pset_manager.add(name, pset)

    config.registry.registerHandler(models.initialise_on_project_creation)
    config.registry.registerHandler(models.cleanup_on_project_deletion)
    config.registry.registerHandler(models.cleanup_removed_user)

# Local Variables:
# mode: python
# End:
