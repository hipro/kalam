import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'pyramid',
    'pyramid_zodbconn',
    'transaction',
    'pyramid_tm',
    'ZODB3',
    'repoze.folder',
    'pyramid_debugtoolbar',
    'pyramid_beaker',
    'pyramid_chameleon',
    'waitress',
    'cornice',
    'colander',
    'sshpubkeys'
]

tests_require = requires + ['mock', ]

setup(name='kalam',
      version='0.1',
      description='kalam',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Pyramid",
          "Topic :: Internet :: WWW/HTTP",
          "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
      ],
      author='HiPro IT Solutions Private Limited',
      author_email='info@hipro.co.in',
      url='',
      keywords='web pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=tests_require,
      extras_require={'test': tests_require},
      test_suite="kalam",
      entry_points="""\
      [paste.app_factory]
      main = kalam:main
      [console_scripts]
      initialise_db = kalam.scripts.initialisedb:main
      populate_db = kalam.scripts.populate_db:main
      """,
      )
